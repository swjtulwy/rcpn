import typing

from PyQt5 import QtGui
from PyQt5.QtCore import Qt, QRectF
from PyQt5.QtGui import QFont, QPen, QPainter, QPainterPath, QColor
from PyQt5.QtWidgets import QGraphicsEllipseItem, QGraphicsSimpleTextItem, QGraphicsItem, QWidget


class MarkingNode(QGraphicsEllipseItem):
    Type = QGraphicsItem.UserType + 10

    def __init__(self, x, y, w, h, node):
        super(MarkingNode, self).__init__()

        self.node = node
        self.setRect(0, 0, w, h)
        self.setPos(x, y)

        self.in_arcs = []
        self.out_arcs = []
        self.brush_color = Qt.white
        for mark in self.node.marking:
            if mark == -1:
                self.brush_color = QColor(72, 192, 163)
                break

        self.label = QGraphicsSimpleTextItem(self)  # 用于显示结点描述的文本图元
        self.label.setText(self.node.name)
        self.label.setPos(13, 7)  # 在父Item的坐标系中设置位置
        self.label.setFlag(QGraphicsItem.ItemIsSelectable, False)
        self.label.setFlag(QGraphicsItem.ItemIsMovable, False)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(8)
        self.label.setFont(font)
        self.label.setZValue(1200.0)

        self.setZValue(1000.0)  # 位于图层的上方
        self.setFlag(QGraphicsItem.ItemIsMovable, True)  # 可以移动
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)  # 可以选择
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)

    def paint(self, painter: QtGui.QPainter, option: 'QStyleOptionGraphicsItem', widget: typing.Optional[QWidget] = ...) -> None:
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
        painter.setBrush(self.brush_color)
        painter.drawEllipse(0, 0, 50, 30)

    def shape(self) -> QtGui.QPainterPath:
        shape = QPainterPath()
        shape.addEllipse(0, 0, 50, 30)
        return shape

    def itemChange(self, change: 'QGraphicsItem.GraphicsItemChange', value: typing.Any) -> typing.Any:
        if change == QGraphicsItem.ItemPositionChange or change == QGraphicsItem.ItemPositionHasChanged:
            for arc in self.in_arcs:
                arc.update_position()
            for arc in self.out_arcs:
                arc.update_position()
        return value

    def get_node_center_pos(self) -> QRectF:
        a = (self.pen().width()) / 2
        return QRectF(0, 0, 50, 30).normalized().adjusted(-a, -a, a, a).center() + self.pos()