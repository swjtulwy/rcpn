import copy

from PyQt5.QtGui import QPainterPath

from src.basic.arc import Arc
from src.basic.place import Place
from src.basic.transition import Transition

import numpy as np
# from numpy import ndarray

from queue import Queue

from src.serialization.xml_attribute import XmlPlaceAttr, XmlTransitionAttr, XmlArcAttr


class ReachableGraphNode:
    def __init__(self, name, marking, fire_list, path):
        self.name = name
        self.marking = marking.copy()
        self.fire_list = fire_list
        self.path = path

    def equal_marking(self, marking):
        if len(marking) != len(self.marking):
            return False
        for i in range(len(marking)):
            # if marking[i] == -1 or self.marking[i] == -1:
            #     continue
            if marking[i] != self.marking[i]:
                return False
        return True

    def equal_marking_general(self, marking):
        if len(marking) != len(self.marking):
            return False
        for i in range(len(marking)):
            if marking[i] == -1 or self.marking[i] == -1:
                continue
            if marking[i] != self.marking[i]:
                return False
        return True

    def greater(self, marking):
        if len(marking) != len(self.marking):
            return False
        flag = True
        for i in range(len(marking)):
            if self.marking[i] >= marking[i]:
                continue
            else:
                flag = False
                break
        return flag

    def check_bound(self):
        for node in self.path:
            if self.greater(node.marking) and not self.equal_marking(node.marking):
                return True
        return False

    def check_bound_general(self, unbound_indices):
        flag = True
        for node in self.path:
            if self.greater(node.marking) and not self.equal_marking(node.marking):
                flag = False
                for i in range(len(self.marking)):
                    if self.marking[i] > node.marking[i]:
                        self.marking[i] = -1
                        unbound_indices.append(i)
        return flag



class ReachableGraphArc:
    def __init__(self, source, target, weight):
        self.source = source
        self.target = target
        self.weight = weight


class DynamicProperty:
    def __init__(self, places, transitions, arcs):
        self.places = places  # 当前场景的所有库所图元集合
        self.transitions = transitions  # 当前场景的所有变迁图元集合
        self.arcs = arcs  # 当前场景的所有弧集合

        self.lbr_flags = [True, True, True]

        self.places = sorted(self.places, key=lambda x: int(x.place_id))
        self.transitions = sorted(self.transitions, key=lambda x: int(x.transition_id))
        self.unbounded_place_indices = []
        self.unlive_tran_indices = []

        self.tran_map = []
        self.place_map = []
        for tran in self.transitions:
            self.tran_map.append(int(tran.transition_id))
        for place in self.places:
            self.place_map.append(int(place.place_id))

        # 输入矩阵和输出矩阵，行为变迁，列为库所
        self.in_matrix = np.zeros((len(self.transitions), len(self.places))).astype(int)
        self.out_matrix = np.zeros((len(self.transitions), len(self.places))).astype(int)

        for arc in self.arcs:
            source_item = arc.source_item
            target_item = arc.target_item
            if arc.source_item.Type == Place.Type:
                source_index = self.place_map.index(int(source_item.place_id))
                target_index = self.tran_map.index(int(target_item.transition_id))
                self.in_matrix[target_index][source_index] = int(arc.weight)
            if arc.source_item.Type == Transition.Type:
                source_index = self.tran_map.index(int(source_item.transition_id))
                target_index = self.place_map.index(int(target_item.place_id))
                self.out_matrix[source_index][target_index] = int(arc.weight)

        self.initial_marking = []
        self.current_marking = []
        for place in self.places:
            self.current_marking.append(place.tokens)
            self.initial_marking.append(place.tokens)

        # self.reachable_marking_set = []      # 可达标识集
        self.rg_nodes = []
        self.rg_arcs = []

    def get_fireable_trans(self, current_marking):
        fireable_trans_index = []
        for i in range(len(self.transitions)):
            flag = True
            for j in range(len(self.places)):
                in_arc_weight = self.in_matrix[i][j]
                if current_marking[j] != -1 and in_arc_weight != 0 and current_marking[j] < in_arc_weight:
                    flag = False
            if flag:
                fireable_trans_index.append(i)
        return fireable_trans_index

    def fire(self, tran_index, current_marking):
        for place_index in range(len(self.places)):
            if current_marking[place_index] == -1:
                # current_marking[place_index] = -1
                continue
            in_weight = self.in_matrix[tran_index][place_index]
            if in_weight != 0 and current_marking[place_index] >= in_weight:
                current_marking[place_index] -= in_weight
            out_weight = self.out_matrix[tran_index][place_index]
            if out_weight != 0:
                current_marking[place_index] += out_weight

    def find_node(self, nodes, marking):
        for node in nodes:
            if node.equal_marking_general(marking):
                return node
        return None

    def check_bound(self, init_marking):
        queue = Queue()
        m0 = ReachableGraphNode("M0", init_marking, self.get_fireable_trans(init_marking), [])
        self.rg_nodes.append(m0)
        queue.put(m0)
        while not queue.empty():
            cur_node = queue.get()
            for tran_index in cur_node.fire_list:
                new_marking = cur_node.marking.copy()
                self.fire(tran_index, new_marking)
                if not self.find_node(self.rg_nodes, new_marking):
                    path = cur_node.path.copy()
                    path.append(cur_node)
                    new_node = ReachableGraphNode("M"+str(len(self.rg_nodes)), new_marking, self.get_fireable_trans(new_marking), path)
                    unbound_indices = []
                    if not new_node.check_bound_general(unbound_indices):
                        self.lbr_flags[0] = False
                    self.unbounded_place_indices = list(set(self.unbounded_place_indices).union(set(unbound_indices)))
                    self.rg_nodes.append(new_node)
                    queue.put(new_node)
                    # self.rg_arcs.append(ReachableGraphArc(cur_node, new_node, "t"+str(self.tran_map.index(tran_index))))
                    self.rg_arcs.append(ReachableGraphArc(cur_node, new_node, self.transitions[tran_index].name))
                else:
                    exist_node = self.find_node(self.rg_nodes, new_marking)
                    # self.rg_arcs.append(ReachableGraphArc(cur_node, exist_node, "t"+str(self.tran_map.index(tran_index))))
                    self.rg_arcs.append(ReachableGraphArc(cur_node, exist_node, self.transitions[tran_index].name))

    def check_reversible(self, init_marking):
        self.check_bound(init_marking)
        m0_rg_nodes = copy.deepcopy(self.rg_nodes)
        for node in m0_rg_nodes:
            self.reset_state()
            self.check_bound(node.marking)
            m1_rg_nodes = copy.deepcopy(self.rg_nodes)
            if not self.find_node(m1_rg_nodes, init_marking):
                return False
        return True

    def check_live(self, init_marking):
        live_flag = True
        self.unlive_tran_indices = []
        if self.check_reversible(init_marking):
            self.check_bound(init_marking)
            m0_rg_arcs = copy.deepcopy(self.rg_arcs)
            weights = [arc.weight for arc in m0_rg_arcs]
            for i in range(len(self.transitions)):
                # 一级活变迁判断, 在可达标记图中出现过就是一级活的
                if self.transitions[i].name not in weights:
                    live_flag = False
                    self.unlive_tran_indices.append(i)
        else:
            # 若可达标识集 R(M_0)  中任意标识 M 下，存在一个标识 M'  属于 R(M) ，
            # 使得变迁 t 在以 M' 下可以激发，那么 t 在网中是四级活的（初始标识为 M）。
            tran_flags = [True for i in range(len(self.transitions))]
            self.check_bound(init_marking)
            m0_rg_nodes = copy.deepcopy(self.rg_nodes) # R(M_0)
            # node 为 M
            for node in m0_rg_nodes:
                self.reset_state()
                self.check_bound(node.marking)
                new_rg_nodes = copy.deepcopy(self.rg_nodes)  # R(M)
                tran_flags_ = [False for i in range(len(self.transitions))]
                # node_ 为 M'
                for node_ in new_rg_nodes:
                    fireable_trans = self.get_fireable_trans(node_.marking)
                    for i in fireable_trans:
                        tran_flags_[i] = True
                for i in range(len(self.transitions)):
                    tran_flags[i] = tran_flags[i] and tran_flags_[i]
            for i in range(len(self.transitions)):
                if not tran_flags[i]:
                    self.unlive_tran_indices.append(i)
                live_flag = live_flag and tran_flags[i]

        self.reset_state()
        return live_flag

    def reset_state(self):
        self.rg_nodes = []
        self.rg_arcs = []

    def get_reachable_graph(self):
        self.check_bound(self.initial_marking)
        rg_nodes_back = self.rg_nodes.copy()
        rg_arcs_back = self.rg_arcs.copy()
        self.reset_state()
        self.lbr_flags[2] = self.check_reversible(self.initial_marking)
        self.lbr_flags[1] = self.check_live(self.initial_marking)
        unbounded_place_names = [self.places[index].name for index in self.unbounded_place_indices]
        unlive_tran_names = [self.transitions[index].name for index in self.unlive_tran_indices]
        return self.lbr_flags, rg_nodes_back, rg_arcs_back, self.tran_map,\
               self.place_map, unbounded_place_names, unlive_tran_names

    def get_lbr_properties(self):
        self.check_bound(self.initial_marking)
        self.reset_state()
        self.lbr_flags[2] = self.check_reversible(self.initial_marking)
        self.lbr_flags[1] = self.check_live(self.initial_marking)
        return self.lbr_flags

    # 判断是否为开式状态机 任一变迁只有一个前置位置和后置位置
    def is_osm(self):
        for tran in self.transitions:
            if len(tran.input_arcs) > 1 or len(tran.output_arcs) > 1:
                return False
        return True

    # 判断是否为开式标记图 任意位置只有一个前置变迁和后置变迁
    def is_omg(self):
        for place in self.places:
            if len(place.input_arcs) > 1 or len(place.output_arcs) > 1:
                return False
        return True

    # 判断是否为闭合标记图
    def is_cmg(self, input_node, output_node):
        pass

    def is_ppn(self, input_node, output_node):
        new_places = self.places.copy()
        new_trans = self.transitions.copy()
        new_arcs = self.arcs.copy()
        if input_node.Type == Place.Type:
            added_place = XmlPlaceAttr()
            added_place.id = 199
            added_place.identity_id = 999999999
            added_place.initmark = 1
            added_place.capacity = 10000
            added_place_item = Place(place=added_place)
            added_place_item.init_from_xml(added_place)
            new_places.append(added_place_item)

            added_tran1 = XmlTransitionAttr()
            added_tran1.id = 199
            added_tran1.identity_id = 999999998
            added_tran1_item = Transition(transition=added_tran1)
            added_tran1_item.init_from_xml(added_tran1)

            added_tran2 = XmlTransitionAttr()
            added_tran2.id = 198
            added_tran2.identity_id = 99999997
            added_tran2_item = Transition(transition=added_tran2)
            added_tran2_item.init_from_xml(added_tran2)

            new_trans.append(added_tran1_item)
            new_trans.append(added_tran2_item)

            added_arc1 = XmlArcAttr()
            added_arc1.id = 198
            added_arc1.identity_id = 999999996
            added_arc1.weight = 1
            added_arc1_item = Arc(arc=added_arc1)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc1_item.init_from_xml(added_tran1_item, input_node, path, added_arc1)

            added_arc2 = XmlArcAttr()
            added_arc2.id = 197
            added_arc2.identity_id = 999999995
            added_arc2.weight = 1
            added_arc2_item = Arc(arc=added_arc2)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc2_item.init_from_xml(added_place_item, added_tran1_item, path, added_arc2)

            added_arc3 = XmlArcAttr()
            added_arc3.id = 196
            added_arc3.identity_id = 999999994
            added_arc3.weight = 1
            added_arc3_item = Arc(arc=added_arc3)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc3_item.init_from_xml(added_tran2_item, added_place_item, path, added_arc3)

            added_arc4 = XmlArcAttr()
            added_arc4.id = 195
            added_arc4.identity_id = 999999993
            added_arc4.weight = 1
            added_arc4_item = Arc(arc=added_arc4)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc4_item.init_from_xml(output_node, added_tran2_item, path, added_arc4)

            new_arcs.append(added_arc1_item)
            new_arcs.append(added_arc2_item)
            new_arcs.append(added_arc3_item)
            new_arcs.append(added_arc4_item)

        else:
            added_tran = XmlTransitionAttr()
            added_tran.id = 199
            added_tran.identity_id = 999999999
            added_tran_item = Transition(transition=added_tran)
            added_tran_item.init_from_xml(added_tran)

            new_trans.append(added_tran_item)
            
            added_place1 = XmlPlaceAttr()
            added_place1.id = 199
            added_place1.identity_id = 999999998
            added_place1.initmark = 1
            added_place1.capacity = 10000
            added_place_item1 = Place(place=added_place1)
            added_place_item1.init_from_xml(added_place1)
            
            added_place2 = XmlPlaceAttr()
            added_place2.id = 199
            added_place2.identity_id = 999999997
            added_place2.initmark = 0
            added_place2.capacity = 10000
            added_place_item2 = Place(place=added_place2)
            added_place_item2.init_from_xml(added_place2)
            
            new_places.append(added_place_item1)
            new_places.append(added_place_item2)


            added_arc1 = XmlArcAttr()
            added_arc1.id = 198
            added_arc1.identity_id = 999999996
            added_arc1.weight = 1
            added_arc1_item = Arc(arc=added_arc1)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc1_item.init_from_xml(added_place_item1, input_node, path, added_arc1)

            added_arc2 = XmlArcAttr()
            added_arc2.id = 197
            added_arc2.identity_id = 999999995
            added_arc2.weight = 1
            added_arc2_item = Arc(arc=added_arc2)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc2_item.init_from_xml(added_tran_item, added_place_item1, path, added_arc2)

            added_arc3 = XmlArcAttr()
            added_arc3.id = 196
            added_arc3.identity_id = 999999994
            added_arc3.weight = 1
            added_arc3_item = Arc(arc=added_arc3)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc3_item.init_from_xml(added_place_item2, added_tran_item, path, added_arc3)

            added_arc4 = XmlArcAttr()
            added_arc4.id = 195
            added_arc4.identity_id = 999999993
            added_arc4.weight = 1
            added_arc4_item = Arc(arc=added_arc4)
            path = QPainterPath(input_node.mapToScene(input_node.get_bound_rect().center()))
            added_arc4_item.init_from_xml(output_node, added_place_item2, path, added_arc4)

            new_arcs.append(added_arc1_item)
            new_arcs.append(added_arc2_item)
            new_arcs.append(added_arc3_item)
            new_arcs.append(added_arc4_item)
        # print(len(self.places), len(new_places))
        # print(len(self.transitions), len(new_trans))
        # print(len(self.arcs), len(new_arcs))
        new_d = DynamicProperty(new_places, new_trans, new_arcs)
        l, b, r = new_d.get_lbr_properties()
        if not l or not b or not r:
            return False
        return True



if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import networkx as nx

    G = nx.DiGraph()
    G.add_edge("(2, 0, 1)", "(0, 2, 1)", weight=0.1, ac="T1")
    G.add_edge("(2, 0, 1)", "(1, 1, 1)", weight=0.1, ac="T2")
    G.add_edge("(1, 1, 1)", "(2, 0, 1)", weight=0.1, ac="T3")
    G.add_edge("(1, 1, 1)", "(0, 2, 1)", weight=0.1, ac="T2")
    G.add_edge("(0, 2, 1)", "(1, 1, 1)", weight=0.1, ac="T3")

    pos = nx.spring_layout(G)  # positions for all nodes - seed for reproducibility
    nx.draw_networkx_nodes(G, pos)
    nx.draw_networkx_edges(G, pos)

    nx.draw_networkx_labels(G, pos, font_size=8, font_family="sans-serif")
    edge_labels = nx.get_edge_attributes(G, "ac")
    nx.draw_networkx_edge_labels(G, pos, edge_labels)

    ax = plt.gca()
    ax.margins(0.08)
    plt.axis("off")

    plt.show()



