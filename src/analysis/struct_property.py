from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc import Arc

import numpy as np

class StructProperty:
    def __init__(self, places, transitions, arcs):
        self.places = places
        self.transitions = transitions
        self.arcs = arcs

    def init_items(self):
        for item in self.items:
            if item.Type == Place.Type:
                self.places.append(item)
            elif item.Type == Transition.Type:
                self.transitions.append(item)
            elif item.Type == Arc.Type:
                self.arcs.append(item)

    def is_pure_net(self):
        for place in self.places:
            if place.check_has_loop():
                return False
        for tran in self.transitions:
            if tran.check_has_loop():
                return False
        return True

    def get_incidence_matrix(self):
        matrix = np.zeros((len(self.transitions), len(self.places))).astype(int)
        tran_map = []
        place_map = []
        for tran in self.transitions:
            tran_map.append(int(tran.transition_id))
        for place in self.places:
            place_map.append(int(place.place_id))
        place_map = sorted(place_map)
        tran_map = sorted(tran_map)
        for arc in self.arcs:
            source = arc.source_item
            target = arc.target_item
            if source.Type == Transition.Type:
                matrix[tran_map.index(int(source.transition_id))][place_map.index(int(target.place_id))] += 1
            if source.Type == Place.Type:
                matrix[tran_map.index(int(target.transition_id))][place_map.index(int(source.place_id))] -= 1
        return matrix, place_map, tran_map

    def get_p_invariant(self, incidence):
        matrix = None
        incidence = incidence
        n = incidence.shape[0]
        m = incidence.shape[1]
        incidence_t = np.transpose(incidence)  # 9 * 10
        matrix_i = np.eye(m).astype(int)

        augement_matrix = np.c_[incidence_t, matrix_i]

        for col in range(incidence_t.shape[1]):
            for i in range(augement_matrix.shape[0]):
                if augement_matrix[i][col] != 0:
                    j = i
                    while j < augement_matrix.shape[0]:
                        if augement_matrix[j][col] == -augement_matrix[i][col] :
                            augement_matrix = np.append(augement_matrix, [augement_matrix[j] + augement_matrix[i]], axis=0)
                        j = j + 1
                else:
                    continue
            delete_rows = []
            for i in range(augement_matrix.shape[0]):
                if augement_matrix[i][col] != 0:
                    delete_rows.append(i)
            augement_matrix = np.delete(augement_matrix, delete_rows, axis=0)
            matrix = np.delete(augement_matrix, range(incidence_t.shape[1]), axis=1)
        return matrix

    def get_t_invariant(self):
        incidence = np.transpose(self.get_incidence_matrix()[0])
        matirx = self.get_p_invariant(incidence)
        return matirx




