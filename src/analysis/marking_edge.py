import math
import typing

from PyQt5 import QtGui
from PyQt5.QtCore import QPointF, QLineF, Qt, QRect, QRectF
from PyQt5.QtGui import QPainterPath, QPolygonF, QPainter, QColor
from PyQt5.QtWidgets import QGraphicsSimpleTextItem, QGraphicsItem, QGraphicsEllipseItem, \
    QGraphicsPathItem, QWidget, QStyleOptionGraphicsItem


class MarkingEdge(QGraphicsPathItem):
    OUTLINE_WIDTH = 20
    ARC_SIZE = 10.0
    ARC_ANGLE = math.pi / 3

    def __init__(self, parent, source, dest, weight, flag, show_first_flag):
        super(MarkingEdge, self).__init__()
        self.parent = parent
        self.source_item = source
        self.target_item = dest
        self.weight = weight

        self.arc_flag = flag
        self.show_first_flag = show_first_flag

        self.arc_head = QPolygonF()  # 弧箭头

        self.p1 = self.source_item.get_node_center_pos()
        # 目标节点的中心位置
        self.p2 = self.target_item.get_node_center_pos()

        self.label = QGraphicsSimpleTextItem(self)  # 用于显示结点描述的文本图元
        self.label.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.label.setFlag(QGraphicsItem.ItemIsMovable, True)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei UI")
        font.setPointSize(8)
        self.label.setFont(font)
        self.setZValue(-1000.0)

    def intersection_point_ellipse_line(self, ellipse: QGraphicsEllipseItem, line: QLineF):
        a = 25
        b = 15
        x0 = ellipse.get_node_center_pos().x()
        y0 = ellipse.get_node_center_pos().y()
        # 非垂直
        if line.x2() != line.x1():
            yaxis_intersection = QPointF()
            line.intersect(QLineF(QPointF(0, 1000000), QPointF(0, -1000000)), yaxis_intersection)
            k = (line.y2() - line.y1()) / (line.x2() - line.x1())
            m = yaxis_intersection.y() + k * x0 - y0

            A = b * b + a * a * k * k
            B = 2 * a * a * k * m
            C = a * a * (m * m - b * b)

            Q = B * B - 4 * A * C
            if Q < 0:
                Q = 0
            s1 = (-1) * (B + math.sqrt(Q)) / (2 * A)
            s2 = (math.sqrt(Q) - B) / (2 * A)

            ps1 = QPointF(s1 + x0, k * s1 + m + y0)
            ps2 = QPointF(s2 + x0, k * s2 + m + y0)

            if QLineF(line.p1(), ps1).length() <= QLineF(line.p1(), ps2).length():
                return ps1
            else:
                return ps2
        else:
            if math.fabs(line.y1()) < math.fabs(line.y2()):
                return QPointF(x0, y0 - 15)
            else:
                return QPointF(x0, y0 + 15)

    def update_position(self):
        self.p1 = self.source_item.get_node_center_pos()
        self.p2 = self.target_item.get_node_center_pos()
        self.path().setElementPositionAt(0, self.p1.x(), self.p1.y())
        self.path().setElementPositionAt(self.path().elementCount() - 1, self.p2.x(), self.p2.y())
        self.update()

    def get_control_point(self):
        x1 = self.p1.x()
        y1 = self.p1.y()
        x2 = self.p2.x()
        y2 = self.p2.y()

        angle = math.pi / 12

        if x1 == x2:
            half_len = math.fabs(y2 - y1) / 2
            l = half_len * math.tan(angle)
            if y1 > y2:
                return QPointF(x1 + l, (y1 + y2) / 2)
            else:
                return QPointF(x1 - l, (y1 + y2) / 2)
        elif y1 == y2:
            half_len = math.fabs(x2 - x1) / 2
            l = half_len * math.tan(angle)
            if x2 > x1:
                return QPointF((x1 + x2) / 2, y1 + l)
            else:
                return QPointF((x1 + x2) / 2, y1 - l)
        else:
            mid_point = (self.p1 + self.p2) / 2
            k = (x1 - x2) / (y2 - y1)
            b = mid_point.y() - k * mid_point.x()
            half_len = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2) / 2
            l = half_len * math.tan(angle)
            # l = half_len * 2 / 5
            alpha = math.atan(k)
            if alpha > math.pi / 2:
                alpha = math.pi - alpha
            if alpha < 0:
                alpha = -alpha
            offset_x = l * math.cos(alpha)
            offset_y = l * math.sin(alpha)
            if (x1 < x2) and (y1 > y2):
                return QPointF(mid_point.x() + offset_x, mid_point.y() + offset_y)
            elif (x1 < x2) and (y1 < y2):
                return QPointF(mid_point.x() - offset_x, mid_point.y() + offset_y)
            elif (x1 > x2) and (y1 > y2):
                return QPointF(mid_point.x() + offset_x, mid_point.y() - offset_y)
            elif (x1 > x2) and (y1 < y2):
                return QPointF(mid_point.x() - offset_x, mid_point.y() - offset_y)

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        # 画弧线
        if self.source_item == self.target_item:
            # rect, cross_p1, cross_p2 = self.get_self_circle_args()
            path = QPainterPath()
            path.moveTo(self.p1)
            circle_center = self.p1 + QPointF(30, 0)
            r = 15
            if self.show_first_flag:
                path.addEllipse(QRectF(circle_center - QPointF(r, r), circle_center + QPointF(r, r)))
            self.setPath(path)
            paint_path = self.path()
            last_line = QLineF(circle_center + QPointF(r, 30), circle_center + QPointF(r, -2))
            head_point = circle_center + QPointF(r, -2)
        else:
            if self.arc_flag:
                    path = QPainterPath()
                    path.moveTo(self.p1)
                    control_point = self.get_control_point()
                    path.quadTo(control_point, self.p2)

                    self.setPath(path)
                    paint_path = self.path()
                    last_line = QLineF(control_point, self.p2)
                    head_point = self.intersection_point_ellipse_line(self.target_item, last_line)
            # 画直线
            else:
                path = QPainterPath()
                path.moveTo(self.p1)
                path.lineTo(self.p2)
                self.setPath(path)
                paint_path = self.path()
                elems = paint_path.elementCount()

                last_line = QLineF(paint_path.elementAt(0).x, paint_path.elementAt(0).y,
                                   paint_path.elementAt(elems - 1).x, paint_path.elementAt(elems - 1).y)
                head_point = self.intersection_point_ellipse_line(self.target_item, last_line)

        angle = math.acos(last_line.dx() * 1.0 / last_line.length())
        if last_line.dy() >= 0:
            angle = 2 * math.pi - angle

        p1 = head_point + QPointF(math.sin(angle - self.ARC_ANGLE) * self.ARC_SIZE,
                                  math.cos(angle - self.ARC_ANGLE) * self.ARC_SIZE)

        p3 = head_point + QPointF(math.sin(angle - math.pi + self.ARC_ANGLE) * self.ARC_SIZE,
                                  math.cos(angle - math.pi + self.ARC_ANGLE) * self.ARC_SIZE)

        line = QLineF(head_point.x(), head_point.y(), last_line.x1(), last_line.y1())
        p2 = line.pointAt(5.0 / (line.length() + 0.0001))
        self.arc_head.clear()
        self.arc_head.append(head_point)
        self.arc_head.append(p1)
        self.arc_head.append(p2)
        self.arc_head.append(p3)
        painter.drawPath(paint_path)
        painter.setBrush(Qt.black)
        painter.drawPolygon(self.arc_head)
        self.label.setText(str(self.weight))
        if self.p1 == self.p2:
            self.label.setPos(head_point)
        else:
            self.label.setPos(paint_path.pointAtPercent(0.5) + QPointF(-10, 0))
        self.label.setBrush(Qt.black)
        if self.p1 == self.p2 and not self.show_first_flag:
            return
        self.setPath(paint_path)
