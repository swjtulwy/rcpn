from PyQt5 import QtGui
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QPainter, QColor
from PyQt5.QtWidgets import QWidget, QAbstractItemView, QHeaderView, QTableWidget, QTableWidgetItem, QGraphicsRectItem, \
    QGraphicsScene, QGraphicsView, QGraphicsEllipseItem
from forms.reachable_graph import Ui_ReachableGraphForm
from src.analysis.marking_edge import MarkingEdge
from src.analysis.marking_node import MarkingNode


class ReachableGraph(QWidget):
    def __init__(self, flags, rg_nodes, rg_arcs, tran_map, place_map, unbounded_places, unlive_trans):
        super(ReachableGraph, self).__init__()
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        self.ui = Ui_ReachableGraphForm()
        self.ui.setupUi(self)

        self.flags = flags
        self.rg_nodes = rg_nodes
        self.rg_arcs = rg_arcs
        self.tran_map = tran_map
        self.place_map = place_map
        self.unbounded_places = unbounded_places
        self.unlive_trans = unlive_trans

        self.scale_m = 1

        self.edge_dict = {}
        self.edge_show_first = {}
        for arc in self.rg_arcs:
            self.edge_dict[arc] = []
            self.edge_show_first[arc] = True
        self.scene = QGraphicsScene(self)
        self.ui.graphicsView.setScene(self.scene)
        # self.ui.graphicsView.centerOn(-10000, -10000)

        self.ui.graphicsView.setRenderHints(QPainter.Antialiasing |  # 抗锯齿
                                            QPainter.HighQualityAntialiasing |  # 高品质抗锯齿
                                            QPainter.TextAntialiasing |  # 文字抗锯齿
                                            QPainter.SmoothPixmapTransform |  # 使图元变换更加平滑
                                            QPainter.LosslessImageRendering)  # 不失真的图片渲染
        # 视窗更新模式
        self.ui.graphicsView.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        # 设置水平和竖直方向的滚动条不显示
        self.ui.graphicsView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.ui.graphicsView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.ui.graphicsView.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        # 设置拖拽模式
        self.ui.graphicsView.setDragMode(QGraphicsView.RubberBandDrag)

        self.ui.tableWidget.setColumnCount(2)

        str_mark_head = ""
        for p in self.place_map:
            str_mark_head += "p" + str(p) + ","
        str_mark_head = str_mark_head[:-1]
        self.ui.btn_bounded.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" %
                                          (QColor(Qt.green).getRgb() if self.flags[0] else QColor(Qt.red).getRgb()))

        self.ui.btn_live.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" %
                                          (QColor(Qt.green).getRgb() if self.flags[1] else QColor(Qt.red).getRgb()))
        self.ui.btn_reverse.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" %
                                          (QColor(Qt.green).getRgb() if self.flags[2] else QColor(Qt.red).getRgb()))
        if not self.flags[0]:
            self.ui.text_unbound_places.setText(", ".join(self.unbounded_places))
        else:
            self.ui.text_unbound_places.setText("None")

        if not self.flags[1]:
            self.ui.text_unlive_trans.setText(", ".join(self.unlive_trans))
        else:
            self.ui.text_unlive_trans.setText("None")

        all_place_names = ["p" + str(i) for i in self.place_map]
        self.ui.combobox_place.addItems(all_place_names)
        self.ui.combobox_place.setCurrentIndex(-1)
        all_trans_names = ["t" + str(i) for i in self.tran_map]
        self.ui.combobox_tran.addItems(all_trans_names)
        self.ui.combobox_tran.setCurrentIndex(-1)

        self.ui.combobox_place.currentIndexChanged.connect(self.show_place_state)
        self.ui.combobox_tran.currentIndexChanged.connect(self.show_tran_state)

        self.ui.tableWidget.setHorizontalHeaderLabels(["Marking", str_mark_head])
        self.ui.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.tableWidget.setSelectionMode(QAbstractItemView.SingleSelection)
        self.ui.tableWidget.horizontalHeader().setStretchLastSection(True)
        self.ui.tableWidget.horizontalHeader().setSectionsClickable(False)
        self.ui.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.tableWidget.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.ui.tableWidget.setColumnWidth(0, 80)
        # self.ui.tableWidget.horizontalHeader().setSectionResizeMode(1, QHeaderView.Fixed)
        # self.ui.tableWidget.setColumnWidth(1, 140)
        self.ui.tableWidget.horizontalHeader().setStyleSheet("QHeaderView::section{background:skyblue;}")
        self.ui.tableWidget.verticalHeader().setVisible(False)
        self.ui.tableWidget.setShowGrid(True)
        self.ui.tableWidget.setEditTriggers(QTableWidget.NoEditTriggers)

        self.ui.tableWidget.setRowCount(len(self.rg_nodes))
        for row in range(len(self.rg_nodes)):
            self.ui.tableWidget.setItem(row, 0, QTableWidgetItem(self.rg_nodes[row].name))
            node_strs = [str('\u03c9') if x == -1 else str(x) for x in self.rg_nodes[row].marking]
            marking_str = ",".join(node_strs)
            self.ui.tableWidget.setItem(row, 1, QTableWidgetItem(marking_str))
            for i in range(2):
                self.ui.tableWidget.item(row, i).setTextAlignment(Qt.AlignCenter)

        bound_str = "bounded, " if self.flags[0] else "unbouneded, "
        live_str = "live, " if self.flags[1] else "unlive, "
        reverse_str = "reversible" if self.flags[2] else "unreversible, "
        self.ui.label.setText("The Petri Net is " + bound_str + live_str + reverse_str)
        self.init_view()

    def show_place_state(self):
        index = self.ui.combobox_place.currentIndex()
        name_str = "p" + str(index)
        if name_str in self.unbounded_places:
            self.ui.label_place_state.setText("Unbounded")
        else:
            self.ui.label_place_state.setText("Bounded")

    def show_tran_state(self):
        index = self.ui.combobox_tran.currentIndex()
        name_str = "t" + str(index)
        if name_str in self.unlive_trans:
            self.ui.label_tran_state.setText("Unlive")
        else:
            self.ui.label_tran_state.setText("Live")

    def init_view(self):
        import igraph
        graph = igraph.Graph(directed=True)
        for node in self.rg_nodes:
            graph.add_vertex(node.name)
        for arc in self.rg_arcs:
            graph.add_edge(arc.source.name, arc.target.name)
        layout = graph.layout_sugiyama(vgap=0.8, hgap=0.9)
        for l in layout:
            l[0] = l[0] * 100
            l[1] = l[1] * 100
        for i in range(len(self.rg_nodes)):
            item = MarkingNode(layout[i][0], layout[i][1], 50, 30, self.rg_nodes[i])
            self.scene.addItem(item)
        for edge in self.rg_arcs:
            source_item = self.find_node(edge.source)
            target_item = self.find_node(edge.target)
            repeat_edge_flag = self.find_edge(edge, self.rg_arcs)
            if edge.source == edge.target:
                self.edge_dict[edge] = self.get_same_edges(edge, self.rg_arcs)
                edge_item = MarkingEdge(self, source_item, target_item, ",".join(self.edge_dict[edge]),
                                        repeat_edge_flag, self.edge_show_first[edge])
                self.edge_show_first[edge] = False
            else:
                edge_item = MarkingEdge(self, source_item, target_item, edge.weight, repeat_edge_flag, False)
            source_item.out_arcs.append(edge_item)
            target_item.in_arcs.append(edge_item)
            self.scene.addItem(edge_item)

    def find_node(self, node):
        for item in self.scene.items():
            if item.type() == 4 and item.node == node:
                return item

    def get_same_edges(self, edge, edges):
        ret = []
        for edge_ in edges:
            if edge_.source == edge.source and edge_.target == edge.target:
                ret.append(edge_.weight)
        return ret

    def find_edge(self, edge, edges):
        for edge_ in edges:
            if edge_.source == edge.target and edge_.target == edge.source:
                return True
        return False

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        if event.modifiers() == Qt.ControlModifier:
            self.scale_fun1(event)
        else:
            QWidget.wheelEvent(self, event)

    def scale_fun1(self, event: QtGui.QWheelEvent):
        if event.modifiers() == Qt.ControlModifier:
            if event.angleDelta().y() > 0 and self.scale_m >= 50:
                return
            elif event.angleDelta().y() < 0 and self.scale_m <= 0.01:
                return
            else:
                scale_factor = self.ui.graphicsView.transform().m11()
                self.scale_m = scale_factor
                wheel_data_value = event.angleDelta().y()
                if wheel_data_value > 0:
                    self.ui.graphicsView.scale(1.1, 1.1)
                else:
                    self.ui.graphicsView.scale(1.0 / 1.1, 1.0 / 1.1)
                self.ui.graphicsView.update()