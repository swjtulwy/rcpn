import time


class Util:
    INIT_MODE = 0
    SELECT_MODE = 1
    PLACE_MODE = 2
    TRANS_MODE = 3
    ARC_MODE = 4
    SIM_MODE = 5

    @staticmethod
    def get_str_ruid():
        """
        获取16进制字符串唯一id
        """
        base_time = round(time.mktime(time.strptime('1970-01-02 00:00:00', '%Y-%m-%d %H:%M:%S')) * 10 ** 3)
        ruid = round(time.time() * 10 ** 3) - base_time
        time.sleep(0.001)
        return str(hex(ruid)).replace('0x', '')

    @staticmethod
    def get_int_ruid():
        """
        获取10进制整数唯一id
        """
        base_time = round(time.mktime(time.strptime('1970-01-02 00:00:00', '%Y-%m-%d %H:%M:%S')) * 10 ** 3)
        ruid = round(time.time() * 10 ** 3) - base_time
        time.sleep(0.001)
        return int(ruid)

    @staticmethod
    def str_strip_blank(string):
        return string.strip(' ')


if __name__ == '__main__':
    id16 = Util.get_str_ruid()
    print(id16)
    id10 = Util.get_int_ruid()
    print(id10)
