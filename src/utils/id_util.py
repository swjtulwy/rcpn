from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc import Arc

class Id_Util(object):
    def __init__(self):
        self.place_id_list = []
        self.place_count = 0
        self.trans_id_list = []
        self.transition_count = 0
        self.arc_id_list = []
        self.arc_count = 0

    def get_place_id(self, identity_id):
        # self.place_count += 1
        list_len = len(self.place_id_list)
        for i in range(0, list_len):
            if self.place_id_list[i] is None:
                self.place_id_list[i] = identity_id
                return i
        self.place_id_list.append(identity_id)
        return list_len

    def get_place_id_index(self, place_id):
        return self.place_id_list.index(place_id)

    def push_ids(self, items):
        place_ids = []
        trans_ids = []
        arc_ids = []

        for item in items:
            if item.Type == Place.Type:
                place_ids.append(int(item.place_id))
            elif item.Type == Transition.Type:
                trans_ids.append(int(item.transition_id))
            elif item.Type == Arc.Type:
                arc_ids.append(int(item.arc_id))

        max_place_len = 0 if len(place_ids) == 0 else max(place_ids)
        for index in range(0, max_place_len + 1):
            self.place_id_list.insert(index, None)
        max_trans_len = 0 if len(trans_ids) == 0 else max(trans_ids)
        for index in range(0, max_trans_len + 1):
            self.trans_id_list.insert(index, None)
        max_arcs_len = 0 if len(arc_ids) == 0 else max(arc_ids)
        for index in range(0, max_arcs_len + 1):
            self.arc_id_list.insert(index, None)

        for item in items:
            if item.Type == Place.Type:
                self.place_id_list[int(item.place_id)] = int(item.identity_id)
            elif item.Type == Transition.Type:
                self.trans_id_list[int(item.transition_id)] = int(item.identity_id)
            elif item.Type == Arc.Type:
                self.arc_id_list[int(item.arc_id)] = int(item.identity_id)

    def get_transition_id(self, identity_id):
        # self.transition_count += 1
        list_len = len(self.trans_id_list)
        for i in range(0, list_len):
            if self.trans_id_list[i] is None:
                self.trans_id_list[i] = identity_id
                return i
        self.trans_id_list.append(identity_id)
        return list_len

    def get_transition_id_index(self, transition_id):
        return self.trans_id_list.index(transition_id)

    def get_arc_id(self, arc_id):
        # self.arc_count += 1
        list_len = len(self.arc_id_list)
        for i in range(0, list_len):
            if self.arc_id_list[i] is None:
                self.arc_id_list[i] = arc_id
                return i
        self.arc_id_list.append(arc_id)
        return list_len

    def get_arc_id_index(self, arc_id):
        return self.arc_id_list.index(arc_id)

if __name__ == "__main__":
    li = []
    li.insert(1, 2)
    li.insert(3, 4)
    print(len(li))
    for i in range(len(li)):
        print(li[i])