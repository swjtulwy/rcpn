import logging

# create logger
logger_name = "rcpn_log"
logger = logging.getLogger(logger_name)
logger.setLevel(logging.DEBUG)

# logging.basicConfig(level=logging.DEBUG)

# create file handler
log_path = "../../log/rcpnlog.log"
file_handler = logging.FileHandler(log_path)
file_handler.setLevel(logging.WARN)

# create formatter
fmt = "%(asctime)-15s %(levelname)s %(filename)s %(lineno)d %(process)d %(message)s"
date_fmt = "%a %d %b %Y %H:%M:%S"
formatter = logging.Formatter(fmt, date_fmt)

# add handler and formatter to logger
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# print log info

if __name__ == '__main__':
    logger.debug('debug message')
    logger.info('info message')
    logger.warning('warn message')
    logger.error('error message')
    logger.critical('critical message')