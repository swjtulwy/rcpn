from PyQt5.QtGui import QColor


class XmlPlaceAttr:
    """
    库所在xml文件中保存的属性
    """
    def __init__(self):
        self.id = ""
        self.identity_id = ""
        self.name = ""
        self.x = 0                # 本图元的坐标, 相对父图元, 没有父图元则相对scene
        self.y = 0
        self.initmark = 0         # 初始标记
        self.capacity = 0         # 容量
        self.offsetx = 0          # 文本描述图元偏移
        self.offsety = 0
        self.comment = ""
        self.show = False
        self.brushColor = QColor()
        self.penColor = QColor()


class XmlTransitionAttr:
    """
    变迁子xml文件中保存的属性
    """
    def __init__(self):
        self.id = ""
        self.identity_id = ""
        self.name = ""
        self.x = 0
        self.y = 0
        self.rotation = 0
        self.offsetx = 0
        self.offsety = 0
        self.function = ""
        self.comment = ""
        self.show = False
        self.brushColor = QColor()
        self.penColor = QColor()


class XmlArcAttr:
    """
    弧在xml文件中保存的属性
    """
    def __init__(self):
        self.id = ""
        self.identity_id = ""
        self.source = ""
        self.target = ""
        self.weight = 0
        self.points = []
        self.brushColor = QColor()
        self.penColor = QColor()


class XmlPageAttr:
    """
    Page指的是
    """
    def __init__(self):
        self.id = ""
        self.name = ""
        self.placeNodes = []
        self.transitionNodes = []
        self.arcs = []


class XmlPetriNetAttr:
    """
    Petri网在xml文件中保存的结构
    """
    def __init__(self):
        self.id = ""
        self.name = ""
        self.pages = []


class XmlColorAttr:
    """
    颜色在xml文件中保存的结构
    """
    def __init__(self):
        self.red = 0
        self.green = 0
        self.blue = 0



