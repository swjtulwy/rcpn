from PyQt5.QtCore import QXmlStreamReader, qDebug, QPointF

from src.serialization.xml_attribute import *


class XmlParser(object):
    def __init__(self):
        self.net = XmlPetriNetAttr()
        self.name = ""
        self.id = ""
        self.identity_id = ""
        self.source = ""
        self.target = ""
        self.pages = []
        self.offsetx = 0
        self.offsety = 0
        self.x = 0
        self.y = 0
        self.capacity = 0
        self.rotation = 0
        self.weight = 0
        self.comment = ""
        self.show = ""
        self.initmark = 0
        self.points = []
        self.pen = XmlColorAttr()
        self.brush = XmlColorAttr()
        self.function = ""

    def getNetFromXML(self):
        return self.net

    def parseXML(self, xmlContent):
        xml = QXmlStreamReader(xmlContent)
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.ProcessingInstruction:
                continue
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "pnml":
                    continue
                if xml.name() == "net":
                    if xml.attributes().hasAttribute("id"):
                        self.net.id = xml.attributes().value("id")
                        if not self.parseXML_Net(xml):
                            return False
                        continue

        if xml.hasError():
            qDebug("XML-Pseudocode parsing error in xmlContent:" + xml.errorString())
            return False

        return True

    def parseXML_Net(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "net":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "name":
                    if not self.parseXML_Name(xml):
                        return False
                    self.net.name = self.name
                    continue

                if xml.name() == "page":
                    page = XmlPageAttr()
                    if xml.attributes().hasAttribute("id"):
                        page.id = xml.attributes().value("id")
                        if not self.parseXML_Page(xml, page):
                            return False
                        continue

        return True

    def parseXML_Page(self, xml, page):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "page":
                self.net.pages.append(page)
                break

            if token == QXmlStreamReader.StartElement:
                if xml.name() == "name":
                    if not self.parseXML_Name(xml):
                        return False
                    page.name = self.name
                    continue
                if xml.name() == "place":
                    if xml.attributes().hasAttribute("id"):
                        self.id = xml.attributes().value("id")
                        self.identity_id = xml.attributes().value("identity_id")
                        if not self.parseXML_Place(xml, page):
                            return False
                        continue
                if xml.name() == "transition":
                    if xml.attributes().hasAttribute("id"):
                        self.id = xml.attributes().value("id")
                        self.identity_id = xml.attributes().value("identity_id")
                        if not self.parseXML_Transition(xml, page):
                            return False
                        continue
                if xml.name() == "arc":
                    if xml.attributes().hasAttribute("id") and xml.attributes().hasAttribute("source") and \
                            xml.attributes().hasAttribute("target"):
                        self.id = xml.attributes().value("id")
                        self.identity_id = xml.attributes().value("identity_id")
                        self.source = xml.attributes().value("source")
                        self.target = xml.attributes().value("target")

                        if not self.parseXML_Arc(xml, page):
                            return False
                        continue
                continue

        if xml.hasError():
            qDebug("XML-Pseudocode parsing error in xml page:" + xml.errorString())
            return False

        return True

    def parseXML_Name(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "name":
                break

            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    self.name = self.getElementData(xml)
                    continue
                elif xml.name() == "graphics":
                    if not self.parseXML_Graphics(xml):
                        return False
                    continue
        return True

    def getElementData(self, xml):
        if xml.tokenType() != QXmlStreamReader.StartElement:
            return ""
        xml.readNext()
        if xml.tokenType() != QXmlStreamReader.Characters:
            return ""
        return xml.text()

    def parseXML_Graphics(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "graphics":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "offset":
                    self.offsetx = float(xml.attributes().value("x"))
                    self.offsety = float(xml.attributes().value("y"))
                    continue
                elif xml.name() == "position":
                    self.x = float(xml.attributes().value("x"))
                    self.y = float(xml.attributes().value("y"))
                    continue

        return True

    def parseXML_Place(self, xml, page):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "place":
                place = XmlPageAttr()
                place.id = self.id
                place.identity_id = self.identity_id
                place.name = self.name
                place.x = self.x
                place.y = self.y
                place.offsetx = self.offsetx
                place.offsety = self.offsety
                place.initmark = self.initmark
                place.capacity = self.capacity
                place.comment = self.comment
                place.show = (self.show == "True")
                brushColor = QColor(self.brush.red, self.brush.green, self.brush.blue)
                penColor = QColor(self.pen.red, self.pen.green, self.pen.blue)
                place.brushColor = brushColor
                place.penColor = penColor
                page.placeNodes.append(place)
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "name":
                    if not self.parseXML_Name(xml):
                        return False
                    continue
                elif xml.name() == "graphics":
                    if not self.parseXML_Graphics(xml):
                        return False
                    continue
                elif xml.name() == "initialMarking":
                    if not self.parseXML_InitialMarking(xml):
                        return False
                    continue
                elif xml.name() == "toolspecific":
                    if not self.parseXML_ToolSpecific(xml):
                        return False
                    continue
                elif xml.name() == "comment":
                    if not self.parseXML_Comment(xml):
                        return False
                    continue
                elif xml.name() == "color":
                    if not self.parseXML_Color(xml):
                        return False
                    continue

        return True

    def parseXML_Transition(self, xml, page):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "transition":
                transition = XmlTransitionAttr()
                transition.id = self.id
                transition.identity_id = self.identity_id
                transition.name = self.name
                transition.x = self.x
                transition.y = self.y
                transition.offsetx = self.offsetx
                transition.offsety = self.offsety
                transition.rotation = self.rotation
                transition.function = self.function
                transition.comment = self.comment
                transition.show = (self.show == "True")
                brushColor = QColor(self.brush.red, self.brush.green, self.brush.blue)
                penColor = QColor(self.pen.red, self.pen.green, self.pen.blue)
                transition.brushColor = brushColor
                transition.penColor = penColor
                page.transitionNodes.append(transition)
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "name":
                    if not self.parseXML_Name(xml):
                        return False
                    continue
                elif xml.name() == "graphics":
                    if not self.parseXML_Graphics(xml):
                        return False
                    continue
                elif xml.name() == "function":
                    if not self.parseXML_Function(xml):
                        return False
                    continue
                elif xml.name() == "comment":
                    if not self.parseXML_Comment(xml):
                        return False
                    continue
                elif xml.name() == "color":
                    if not self.parseXML_Color(xml):
                        return False
                    continue
        return True

    def parseXML_Arc(self, xml, page):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "arc":
                arc = XmlArcAttr()
                arc.id = self.id
                arc.identity_id = self.identity_id
                arc.source = self.source
                arc.target = self.target
                arc.points.extend(self.points)
                arc.weight = self.weight
                brushColor = QColor(self.brush.red, self.brush.green, self.brush.blue)
                penColor = QColor(self.pen.red, self.pen.green, self.pen.blue)
                arc.brushColor = brushColor
                arc.penColor = penColor
                self.points.clear()
                page.arcs.append(arc)
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "inscription":
                    if not self.parseXML_Inscription(xml):
                        return False
                    continue
                if xml.name() == "expression":
                    if not self.parseXML_Expression(xml):
                        return False
                    continue
                if xml.name() == "graphics":
                    if not self.parseXML_Positions(xml):
                        return False
                    continue
                elif xml.name() == "color":
                    if not self.parseXML_Color(xml):
                        return False
                    continue
        return True

    def parseXML_Expression(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "expression":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    continue
        return True

    def parseXML_Color(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "color":
                break
            if token == QXmlStreamReader.StartElement:
                if token == QXmlStreamReader.StartElement:
                    if xml.name() == "brush":
                        self.brush.red = int(xml.attributes().value("red"))
                        self.brush.green = int(xml.attributes().value("green"))
                        self.brush.blue = int(xml.attributes().value("blue"))
                        continue
                    elif xml.name() == "pen":
                        self.pen.red = int(xml.attributes().value("red"))
                        self.pen.green = int(xml.attributes().value("green"))
                        self.pen.blue = int(xml.attributes().value("blue"))
                        continue
        return True

    def parseXML_InitialMarking(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "initialMarking":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    self.initmark = float(self.getElementData(xml))
                elif xml.name() == "graphics":
                    if not self.parseXML_Graphics(xml):
                        return False
                    continue
        return True

    def parseXML_Inscription(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "inscription":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    self.weight = int(self.getElementData(xml))
        return True

    def parseXML_Positions(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "graphics":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "position":
                    self.x = float(xml.attributes().value("x"))
                    self.y = float(xml.attributes().value("y"))
                    self.points.append(QPointF(self.x, self.y))
                    continue
        return True

    def parseXML_ToolSpecific(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "toolspecific":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "placeCapacity":
                    self.capacity = int(xml.attributes().value("capacity"))
                    continue
                elif xml.name() == "rotation":
                    self.rotation = int(xml.attributes().value("degree"))
                    continue

        return True

    def parseXML_Comment(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "comment":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    self.comment = self.getElementData(xml)
                    continue
                elif xml.name() == "show":
                    self.show = self.getElementData(xml)
                    continue
        return True

    def parseXML_Function(self, xml):
        while (not xml.atEnd()) and (not xml.hasError()):
            token = xml.readNext()
            if token == QXmlStreamReader.EndElement and xml.name() == "function":
                break
            if token == QXmlStreamReader.StartElement:
                if xml.name() == "text":
                    self.function = self.getElementData(xml)
                    continue
        return True
