from PyQt5.QtCore import QXmlStreamWriter, qDebug


class XmlWriter(object):
    def __init__(self, net):
        self.ptnet = net

    def writeXML(self, file):
        xml = QXmlStreamWriter(file)
        xml.setCodec("GBK")
        xml.setAutoFormatting(True)
        xml.writeStartDocument()
        xml.writeStartElement("pnml")

        xml.writeStartElement("net")
        xml.writeAttribute("id", str(self.ptnet.id))
        xml.writeAttribute("type", "http://www.pnml.org/version-2009/grammar/ptnet")

        xml.writeStartElement("name")
        xml.writeTextElement("text", str(self.ptnet.name))
        xml.writeEndElement()

        for page in self.ptnet.pages:
            if not self.writePage(xml, page):
                qDebug("Error ...")

        xml.writeEndElement()
        xml.writeEndElement()
        xml.writeEndDocument()

        if xml.hasError():
            return False
        return True

    def writePage(self, xml, page):
        xml.writeStartElement("page")
        xml.writeAttribute("id", page.id)

        xml.writeStartElement("name")
        xml.writeTextElement("text", page.name)
        xml.writeEndElement()

        for place in page.placeNodes:
            if not self.writePlaceNode(xml, place):
                qDebug("Error ...")

        for transition in page.transitionNodes:
            if not self.writeTransitionNode(xml, transition):
                qDebug("Error ...")

        for arc in page.arcs:
            if not self.writeArc(xml, arc):
                qDebug("Error ...")

        xml.writeEndElement()
        if xml.hasError():
            return False
        return True

    def writePlaceNode(self, xml, place):
        xml.writeStartElement("place")
        xml.writeAttribute("id", str(place.id))
        xml.writeAttribute("identity_id", str(place.identity_id))

        # name
        xml.writeStartElement("name")
        xml.writeTextElement("text", place.name)
        xml.writeStartElement("graphics")
        xml.writeEmptyElement("offset")
        xml.writeAttribute("x", str(place.offsetx))
        xml.writeAttribute("y", str(place.offsety))
        xml.writeEndElement()
        xml.writeEndElement()

        # color
        [bRed, bGreen, bBlue, _] = place.brushColor.getRgb()
        [pRed, pGreen, pBlue, _] = place.penColor.getRgb()
        xml.writeStartElement("color")
        xml.writeEmptyElement("brush")
        xml.writeAttribute("red", str(bRed))
        xml.writeAttribute("green", str(bGreen))
        xml.writeAttribute("blue", str(bBlue))
        xml.writeEmptyElement("pen")
        xml.writeAttribute("red", str(pRed))
        xml.writeAttribute("green", str(pGreen))
        xml.writeAttribute("blue", str(pBlue))
        xml.writeEndElement()

        # toolspecific
        xml.writeStartElement("toolspecific")
        xml.writeAttribute("tool", "petrinet")
        xml.writeAttribute("version", "1.0")
        xml.writeEmptyElement("placeCapacity")
        xml.writeAttribute("capacity", str(place.capacity))
        xml.writeEndElement()

        xml.writeStartElement("graphics")
        xml.writeEmptyElement("position")
        xml.writeAttribute("x", str(place.x))
        xml.writeAttribute("y", str(place.y))
        xml.writeEndElement()

        xml.writeStartElement("initialMarking")
        xml.writeTextElement("text", str(place.initmark))
        xml.writeEndElement()

        xml.writeStartElement("comment")
        xml.writeTextElement("text", str(place.comment))
        xml.writeTextElement("show", str(place.show))
        xml.writeEndElement()

        xml.writeEndElement()

        if xml.hasError():
            return False
        return True

    def writeTransitionNode(self, xml, transition):
        xml.writeStartElement("transition")
        xml.writeAttribute("id", str(transition.id))
        xml.writeAttribute("identity_id", str(transition.identity_id))

        # name
        xml.writeStartElement("name")
        xml.writeTextElement("text", transition.name)
        xml.writeStartElement("graphics")
        xml.writeEmptyElement("offset")
        xml.writeAttribute("x", str(transition.offsetx))
        xml.writeAttribute("y", str(transition.offsety))
        xml.writeEndElement()
        xml.writeEndElement()

        # color
        [bRed, bGreen, bBlue, _] = transition.brushColor.getRgb()
        [pRed, pGreen, pBlue, _] = transition.penColor.getRgb()
        xml.writeStartElement("color")
        xml.writeEmptyElement("brush")
        xml.writeAttribute("red", str(bRed))
        xml.writeAttribute("green", str(bGreen))
        xml.writeAttribute("blue", str(bBlue))
        xml.writeEmptyElement("pen")
        xml.writeAttribute("red", str(pRed))
        xml.writeAttribute("green", str(pGreen))
        xml.writeAttribute("blue", str(pBlue))
        xml.writeEndElement()

        # toolspecific
        xml.writeStartElement("toolspecific")
        xml.writeAttribute("tool", "petrinet")
        xml.writeAttribute("version", "1.0")
        xml.writeEmptyElement("rotation")
        xml.writeAttribute("degree", str(transition.rotation))
        xml.writeEndElement()

        xml.writeStartElement("graphics")
        xml.writeEmptyElement("position")
        xml.writeAttribute("x", str(transition.x))
        xml.writeAttribute("y", str(transition.y))
        xml.writeEndElement()

        xml.writeStartElement("function")
        xml.writeTextElement("text", str(transition.function))
        xml.writeEndElement()

        xml.writeStartElement("comment")
        xml.writeTextElement("text", str(transition.comment))
        xml.writeTextElement("show", str(transition.show))
        xml.writeEndElement()

        xml.writeEndElement()

        if xml.hasError():
            return False
        return True

    def writeArc(self, xml, arc):
        xml.writeStartElement("arc")

        xml.writeAttribute("id", str(arc.id))
        xml.writeAttribute("identity_id", str(arc.identity_id))
        xml.writeAttribute("source", str(arc.source))
        xml.writeAttribute("target", str(arc.target))

        xml.writeStartElement("inscription")
        xml.writeTextElement("text", str(arc.weight))
        xml.writeEndElement()

        if arc.points:
            xml.writeStartElement("graphics")
            for p in arc.points:
                xml.writeEmptyElement("position")
                xml.writeAttribute("x", str(p.x))
                xml.writeAttribute("y", str(p.y))
            xml.writeEndElement()

        # color
        [bRed, bGreen, bBlue, _] = arc.brushColor.getRgb()
        [pRed, pGreen, pBlue, _] = arc.penColor.getRgb()
        xml.writeStartElement("color")
        xml.writeEmptyElement("brush")
        xml.writeAttribute("red", str(bRed))
        xml.writeAttribute("green", str(bGreen))
        xml.writeAttribute("blue", str(bBlue))
        xml.writeEmptyElement("pen")
        xml.writeAttribute("red", str(pRed))
        xml.writeAttribute("green", str(pGreen))
        xml.writeAttribute("blue", str(pBlue))
        xml.writeEndElement()

        xml.writeEndElement()

        if xml.hasError():
            return False
        return True
