class Rule(object):
    def __init__(self, id, name, path, lhs, rhs, input_lhs, output_lhs, input_rhs, output_rhs):
        self.id = id
        self.name = name
        self.path = path
        self.lhs = lhs
        self.rhs = rhs
        self.input_lhs = input_lhs
        self.output_lhs = output_lhs
        self.input_rhs = input_rhs
        self.output_rhs = output_rhs

    def to_json(self):
        json_str = {
                    "Id": str(self.id),
                    "Name": self.name,
                    "Path": self.path,
                    "Lhs": self.lhs,
                    "Rhs": self.rhs,
                    "Input_Lhs": self.input_lhs,
                    "Output_Lhs": self.output_lhs,
                    "Input_Rhs": self.input_rhs,
                    "Output_Rhs": self.output_rhs
                    }
        return json_str
