import json

import igraph
from PyQt5.QtCore import QFile, QTextStream, QIODevice, QDir
from PyQt5.QtWidgets import QMessageBox, QDialog

from src.basic.arc import Arc
from src.basic.multi_match_dialog import MultiMatchDialog
from src.basic.place import Place
from src.basic.transition import Transition
from src.serialization.xml_parser import XmlParser


class Reconfigure:
    def __init__(self, rule):
        self.multi_match_dialog = None
        self.rule = rule

    def get_net_from_file(self, url):
        file = QFile(url)
        if not file.open(QIODevice.ReadOnly | QIODevice.Text):
            return
        file.seek(0)
        text_stream = QTextStream(file)
        xml_content = text_stream.readAll()
        file.close()
        parser = XmlParser()
        if not parser.parseXML(xml_content):
            return None
        net = parser.getNetFromXML()
        pages = net.pages
        return pages

    def get_id_by_name_in_page(self, pages, names):
        places = []
        transitions = []
        ids = []
        for page in pages:
            places = page.placeNodes
            transitions = page.transitionNodes
        for name in names:
            if name.startswith("p"):
                for place in places:
                    if place.name == name:
                        ids.append(place.identity_id)
            elif name.startswith("t"):
                for trans in transitions:
                    if trans.name == name:
                        ids.append(trans.identity_id)
        return ids

    def get_inner_arcs_by_nodes(self, scene, subgraph_nodes_ids):
        arcs = []
        ret_arcs = []
        ret_arc_pairs = []
        for item in scene.items():
            if item.Type == Arc.Type:
                arcs.append(item)
        for subgraph_nodes_id in subgraph_nodes_ids:
            temp = []
            temp2 = []
            for arc in arcs:
                if str(arc.source_item.identity_id) in subgraph_nodes_id and str(
                        arc.target_item.identity_id) in subgraph_nodes_id:
                    temp.append(arc)
                    temp2.append((arc.source_item.name, arc.target_item.name))
            ret_arcs.append(temp)
            ret_arc_pairs.append(temp2)
        return ret_arcs, ret_arc_pairs

    def match(self, scene, l_url):
        # 原网建图
        big_graph = igraph.Graph(directed=True)
        nodes = scene.items()
        vertex = []
        g_type = []
        for n in nodes:
            if n.Type == Place.Type:
                g_type.append(0)
                vertex.append(str(n.identity_id))
            if n.Type == Transition.Type:
                g_type.append(1)
                vertex.append(str(n.identity_id))
        big_graph.add_vertices(vertex)
        edges = []
        for n in nodes:
            if n.Type == Arc.Type:
                edges.append((str(n.source_item.identity_id), str(n.target_item.identity_id)))
        big_graph.add_edges(edges)
        sub_graph = igraph.Graph(directed=True)
        # 左手侧建图
        lhs_graph_vertex = []
        lhs_graph_edges = []
        pages = self.get_net_from_file(l_url)
        if pages is None:
            return None
        lhs_type = []
        for page in pages:
            for p in page.placeNodes:
                lhs_type.append(0)
                lhs_graph_vertex.append(p.identity_id)
            for t in page.transitionNodes:
                lhs_type.append(1)
                lhs_graph_vertex.append(t.identity_id)
            for a in page.arcs:
                lhs_graph_edges.append((a.source, a.target))
        sub_graph.add_vertices(lhs_graph_vertex)
        sub_graph.add_edges(lhs_graph_edges)
        # 子图同构匹配
        is_match = big_graph.subisomorphic_vf2(sub_graph, color1=g_type, color2=lhs_type)
        # 获得匹配的子图结构
        ret_map = big_graph.get_subisomorphisms_vf2(sub_graph, color1=g_type, color2=lhs_type)
        all_subgraph_nodes_id = []  # 匹配的原网中子网部分的节点id
        all_subgraph_nodes = []  # 匹配的原网中子网部分的节点(item)
        map_back = ret_map  # 算法匹配结果的映射字典
        new_map = []
        dup_indices = []
        i = 0
        for m in map_back:
            m = sorted(m)
            if m not in new_map:
                new_map.append(m)
            else:
                dup_indices.append(i)
            i = i + 1
        ret_map = [ret_map[i] for i in range(len(ret_map)) if (i not in dup_indices)]
        for m in ret_map:
            subgraph_nodes_id = []
            subgraph_nodes = []
            for i in m:
                subgraph_nodes_id.append(vertex[i])
                subgraph_nodes.append(scene.get_item_by_id(vertex[i]))
            all_subgraph_nodes_id.append(subgraph_nodes_id)
            all_subgraph_nodes.append(subgraph_nodes)

        return is_match, all_subgraph_nodes_id, all_subgraph_nodes, lhs_graph_vertex

    def apply(self, scene, lhs_path):
        # 是否匹配, 匹配的子网id, 匹配的子网结点, 左手侧id列表
        is_match, subgraph_nodes_ids, subgraph_nodes, lhs_id_list = self.match(scene, lhs_path)
        if len(subgraph_nodes_ids) < 1 or not is_match:
            return False
        # 获取匹配的子网内部弧
        arcs, arc_pairs = self.get_inner_arcs_by_nodes(scene, subgraph_nodes_ids)
        # 多重匹配的情况
        if len(subgraph_nodes_ids) > 1:
            self.multi_match_dialog = MultiMatchDialog(parent=scene.parent().view.main_win,
                                                       subgraph_list=subgraph_nodes.copy(), arcs=arcs.copy())
            res = self.multi_match_dialog.exec()
            if res == QDialog.Rejected:
                return

        # multi_index 由用户选择了, 默认是0
        subgraph_nodes_id_selected = subgraph_nodes_ids[scene.parent().view.main_win.multi_index]
        subgraph_nodes_selected = subgraph_nodes[scene.parent().view.main_win.multi_index]

        # 获取左右手侧的item
        lhs_pages = self.get_net_from_file(QDir.currentPath() + "/examples/" + self.rule["Lhs"].split('/')[-1])
        rhs_pages = self.get_net_from_file(QDir.currentPath() + "/examples/" + self.rule["Rhs"].split('/')[-1])

        rule_path = QDir.currentPath() + "/rules/" + self.rule["Path"].split('/')[-1]
        # 获取左右手侧的接口结点名字
        with open(rule_path, 'r') as json_file:
            rule_json_str = json.load(json_file)
        rule_json_str = json.dumps(rule_json_str)
        try:
            rule_object = json.loads(rule_json_str)
        except ValueError:
            return
        lhs_input_names = rule_object["Input_Lhs"].split(",")
        lhs_output_names = rule_object["Output_Lhs"].split(",")
        rhs_input_names = rule_object["Input_Rhs"].split(",")
        rhs_output_names = rule_object["Output_Rhs"].split(",")

        # 左手侧中的输入输出接口identity_id, 用于寻找索引, 后续作用不大
        lhs_input_ids = self.get_id_by_name_in_page(lhs_pages, lhs_input_names)
        lhs_output_ids = self.get_id_by_name_in_page(lhs_pages, lhs_output_names)

        # 这个索引对于原网匹配子网和左手侧的位置 是一致的
        input_map_index = lhs_id_list.index(lhs_input_ids[0])  # 左手侧中输入接口的位置索引
        output_map_index = lhs_id_list.index(lhs_output_ids[0])  # 左手侧中输出接口的位置索引

        net_input_ids = [subgraph_nodes_id_selected[input_map_index]]  # 原网中匹配子网输入接口的identity_id
        net_output_ids = [subgraph_nodes_id_selected[output_map_index]]  # 原网中匹配子网输出接口的identity_id

        input_pre_set = subgraph_nodes_selected[input_map_index].get_pre_set()
        input_post_set = subgraph_nodes_selected[input_map_index].get_post_set()
        output_pre_set = subgraph_nodes_selected[output_map_index].get_pre_set()
        output_post_set = subgraph_nodes_selected[output_map_index].get_post_set()

        for pre_id in input_pre_set[:]:
            if pre_id in subgraph_nodes_id_selected:
                input_pre_set.remove(pre_id)
        for post_id in input_post_set[:]:
            if post_id in subgraph_nodes_id_selected:
                input_post_set.remove(post_id)
        for pre_id in output_pre_set[:]:
            if pre_id in subgraph_nodes_id_selected:
                output_pre_set.remove(pre_id)
        for post_id in output_post_set[:]:
            if post_id in subgraph_nodes_id_selected:
                output_post_set.remove(post_id)

        net_input_nodes = []  # 原网的输入结点
        net_output_node = []  # 原网的输出结点

        subgraph_nodes_selected_ultimate = []  # 最终要在原网中删除的结点 (item形式)
        for node in subgraph_nodes_selected:
            if str(node.identity_id) in net_input_ids:
                net_input_nodes.append(node)
            elif str(node.identity_id) in net_output_ids:
                net_output_node.append(node)
            else:
                subgraph_nodes_selected_ultimate.append(node)

        pre_post_arcs_weight = {}
        for pre_node_id in input_pre_set:
            pre_post_arcs_weight[str(pre_node_id) + "+" + str(net_input_ids[0])] \
                = scene.find_arc(pre_node_id, net_input_ids[0]).weight
        for post_node_id in input_post_set:
            pre_post_arcs_weight[str(net_input_ids[0]) + "+" + str(post_node_id)] \
                = scene.find_arc(net_input_ids[0], post_node_id).weight
        for pre_node_id in output_pre_set:
            pre_post_arcs_weight[str(pre_node_id) + "+" + str(net_output_ids[0])] \
                = scene.find_arc(pre_node_id, net_output_ids[0]).weight
        for post_node_id in output_post_set:
            pre_post_arcs_weight[str(net_output_ids[0]) + "+" + str(post_node_id)] \
                = scene.find_arc(net_output_ids[0], post_node_id).weight
        # 删除子网, 参数为Item类型列表
        scene.del_sub_graph(subgraph_nodes_selected)

        rhs_input_ids = self.get_id_by_name_in_page(rhs_pages, rhs_input_names)
        rhs_output_ids = self.get_id_by_name_in_page(rhs_pages, rhs_output_names)

        # 添加右手侧
        scene.add_rhs_graph(rhs_pages, input_pre_set, input_post_set, output_pre_set, output_post_set, rhs_input_ids,
                            rhs_output_ids, net_input_ids, net_output_ids, pre_post_arcs_weight)

        self.clear_tran_state(scene)


    def clear_tran_state(self, scene):
        for item in scene.items():
            if item.Type == Transition.Type:
                item.fired_times = 0
