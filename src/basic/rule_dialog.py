
from PyQt5.QtCore import QDir, QMimeData, QModelIndex, Qt, QFile
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QDialog, QCheckBox, QListWidgetItem, QFileDialog, QMessageBox
from forms.rule_edit_dialog import Ui_RuleEditDialog
from src.utils.util import Util
from src.basic.rule import Rule
import json

class RuleDialog(QDialog):
    def __init__(self, parent, edit_flag=False, rule_infos=None):
        super(RuleDialog, self).__init__()
        self.parent = parent
        self.ui = Ui_RuleEditDialog()
        self.ui.setupUi(self)

        self.setWindowTitle("Rule Edit")
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.edit_flag = edit_flag
        self.rule_infos = rule_infos

        if self.edit_flag:
            self.init_rule_info(self.rule_infos)

        self.ui.pushButtonLHS.clicked.connect(self.selectLHS)
        self.ui.pushButtonRHS.clicked.connect(self.selectRHS)

        self.ui.buttonBox.accepted.connect(self.apply)
        self.ui.buttonBox.rejected.connect(self.reject_action)

    def init_rule_info(self, rule_infos):
        self.ui.lineEditName.setText(rule_infos[0])
        self.ui.lineEditLHS.setText(rule_infos[1])
        self.fill_list_items(rule_infos[1], self.ui.listWidgetInputLHS)
        self.fill_list_items(rule_infos[1], self.ui.listWidgetOutputLHS)

        self.ui.lineEditRHS.setText(rule_infos[2])
        self.fill_list_items(rule_infos[2], self.ui.listWidgetInputRHS)
        self.fill_list_items(rule_infos[2], self.ui.listWidgetOutputRHS)

        with open(rule_infos[3], 'r') as json_file:
            json_str = json.load(json_file)
        json_str = json.dumps(json_str)
        rule_object = json.loads(json_str)
        if rule_object:
            self.fill_check_states(rule_object["Input_Lhs"], self.ui.listWidgetInputLHS)
            self.fill_check_states(rule_object["Output_Lhs"], self.ui.listWidgetOutputLHS)
            self.fill_check_states(rule_object["Input_Rhs"], self.ui.listWidgetInputRHS)
            self.fill_check_states(rule_object["Output_Rhs"], self.ui.listWidgetOutputRHS)

    def fill_check_states(self, values, list_widget):
        c = list_widget.count()
        for i in range(c):
            model_index = QModelIndex(list_widget.model().index(i, 0))
            chk_box = list_widget.indexWidget(model_index)
            if chk_box.text() in values:
                chk_box.setChecked(True)

    def fill_list_items(self, path, list_widget):
        list_data = []
        pages = self.parent.get_net_from_file(path)
        places = []
        transitions = []
        for page in pages:
            places = page.placeNodes
            transitions = page.transitionNodes
        for place in places:
            list_data.append(place.name)
        for trans in transitions:
            list_data.append(trans.name)

        self.insert_node(list_data, list_widget)


    def selectLHS(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Select PNML Document", QDir.currentPath() + "/examples",
                                                  "Petri Net Files (*.pnml)")
        if file_name == "":
            return
        self.ui.listWidgetInputLHS.clear()
        self.ui.listWidgetOutputLHS.clear()

        self.fill_list_items(file_name, self.ui.listWidgetInputLHS)
        self.fill_list_items(file_name, self.ui.listWidgetOutputLHS)
        self.ui.lineEditLHS.setText(file_name)

    def selectRHS(self):
        file_name, _ = QFileDialog.getOpenFileName(self, "Select PNML Document", QDir.currentPath() + "/examples",
                                                  "Petri Net Files (*.pnml)")
        if file_name == "":
            return
        self.ui.listWidgetInputRHS.clear()
        self.ui.listWidgetOutputRHS.clear()

        self.fill_list_items(file_name, self.ui.listWidgetInputRHS)
        self.fill_list_items(file_name, self.ui.listWidgetOutputRHS)

        self.ui.lineEditRHS.setText(file_name)

    def insert_node(self, data, list_widget):
        for d in data:
            box = QCheckBox(d)
            item = QListWidgetItem()
            list_widget.addItem(item)
            list_widget.setItemWidget(item, box)

    def get_choose_sockets(self):
        input_lhs_count = self.ui.listWidgetInputLHS.count()
        output_lhs_count = self.ui.listWidgetOutputLHS.count()
        input_rhs_count = self.ui.listWidgetInputRHS.count()
        output_rhs_count = self.ui.listWidgetOutputRHS.count()
        input_cb_list_lhs = [self.ui.listWidgetInputLHS.itemWidget(self.ui.listWidgetInputLHS.item(i))
                            for i in range(input_lhs_count)]
        output_cb_list_lhs = [self.ui.listWidgetOutputLHS.itemWidget(self.ui.listWidgetOutputLHS.item(i))
                            for i in range(output_lhs_count)]
        input_cb_list_rhs = [self.ui.listWidgetInputRHS.itemWidget(self.ui.listWidgetInputRHS.item(i))
                            for i in range(input_rhs_count)]
        output_cb_list_rhs = [self.ui.listWidgetOutputRHS.itemWidget(self.ui.listWidgetOutputRHS.item(i))
                            for i in range(output_rhs_count)]
        input_lhs = []
        output_lhs = []
        input_rhs = []
        output_rhs = []

        for cb in input_cb_list_lhs:
            if cb.isChecked():
                input_lhs.append(cb.text())
        for cb in output_cb_list_lhs:
            if cb.isChecked():
                output_lhs.append(cb.text())
        for cb in input_cb_list_rhs:
            if cb.isChecked():
                input_rhs.append(cb.text())
        for cb in output_cb_list_rhs:
            if cb.isChecked():
                output_rhs.append(cb.text())
        return input_lhs, output_lhs, input_rhs, output_rhs

    def apply(self):
        if self.ui.lineEditName.text() == "":
            QMessageBox.warning(self, 'Warning', 'Name cannot be empty!')
            return
        else:
            name = self.ui.lineEditName.text()
            if not self.edit_flag:
                if name in self.parent.rule_names_set:
                    QMessageBox.warning(self, 'Warning', 'Name already exist!')
                    return
        if self.ui.lineEditLHS.text() == "":
            QMessageBox.warning(self, 'Warning', 'Please select Left-hand Side file!')
            return
        if self.ui.lineEditRHS.text() == "":
            QMessageBox.warning(self, 'Warning', 'Please select Right-hand Side file!')
            return
        id = Util.get_int_ruid()
        input_lhs, output_lhs, input_rhs, output_rhs = self.get_choose_sockets()
        rule = Rule(id, self.ui.lineEditName.text(),
                        "",
                        self.ui.lineEditLHS.text(),
                        self.ui.lineEditRHS.text(),
                        ",".join(input_lhs),
                        ",".join(output_lhs),
                        ",".join(input_rhs),
                        ",".join(output_rhs))
        if self.edit_flag:
            filepath = QDir.currentPath() + "/rules/" + self.ui.lineEditName.text() + ".json"
        else:
            filepath, type = QFileDialog.getSaveFileName(self, 'Save Rule', QDir.currentPath() + "/rules/"
                                                     + self.ui.lineEditName.text(), 'json(*.json)')
        if not filepath:
            return
        rule.path = str(filepath)
        json_str = rule.to_json()
        with open(filepath, 'w') as file_obj:
            json.dump(json_str, file_obj)
        if self.edit_flag and filepath != self.rule_infos[3]:
            QFile.remove(self.rule_infos[3])
        if self.edit_flag:
            self.parent.mod_item(str(rule.id), rule.name, rule.lhs, rule.rhs, rule.path)
        else:
            self.parent.add_item(str(rule.id), rule.name, rule.lhs, rule.rhs, rule.path)
        self.accept()

    def reject_action(self):
        self.reject()
