from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QColor
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QColorDialog
from forms.sim_setting_dialog import Ui_simSettingDialog

class SimulationSetting(QDialog):
    def __init__(self, parent, fire_time, round_interval, ball_color, flash_color, rules, condition):
        super(SimulationSetting, self).__init__()
        self.parent = parent
        self.ui = Ui_simSettingDialog()
        self.ui.setupUi(self)
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        self.ui.buttonBox.addButton("Apply", QDialogButtonBox.AcceptRole)
        self.ui.buttonBox.addButton("Cancel", QDialogButtonBox.RejectRole)
        self.ui.buttonBox.accepted.connect(self.accept_action)
        self.ui.buttonBox.rejected.connect(self.reject_action)

        self.ui.btn_selecet_rule.clicked.connect(self.select_rule)

        self.ui.btn_ball_color.clicked.connect(self.get_ball_color)
        self.ui.btn_flash_color.clicked.connect(self.get_flash_color)

        self.flash_color = QColor(flash_color)
        self.rules = rules
        self.condition = condition
        self.condition_values = []

        self.rule_ids = []
        for rule in self.rules:
            self.rule_ids.append(rule["Id"] + "  " + rule["Name"])
        self.places, self.transitions = self.parent.get_all_places_and_transitions()

        self.ui.combobox_con_id.clear()
        self.ui.combobox_con_id.addItems(self.rule_ids)
        self.ui.combobox_con_id.setCurrentIndex(self.condition[0])
        if self.condition[0] != -1:
            rule = self.get_rule_by_id(self.rules, self.ui.combobox_con_id.currentText().split(" ")[0])
            # self.ui.label_rule_name.setText(rule["Name"])
            self.ui.label_rule_path.setText(rule["Path"])
        self.ui.combobox_con_obj.setCurrentIndex(self.condition[1])
        self.ui.combobox_target.setCurrentIndex(self.condition[2])
        self.ui.combobox_con_type.setCurrentIndex(self.condition[3])
        self.ui.edit_con_val.setText(self.condition[4])
        self.fill_con_type_infos()

        self.ui.combobox_con_id.currentIndexChanged.connect(self.fill_rule_infos)
        self.ui.combobox_con_obj.currentIndexChanged.connect(self.fill_con_type_infos)

        self.keep_marking_flag = False
        self.auto_apply_flag = True
        self.ui.chkbox_keep_marking.setChecked(self.keep_marking_flag)
        self.ui.chkbox_auto_apply.setChecked(self.auto_apply_flag)

        self.fire_time = fire_time
        self.round_interval = round_interval
        self.ball_color = QColor(ball_color)
        self.toggle_apply_condition(False, False, False, False)

        self.ui.spinbox_fire_time.setValue(self.fire_time)
        self.ui.spinbox_round_interval.setValue(self.round_interval)
        self.ui.btn_ball_color.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" % self.ball_color.getRgb())
        self.ui.btn_flash_color.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" % self.flash_color.getRgb())

    def toggle_apply_condition(self, con_obj_flag, con_target_flag, con_type_flag, con_value_flag):
        self.ui.combobox_con_obj.setEnabled(con_obj_flag)
        self.ui.combobox_target.setEnabled(con_target_flag)
        self.ui.combobox_con_type.setEnabled(con_type_flag)
        self.ui.edit_con_val.setEnabled(con_value_flag)

    def get_rule_by_id(self, rules, id):
        if len(rules) <= 0:
            return
        for rule in rules:
            if rule["Id"] == id:
                return rule

    def fill_rule_infos(self):
        if len(self.rules) <= 0:
            return
        rule = self.get_rule_by_id(self.rules, self.ui.combobox_con_id.currentText().split(" ")[0])
        # self.ui.label_rule_name.setText(rule["Name"])
        self.ui.label_rule_path.setText(rule["Path"])

    def select_rule(self):
        self.toggle_apply_condition(True, True, True, True)

    def fill_con_type_infos(self):
        self.ui.combobox_target.clear()
        self.ui.combobox_con_type.clear()
        if self.ui.combobox_con_obj.currentText() == "Place tokens":
            self.ui.combobox_con_type.addItems(["<", "<=", "==", ">=", ">"])
            place_names = [place.name for place in self.places]
            place_names = sorted(place_names)
            self.ui.combobox_target.addItems(place_names)

        elif self.ui.combobox_con_obj.currentText() == "Transition fire times":
            # self.ui.combobox_con_type.clear()
            self.ui.combobox_con_type.addItems(["<", "<=", "==", ">=", ">"])
            tran_names = [tran.name for tran in self.transitions]
            tran_names = sorted(tran_names)
            # self.ui.combobox_target.clear()
            self.ui.combobox_target.addItems(tran_names)
        else:
            self.ui.combobox_con_type.addItems([])
        self.ui.combobox_target.setCurrentIndex(self.condition[2])
        self.ui.combobox_con_type.setCurrentIndex(self.condition[3])

    def get_ball_color(self):
        self.ball_color = QColorDialog.getColor()
        if self.ball_color.isValid():
            self.ui.btn_ball_color.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" % self.ball_color.getRgb())

    def get_flash_color(self):
        self.flash_color = QColorDialog.getColor()
        if self.flash_color.isValid():
            self.ui.btn_flash_color.setStyleSheet("background-color:rgba(%d,%d,%d,%d)" % self.flash_color.getRgb())

    def accept_action(self):
        self.keep_marking_flag = self.ui.chkbox_keep_marking.isChecked()
        self.auto_apply_flag = self.ui.chkbox_auto_apply.isChecked()
        self.fire_time = self.ui.spinbox_fire_time.value()
        self.round_interval = self.ui.spinbox_round_interval.value()
        self.condition = [self.ui.combobox_con_id.currentIndex(), self.ui.combobox_con_obj.currentIndex(),
                          self.ui.combobox_target.currentIndex(), self.ui.combobox_con_type.currentIndex(),
                          self.ui.edit_con_val.text()]
        self.condition_values = [self.ui.combobox_con_id.currentText(), self.ui.combobox_con_obj.currentText(),
                                 self.ui.combobox_target.currentText(), self.ui.combobox_con_type.currentText(),
                                 self.ui.edit_con_val.text()]
        self.accept()

    def reject_action(self):
        self.reject()





