import json
import random

import PyQt5.QtNetwork

from PyQt5.QtXmlPatterns import QXmlSchema, QXmlSchemaValidator
from PyQt5.QtCore import Qt, QTimer, QSize, QIODevice, QUrl, QDir, QFile, QFileInfo, QTextStream, QObject, pyqtSignal
from PyQt5.QtGui import QIcon, QFont, QPixmap
from PyQt5.QtWidgets import QMainWindow, QTabWidget, QLabel, QAction, QMenu, QMessageBox, QGraphicsView, QFileDialog, \
    QWidget, QHeaderView, QTableWidgetItem, QAbstractItemView, QDialog, QToolBar, QApplication
from forms.main_window import Ui_main_window

import sys

from PyQt5 import QtCore

from forms.incidece_widget import Ui_Incidence_Form
from forms.invariant_widget import Ui_FormInvariant
from src.basic.reconfigure import Reconfigure
from src.basic.simulation_init import SimulationSetting

from src.basic.tab_widget import TabWidget
from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc import Arc
from src.analysis.struct_property import StructProperty
from src.analysis.dynamic_property import DynamicProperty
from src.serialization.xml_parser import XmlParser
from src.serialization.xml_writer import XmlWriter
from src.utils.util import Util
from src.basic.rules_widget import RulesWidget
from src.analysis.reachable_graph import ReachableGraph
from src.serialization.xml_attribute import *



class MainWindow(QMainWindow):
    mode = Util.INIT_MODE
    reconfigure_sig = pyqtSignal()
    def __init__(self):
        super(MainWindow, self).__init__()

        self.main_ui = Ui_main_window()
        self.main_ui.setupUi(self)
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.tab_widget = QTabWidget(self)
        self.setCentralWidget(self.tab_widget)
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.setFont(QFont("Microsoft YaHei"))

        self.incidence_matrix_widget = None
        self.p_invariant_widget = None
        self.t_invariant_widget = None
        self.reachable_graph_widget = None
        self.rules_widget = None
        self.sim_setting_dialog = None
        self.ball_color = Qt.red
        self.flash_color = QColor(255, 166, 49)

        self.opend_file_names = []
        self.initial_marking = {}  # 初始标记
        self.current_marking = {}  # 当前标记, 键为Place 的id, 值为Place的tokens
        self.marking_set = []  # 标记集合， 每个unit结构为 [有哪个变迁而来, 当前标记选择的待激发变迁, 当前标记]
        self.current_marking_index = 0
        self.anim_timer = QTimer(self)  # 循环计时器，触发多次，用于不间断仿真
        self.norm_timer = QTimer(self)  # 常规计时器，只触发一次
        self.anim_duration = 500  # 每次变迁激发动画时间的一半
        self.round_interval = 300  # 自动仿真间隔时间
        self.still_anim_flag = False  # 记录是否仍然在动画中
        self.has_inited = False
        self.rules = []
        self.condition = [-1, -1, -1, -1, ""]
        self.condition_values = []
        self.multi_index = 0
        self.auto_apply_rule_flag = True

        self.reconfigure_sig.connect(self.reconfigure)

        self.file_menu = None
        self.new_action = None
        self.open_action = None
        self.save_action = None
        self.saveas_action = None
        self.export_action = None
        self.close_action = None

        self.edit_menu = None
        self.add_place_action = None
        self.add_transition_action = None
        self.add_arc_action = None
        self.select_action = None
        self.undo_action = None
        self.redo_action = None
        self.remove_action = None
        self.auto_layout_action = None

        self.analyze_menu = None
        self.grammar_check_action = None  # 模型合法性检查
        self.incidence_action = None  # 关联矩阵求解
        self.p_invariant_action = None  # p不变量求解
        self.t_invariant_action = None  # p不变量求解
        self.reachable_graph_action = None  # 可达图分析

        self.help_menu = None
        self.about_action = None

        self.sim_menu = None
        self.step_forward_action = None
        self.step_backward_action = None
        self.forward_action = None
        self.pause_action = None
        self.stop_action = None

        self.init_action = None
        self.rule_action = None

        self.create_file_menu()
        self.create_edit_menu()
        self.create_analyze_menu()
        self.create_sim_menu()
        self.create_help_menu()

        self.tool_bar = None
        self.create_tool_bar()

        self.statusBar().setLayoutDirection(Qt.LeftToRight)
        self.status_label = QLabel(self)
        self.status_label.setFont(QFont("Microsoft YaHei"))
        self.status_label.resize(100, 30)
        self.main_ui.statusbar.addPermanentWidget(self.status_label, 0)
        self.setMouseTracking(True)
        self.tab_widget.setMouseTracking(True)
        self.set_mode(self.mode)

        self.setContextMenuPolicy(Qt.NoContextMenu)

        self.init_signal()

    def create_file_menu(self):
        self.file_menu = QMenu("File")
        self.main_ui.menubar.addMenu(self.file_menu)

        self.new_action = QAction("New Net")
        self.new_action.setShortcut("Ctrl+N")
        self.new_action.setIcon(QIcon("./resources/new.svg"))

        self.open_action = QAction("Open Net")
        self.open_action.setShortcut("Ctrl+O")
        self.open_action.setIcon(QIcon("./resources/open.svg"))

        self.save_action = QAction("Save")
        self.save_action.setShortcut("Ctrl+S")
        self.save_action.setIcon(QIcon("./resources/save.svg"))

        self.saveas_action = QAction("Save as")
        self.saveas_action.setShortcut("F12")
        self.saveas_action.setIcon(QIcon("./resources/saveas.svg"))

        self.export_action = QAction("Export")
        self.export_action.setShortcut("Ctrl+E")
        self.export_action.setIcon(QIcon("./resources/export.svg"))

        self.close_action = QAction("Close")
        self.close_action.setShortcut("Ctrl+Q")
        self.close_action.setIcon(QIcon("./resources/close-circle.svg"))

        self.file_menu.addAction(self.new_action)
        self.file_menu.addAction(self.open_action)
        self.file_menu.addAction(self.save_action)
        self.file_menu.addAction(self.saveas_action)
        self.file_menu.addAction(self.export_action)
        self.file_menu.addAction(self.close_action)

    def create_edit_menu(self):
        self.edit_menu = QMenu("Edit")
        self.main_ui.menubar.addMenu(self.edit_menu)

        self.remove_action = QAction("Remove")
        self.remove_action.setIcon(QIcon("./resources/remove.svg"))

        self.select_action = QAction("Select")
        self.select_action.setCheckable(True)
        self.select_action.setIcon(QIcon("./resources/cursor.svg"))

        self.add_place_action = QAction("Add Place")
        self.add_place_action.setCheckable(True)
        self.add_place_action.setIcon(QIcon("./resources/place.svg"))

        self.add_transition_action = QAction("Add Transition")
        self.add_transition_action.setCheckable(True)
        self.add_transition_action.setIcon(QIcon("./resources/transition.svg"))

        self.add_arc_action = QAction("Add Flow")
        self.add_arc_action.setCheckable(True)
        self.add_arc_action.setIcon(QIcon("./resources/arc.svg"))

        self.undo_action = QAction("Undo")
        self.undo_action.setIcon(QIcon("./resources/undo.png"))

        self.redo_action = QAction("Redo")
        self.redo_action.setIcon(QIcon("./resources/redo.png"))

        self.auto_layout_action = QAction("Layout")
        self.auto_layout_action.setIcon(QIcon("./resources/layout.svg"))

        self.edit_menu.addAction(self.select_action)
        self.edit_menu.addAction(self.add_place_action)
        self.edit_menu.addAction(self.add_transition_action)
        self.edit_menu.addAction(self.add_arc_action)
        self.edit_menu.addAction(self.undo_action)
        self.edit_menu.addAction(self.redo_action)
        self.edit_menu.addAction(self.remove_action)
        self.edit_menu.addAction(self.auto_layout_action)

    def create_analyze_menu(self):
        self.analyze_menu = QMenu("Analyze")
        self.main_ui.menubar.addMenu(self.analyze_menu)

        self.grammar_check_action = QAction("Legality check")
        self.grammar_check_action.setIcon(QIcon("./resources/grammar_check.svg"))

        self.incidence_action = QAction("Incidence matrix")
        self.incidence_action.setIcon(QIcon("./resources/incidence_matrix.svg"))

        self.p_invariant_action = QAction("P-Invariant")
        self.p_invariant_action.setIcon(QIcon("./resources/p-invariant.svg"))

        self.t_invariant_action = QAction("T-Invariant")
        self.t_invariant_action.setIcon(QIcon("./resources/t-invariant.svg"))

        self.reachable_graph_action = QAction("Reachable graph")
        self.reachable_graph_action.setIcon(QIcon("./resources/reachable_tree.svg"))

        self.analyze_menu.addAction(self.grammar_check_action)
        self.analyze_menu.addAction(self.incidence_action)
        self.analyze_menu.addAction(self.p_invariant_action)
        self.analyze_menu.addAction(self.t_invariant_action)
        self.analyze_menu.addSeparator()
        self.analyze_menu.addAction(self.reachable_graph_action)

    def create_sim_menu(self):
        self.sim_menu = QMenu("Simulation")
        self.main_ui.menubar.addMenu(self.sim_menu)

        self.step_backward_action = QAction("Step backward")
        self.step_backward_action.setIcon(QIcon("./resources/step_backward.png"))

        self.step_forward_action = QAction("Step forward")
        self.step_forward_action.setIcon(QIcon("./resources/step_forward.png"))

        self.forward_action = QAction("Play forward")
        self.forward_action.setIcon(QIcon("./resources/start-circle.png"))

        self.pause_action = QAction("Pause")
        self.pause_action.setIcon(QIcon("./resources/pause-circle.png"))

        self.stop_action = QAction("Stop and reset state")
        self.stop_action.setIcon(QIcon("./resources/stop-circle.png"))

        self.sim_menu.addAction(self.step_backward_action)
        self.sim_menu.addAction(self.step_forward_action)
        self.sim_menu.addAction(self.forward_action)
        self.sim_menu.addAction(self.pause_action)
        self.sim_menu.addAction(self.stop_action)

    def create_help_menu(self):
        self.help_menu = QMenu("Help")
        self.main_ui.menubar.addMenu(self.help_menu)

        self.about_action = QAction("About")
        self.about_action.setIcon(QIcon("./resources/about.svg"))

        self.help_menu.addAction(self.about_action)

    def create_tool_bar(self):
        self.tool_bar = QToolBar()
        self.tool_bar.setContextMenuPolicy(QtCore.Qt.NoContextMenu)
        self.tool_bar.setToolButtonStyle(Qt.ToolButtonStyle.ToolButtonIconOnly)
        self.setStyleSheet("QToolBar::separator{background-color:#f8f4ed; width:20px;height:11;}")
        self.addToolBar(self.tool_bar)
        # 文件部分
        self.tool_bar.addAction(self.new_action)
        self.tool_bar.addAction(self.open_action)
        self.tool_bar.addAction(self.save_action)
        self.tool_bar.addAction(self.saveas_action)
        self.tool_bar.addSeparator()

        # 编辑部分
        self.tool_bar.addAction(self.select_action)
        self.tool_bar.addAction(self.add_place_action)
        self.tool_bar.addAction(self.add_transition_action)
        self.tool_bar.addAction(self.add_arc_action)
        self.tool_bar.addAction(self.undo_action)
        self.tool_bar.addAction(self.redo_action)
        self.tool_bar.addAction(self.remove_action)
        self.tool_bar.addAction(self.auto_layout_action)
        self.tool_bar.addSeparator()

        self.tool_bar.addAction(self.grammar_check_action)
        self.tool_bar.addAction(self.incidence_action)
        self.tool_bar.addAction(self.p_invariant_action)
        self.tool_bar.addAction(self.t_invariant_action)
        self.tool_bar.addAction(self.reachable_graph_action)
        self.tool_bar.addSeparator()

        # 重构规则部分
        self.rule_action = QAction("Reconfigurable Rules")
        self.rule_action.setIcon(QIcon("./resources/rulesetting.svg"))
        self.tool_bar.addAction(self.rule_action)
        # self.tool_bar.addSeparator()

        # 初始化编译
        self.init_action = QAction("Simulation Init")
        self.init_action.setIcon(QIcon("./resources/compile.svg"))
        self.tool_bar.addAction(self.init_action)

        # 仿真部分
        self.tool_bar.addAction(self.step_backward_action)
        self.tool_bar.addAction(self.step_forward_action)
        self.tool_bar.addAction(self.forward_action)
        self.tool_bar.addAction(self.pause_action)
        self.tool_bar.addAction(self.stop_action)

        self.tool_bar.addSeparator()
        # 分析部分
        self.tool_bar.setIconSize(QSize(35, 35))

    def init_signal(self):
        self.tab_widget.tabCloseRequested.connect(self.close_net)
        self.tab_widget.currentChanged.connect(self.change_tab)

        self.new_action.triggered.connect(self.new_net)
        self.open_action.triggered.connect(self.open_net)
        self.save_action.triggered.connect(self.save_net)
        self.saveas_action.triggered.connect(self.saveas_net)
        self.export_action.triggered.connect(self.export_net)
        self.close_action.triggered.connect(self.close_net)

        self.select_action.triggered.connect(self.select_item)
        self.add_place_action.triggered.connect(self.add_place_item)
        self.add_transition_action.triggered.connect(self.add_transition_item)
        self.add_arc_action.triggered.connect(self.add_arc_item)
        self.about_action.triggered.connect(self.about_info)
        self.undo_action.triggered.connect(self.undo)
        self.redo_action.triggered.connect(self.redo)
        self.remove_action.triggered.connect(self.remove)
        self.auto_layout_action.triggered.connect(self.auto_layout)

        self.grammar_check_action.triggered.connect(self.legality_check)
        self.incidence_action.triggered.connect(self.incidence_matix)
        self.p_invariant_action.triggered.connect(self.p_invariant)
        self.t_invariant_action.triggered.connect(self.t_invariant)
        self.reachable_graph_action.triggered.connect(self.reachable_graph)

        self.step_forward_action.triggered.connect(self.sim_step_forward)
        self.step_backward_action.triggered.connect(self.sim_step_backward)
        self.forward_action.triggered.connect(self.sim_forward)
        self.pause_action.triggered.connect(self.sim_pause)
        self.stop_action.triggered.connect(self.sim_stop)

        self.rule_action.triggered.connect(self.rule_setting)
        self.init_action.triggered.connect(self.init_setting)

    def closeEvent(self, event):
        i = 0
        self.statusBar().showMessage("Save Changes or not?");
        quit_flag = True
        while i < self.tab_widget.count() and quit_flag:
            tab = self.tab_widget.widget(i)
            i += 1
            self.tab_widget.setCurrentWidget(tab)
            if not tab.is_saved():
                action = QMessageBox.warning(self, "Save", "Save or not?", QMessageBox.Save | QMessageBox.No | \
                                             QMessageBox.Cancel | QMessageBox.Save)
                if action == QMessageBox.Save:
                    self.save_net()
                    quit_flag = True
                elif action == QMessageBox.Cancel:
                    quit_flag = False
                elif action == QMessageBox.No:
                    quit_flag = True
        if not quit_flag:
            self.statusBar().clearMessage()
            event.ignore()

    def set_mode(self, mode: int):
        # 1 选择模式, 2 库所添加模式, 3 变迁添加 4 弧添加 5 仿真模式 0 初始模式
        self.mode = mode
        cur_tab = self.get_current_tab()
        if mode == Util.SELECT_MODE or mode == Util.INIT_MODE:
            if cur_tab:
                cur_tab.view.setCursor(Qt.ArrowCursor)
        elif mode == Util.PLACE_MODE or mode == Util.TRANS_MODE or mode == Util.ARC_MODE:
            if cur_tab:
                cur_tab.view.setCursor(Qt.CrossCursor)
                cur_tab.view.setDragMode(QGraphicsView.NoDrag)
        if mode == Util.INIT_MODE:
            self.toggle_action(False, False, False, False, False, False, False,
                               False, False, False, False, False, False, False, False)
        elif mode == Util.SELECT_MODE:
            self.toggle_action(True, False, False, False, True, True, True,
                               True, True, True, False, False, False, False, False)
        elif mode == Util.PLACE_MODE:
            self.toggle_action(False, True, False, False, True, True, True,
                               True, True, True, False, False, False, False, False)
        elif mode == Util.TRANS_MODE:
            self.toggle_action(False, False, True, False, True, True, True,
                               True, True, True, False, False, False, False, False)
        elif mode == Util.ARC_MODE:
            self.toggle_action(False, False, False, True, True, True, False,
                               True, True, True, False, False, False, False, False)
        elif mode == Util.SIM_MODE:
            self.toggle_action(False, False, False, False, False, False, False,
                               False, True, True, True, True, True, True, True)

    def toggle_action(self, *args):
        self.select_action.setChecked(args[0])
        self.add_place_action.setChecked(args[1])
        self.add_transition_action.setChecked(args[2])
        self.add_arc_action.setChecked(args[3])
        self.undo_action.setEnabled(args[4])
        self.redo_action.setEnabled(args[5])
        self.remove_action.setEnabled(args[6])
        self.auto_layout_action.setEnabled(args[7])

        self.rule_action.setEnabled(args[8])
        self.init_action.setEnabled(args[9])

        self.step_backward_action.setEnabled(args[10])
        self.step_forward_action.setEnabled(args[11])
        self.forward_action.setEnabled(args[12])
        self.pause_action.setEnabled(args[13])
        self.stop_action.setEnabled(args[14])

    # 新建网
    def new_net(self):
        self.set_mode(0)
        new_tab = TabWidget("untitled*", self.tab_widget)
        self.tab_widget.addTab(new_tab, new_tab.file_name)
        self.tab_widget.setCurrentWidget(new_tab)

    def validateXml(self, file):
        # 1 验证 XML Schema
        schema = QXmlSchema()
        # schema.setMessageHandler(messageHandler)
        schema.load(QUrl.fromLocalFile("./schemas/ptnet.xsd"))

        if not schema.isValid():
            return False

        # 2 打开文件
        if not file.open(QIODevice.ReadOnly | QIODevice.Text):
            QMessageBox.critical(self, "Loading File Error", file.errorString())
            return False

        # 3 验证文件是否符合XML schema
        validator = QXmlSchemaValidator(schema)
        # validator.setMessageHandler(messageHandler)

        if not validator.validate(file, QUrl.fromLocalFile(file.fileName())):
            file.close()
            return False

        return True

    # 打开
    def open_net(self):
        self.set_mode(0)
        self.statusBar().showMessage("Open an existing PNML document ...")
        flag = False
        # 获取文件名
        full_file_path, _ = QFileDialog.getOpenFileName(self, "Open PNML Document", QDir.currentPath() + "/examples",
                                                        "Petri Net Files (*.pnml)")
        if not full_file_path:
            self.statusBar().showMessage("File path error, Document was not opened.", 1000)
            return False

        if self.opend_file_names.count(full_file_path) != 0:
            for i in range(self.tab_widget.count()):
                tab = self.tab_widget.widget(i)
                if tab.file_name == full_file_path:
                    self.tab_widget.setCurrentWidget(tab)
                    self.statusBar().showMessage("Document loaded and opened.", 1000)
                    return True
        else:
            self.opend_file_names.append(full_file_path)

        file = QFile(full_file_path)
        file_info = QFileInfo(file)

        # 验证xml文件
        if not self.validateXml(file):
            self.statusBar().showMessage("Xml validate failed, Document was not opened.", 1000)
            return False

        # 解析xml文件
        file.seek(0)
        text_stream = QTextStream(file)
        xml_content = text_stream.readAll()
        file.close()

        parser = XmlParser()
        if not parser.parseXML(xml_content):
            self.statusBar().showMessage("Parse Xml file failed, Document was not opened.", 1000)
            return False

        # 解析结果展现
        net = parser.getNetFromXML()

        tab = TabWidget(full_file_path, self.tab_widget)
        tab.init_from_xml(net)
        self.tab_widget.addTab(tab, file_info.fileName())
        self.tab_widget.setCurrentWidget(tab)
        self.statusBar().showMessage("Document loaded and opened.", 1000)
        self.set_mode(1)
        return True

    # 保存
    def save_net(self):
        # self.set_mode(0)
        tab = self.tab_widget.currentWidget()
        if not tab:
            return

        # 合法性检查
        # if not self.legality_check_core(self.get_scene_nodes()):
        #     return

        full_file_path = tab.file_name
        if full_file_path == "untitled*":
            self.saveas_net()
        else:
            file = QFile(full_file_path)
            if not file.open(QIODevice.WriteOnly):
                QMessageBox.critical(self, "Open File Error", "The file could not be opened.")
            # 这里序列化
            writer = XmlWriter(tab.to_xml())
            writer.writeXML(file)
            # tab.scene.clean_undo_stack()

    # 另存为
    def saveas_net(self):
        tab = self.tab_widget.currentWidget()
        if not tab:
            return
        index = self.tab_widget.currentIndex()

        # if not self.legality_check_core(self.get_scene_nodes()):
        #     return

        # 获取保存文件路径
        full_file_path, _ = QFileDialog.getSaveFileName(self, "Save As PNML Document", QDir.currentPath() + "/examples",
                                                        "Petri Net Files (*.pnml)")
        # print(full_file_path)  D:/Projects/PythonProjects/rcpn/examples/net1.pnml
        if not full_file_path:
            return
        if QFileInfo(full_file_path).suffix().strip() == '':
            full_file_path += ".pnml"
        # 获取文件结构对象
        file = QFile(full_file_path)
        if not file.open(QIODevice.WriteOnly):
            QMessageBox.critical(self, "Save As Error", "The Petri Net could not be saved to: " + full_file_path)
            return
        # 文件名
        file_name = QFileInfo(full_file_path).fileName()  # net1.pnml
        file_name = file_name[0:-5]  # net1

        tab.net_id = file_name
        tab.name = file_name
        tab.file_name = full_file_path
        # 这里序列化
        writer = XmlWriter(tab.to_xml())
        writer.writeXML(file)
        #  提示上显示完整路径的文件名fileName
        self.tab_widget.setTabToolTip(index, full_file_path)
        # 不显示完整路径带后缀
        self.tab_widget.setTabText(index, QFileInfo(full_file_path).fileName())
        self.opend_file_names.append(full_file_path)
        QMessageBox.information(self, "Save Success!!", "Current Petri net was saved correctly")

    # 导出
    def export_net(self):
        self.set_mode(0)
        pass

    # 关闭tab
    def close_net(self):
        self.set_mode(0)
        self.anim_timer.stop()
        self.tab_widget.removeTab(self.tab_widget.currentIndex())

    def get_current_tab(self):
        if self.tab_widget.count() == 0:
            return
        return self.tab_widget.currentWidget()

    def select_item(self):
        self.set_mode(Util.SELECT_MODE)

    def add_place_item(self):
        self.set_mode(Util.PLACE_MODE)

    def add_transition_item(self):
        self.set_mode(Util.TRANS_MODE)

    def add_arc_item(self):
        self.set_mode(Util.ARC_MODE)

    def undo(self):
        if self.tab_widget.currentWidget() is None:
            return
        self.tab_widget.currentWidget().scene.undo_stack.undo()

    def redo(self):
        if self.tab_widget.currentWidget() is None:
            return
        self.tab_widget.currentWidget().scene.undo_stack.redo()

    def remove(self):
        if self.tab_widget.currentWidget() is None:
            return
        self.tab_widget.currentWidget().scene.remove_selected_items()

    def auto_layout(self):
        if self.tab_widget.currentWidget() is None:
            return
        self.tab_widget.currentWidget().scene.auto_layout()

    def get_scene_nodes(self):
        items = self.get_current_tab().scene.items()
        place_nodes = []
        transition_nodes = []
        arcs = []
        for item in items:
            if item.Type == Place.Type:
                place_nodes.append(item)
            elif item.Type == Transition.Type:
                transition_nodes.append(item)
            elif item.Type == Arc.Type:
                arcs.append(item)
        return place_nodes, transition_nodes, arcs

    def legality_check_core(self, nodes):
        places, transitions, arcs = nodes
        if not places or not transitions or not arcs:
            QMessageBox.warning(self, "Warning", "Check Failed!", buttons=QMessageBox.Ok)
            return False
        else:
            for place in places:
                if not place.check_has_relations():
                    QMessageBox.critical(self, "Net Invalid", "place " + place.name + " is a independent node!")
                    return False
                input_arcs = place.input_arcs
                for arc in input_arcs:
                    if place.check_reach_capacity(arc):
                        QMessageBox.critical(self, "Net Invalid", "place " + place.name + "'s capacity not enough!")
                        return False
            for trans in transitions:
                if not trans.check_has_relations():
                    QMessageBox.critical(self, "Net Invalid", "transition " + trans.name + " is a independent node!")
                    return False
            for arc in arcs:
                if arc.weight < 1:
                    QMessageBox.critical(self, "Net Invalid", "arc " + arc.name + "'s weight less than 1!")
                    return False

        return True

    def legality_check(self):
        cur_tab = self.get_current_tab()
        if not cur_tab:
            return False
        if not self.legality_check_core(self.get_scene_nodes()):
            return
        msg = QMessageBox(self)
        msg.setWindowTitle("Result")
        msg.setText("    Check Pass!    ")
        msg.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        pxmap = QPixmap()
        pxmap.load("./resources/check_pass.png")
        msg.setIconPixmap(pxmap)
        msg.exec_()

    def incidence_matix(self):
        if not self.get_current_tab():
            return
        if not self.legality_check_core(self.get_scene_nodes()):
            return
        s = StructProperty(*self.get_scene_nodes())
        if not s.is_pure_net():
            QMessageBox.critical(self, "Error", "Not Pure Net!")
            return

        matrix, place_head, tran_head = s.get_incidence_matrix()
        self.incidence_matrix_widget = QWidget()
        w = self.incidence_matrix_widget.width()
        h = self.incidence_matrix_widget.height()
        self.incidence_matrix_widget.move(self.pos().x() + self.width() / 2 - w / 2,
                                         self.pos().y() + self.height() / 2 - h / 2)
        self.incidence_matrix_widget.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        matrix_form_ui = Ui_Incidence_Form()
        matrix_form_ui.setupUi(self.incidence_matrix_widget)
        matrix_form_ui.incidenceMatrix.setRowCount(matrix.shape[0])
        matrix_form_ui.incidenceMatrix.setColumnCount(matrix.shape[1])
        row_header = []

        for i in place_head:
            # row_header.append("p" + str(i))
            row_header.append(self.get_current_tab().scene.get_item_by_itemid(str(i), Place.Type).name)
        matrix_form_ui.incidenceMatrix.setHorizontalHeaderLabels(row_header)
        col_header = []
        for i in tran_head:
            # col_header.append("t" + str(i))
            col_header.append(self.get_current_tab().scene.get_item_by_itemid(str(i), Transition.Type).name)
        matrix_form_ui.incidenceMatrix.setVerticalHeaderLabels(col_header)

        self.incidence_matrix_widget.resize(len(s.places) * 40 + 65, len(s.transitions) * 40 + 65)
        self.incidence_matrix_widget.setMinimumSize(400, 400)
        matrix_form_ui.incidenceMatrix.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        matrix_form_ui.incidenceMatrix.verticalHeader().setSectionResizeMode(QHeaderView.Stretch)

        stylesheet = "::section{Background-color:rgb(78, 247, 247)}"
        stylesheet_corner = "QTableCornerButton::section{Background-color:rgb(78, 247, 247)}"
        matrix_form_ui.incidenceMatrix.horizontalHeader().setStyleSheet(stylesheet)
        matrix_form_ui.incidenceMatrix.verticalHeader().setStyleSheet(stylesheet)
        matrix_form_ui.incidenceMatrix.setStyleSheet(stylesheet_corner)

        for i in range(matrix.shape[0]):
            for j in range(matrix.shape[1]):
                table_item = QTableWidgetItem(str(matrix[i][j]))
                table_item.setTextAlignment(Qt.AlignCenter)
                table_item.setFlags(table_item.flags() & (~Qt.ItemIsEditable))
                matrix_form_ui.incidenceMatrix.setItem(i, j, table_item)
        self.incidence_matrix_widget.show()

    def p_invariant(self):
        if not self.get_current_tab():
            return
        if not self.legality_check_core(self.get_scene_nodes()):
            return
        s = StructProperty(*self.get_scene_nodes())
        if not s.is_pure_net():
            QMessageBox.critical(self, "Error", "Not Pure Net!")
            return
        self.p_invariant_widget = QWidget()
        w = self.p_invariant_widget.width()
        h = self.p_invariant_widget.height()
        self.p_invariant_widget.move(self.pos().x() + self.width() / 2 - w / 2,
                                         self.pos().y() + self.height() / 2 - h / 2)
        self.p_invariant_widget.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        p_invariant_form_ui = Ui_FormInvariant()
        p_invariant_form_ui.setupUi(self.p_invariant_widget)
        self.p_invariant_widget.setWindowTitle("p-invariant")

        incidence_matrix, place_head, _ = s.get_incidence_matrix()
        p_matrix = s.get_p_invariant(incidence_matrix)
        p_invariant_form_ui.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        p_invariant_form_ui.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        p_invariant_form_ui.tableWidget.setAlternatingRowColors(True)
        self.p_invariant_widget.resize(len(s.places) * 50 + 65, p_matrix.shape[0] * 50 + 145)
        self.p_invariant_widget.setMinimumWidth(350)
        p_invariant_form_ui.tableWidget.setRowCount(p_matrix.shape[0])
        p_invariant_form_ui.tableWidget.setColumnCount(p_matrix.shape[1])
        row_header = []
        for i in place_head:
            row_header.append(self.get_current_tab().scene.get_item_by_itemid(str(i), Place.Type).name)
        p_invariant_form_ui.tableWidget.setHorizontalHeaderLabels(row_header)
        p_invariant_form_ui.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        stylesheet = "::section{Background-color:rgb(78, 247, 247)}"
        p_invariant_form_ui.tableWidget.horizontalHeader().setStyleSheet(stylesheet)

        cover_flag = False
        for i in range(p_matrix.shape[0]):
            cover_sum = 0
            for j in range(p_matrix.shape[1]):
                cover_sum += p_matrix[i][j]
                table_item = QTableWidgetItem(str(p_matrix[i][j]))
                table_item.setTextAlignment(Qt.AlignCenter)
                table_item.setFlags(table_item.flags() & (~Qt.ItemIsEditable))
                p_invariant_form_ui.tableWidget.setItem(i, j, table_item)
            if cover_sum == p_matrix.shape[1]:
                cover_flag = True

        if cover_flag:
            p_invariant_form_ui.textEdit.setText("The net is covered by positive P-Invariants, "
                                                 "therefore it is bounded.")
        else:
            p_invariant_form_ui.textEdit.setText("The net is not covered by positive P-Invariants, "
                                                 "therefore we do not know if it is bounded.")

        self.p_invariant_widget.show()

    def t_invariant(self):
        if not self.get_current_tab():
            return
        if not self.legality_check_core(self.get_scene_nodes()):
            return
        s = StructProperty(*self.get_scene_nodes())
        if not s.is_pure_net():
            QMessageBox.critical(self, "Error", "Not Pure Net!")
            return
        self.t_invariant_widget = QWidget()
        w = self.t_invariant_widget.width()
        h = self.t_invariant_widget.height()
        self.t_invariant_widget.move(self.pos().x() + self.width() / 2 - w / 2,
                                         self.pos().y() + self.height() / 2 - h / 2)

        self.t_invariant_widget.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        t_invariant_form_ui = Ui_FormInvariant()
        t_invariant_form_ui.setupUi(self.t_invariant_widget)
        self.t_invariant_widget.setWindowTitle("t-invariant")

        t_matrix = s.get_t_invariant()
        _, _, tran_head = s.get_incidence_matrix()
        t_invariant_form_ui.tableWidget.setSelectionBehavior(QAbstractItemView.SelectRows)
        t_invariant_form_ui.tableWidget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        t_invariant_form_ui.tableWidget.setAlternatingRowColors(True)
        self.t_invariant_widget.resize(len(s.transitions) * 50 + 65, t_matrix.shape[0] * 55 + 150)
        self.t_invariant_widget.setMinimumWidth(350)
        t_invariant_form_ui.tableWidget.setRowCount(t_matrix.shape[0])
        t_invariant_form_ui.tableWidget.setColumnCount(t_matrix.shape[1])
        row_header = []
        for i in tran_head:
            row_header.append(self.get_current_tab().scene.get_item_by_itemid(str(i), Transition.Type).name)
        t_invariant_form_ui.tableWidget.setHorizontalHeaderLabels(row_header)
        t_invariant_form_ui.tableWidget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        stylesheet = "::section{Background-color:rgb(78, 247, 247)}"
        t_invariant_form_ui.tableWidget.horizontalHeader().setStyleSheet(stylesheet)

        cover_flag = False
        for i in range(t_matrix.shape[0]):
            cover_sum = 0
            for j in range(t_matrix.shape[1]):
                cover_sum += t_matrix[i][j]
                table_item = QTableWidgetItem(str(t_matrix[i][j]))
                table_item.setTextAlignment(Qt.AlignCenter)
                table_item.setFlags(table_item.flags() & (~Qt.ItemIsEditable))
                t_invariant_form_ui.tableWidget.setItem(i, j, table_item)
            if cover_sum == t_matrix.shape[1]:
                cover_flag = True
        if cover_flag:
            t_invariant_form_ui.textEdit.setText("The net is covered by positive T-Invariants, "
                                                 "therefore it is bounded and live.")
        else:
            t_invariant_form_ui.textEdit.setText("The net is not covered by positive T-Invariants, "
                                                 "therefore we do not know if it is bounded and live.")

        self.t_invariant_widget.show()

    def reachable_graph(self):
        if not self.get_current_tab():
            return
        if not self.legality_check_core(self.get_scene_nodes()):
            return
        s = DynamicProperty(*self.get_scene_nodes())
        reachable_graph = s.get_reachable_graph()
        if reachable_graph is not None:
            self.reachable_graph_widget = ReachableGraph(*reachable_graph)
            # self.mapFrom(self.height())
            w = self.reachable_graph_widget.width()
            h = self.reachable_graph_widget.height()
            self.reachable_graph_widget.move(self.pos().x() + self.width() / 2 - w / 2,
                                             self.pos().y() + self.height() / 2 - h / 2)
            # print(self.pos())
            self.reachable_graph_widget.show()
        # else:
        #     QMessageBox.warning(self, "Unbounded", "This Petri Net is Unbounded!")

    def about_info(self):
        msg = "<h3>RCPN tool</h3>" \
              "<h5>Based on Qt 5.9.4 </h5>" \
              "<p><strong>RCPN tool</strong> is a modeler and simulator for reconfigurable Petri nets." \
              "It uses the standard " \
              "<a href=\"www.pnml.org\">PNML</a> exanchge format." \
              "<p><strong>Author:</strong> <a href=\"mailto:liuf_2001@163.com\">Fei Liu, Weiyi Liu</a>.</p>" \
              "China 2021</p>"
        QMessageBox.information(self, "About RCSPN tool", msg)

    def rule_setting(self):
        self.rules_widget = RulesWidget(parent=self)
        self.rules_widget.setWindowModality(Qt.NonModal)
        w = self.rules_widget.width()
        h = self.rules_widget.height()
        self.rules_widget.move(self.pos().x() + self.width() / 2 - w / 2,
                                         self.pos().y() + self.height() / 2 - h / 2)
        self.rules_widget.show()

    def get_rules_lib(self):
        lib_dir = QDir(QDir.currentPath() + "/rules")
        file_name_filters = ["*.json"]
        file_infos = lib_dir.entryInfoList(file_name_filters, QDir.Files | QDir.Readable, QDir.Name)
        file_paths = [info.absoluteFilePath() for info in file_infos]
        self.rules.clear()
        for json_path in file_paths:
            if json_path == "":
                continue
            with open(json_path, 'r') as json_file:
                json_str = json.load(json_file)
            json_str = json.dumps(json_str)
            try:
                rule_object = json.loads(json_str)
            except ValueError:
                continue
            self.rules.append(rule_object)
        self.rules = sorted(self.rules, key=lambda e: e.__getitem__("Id"))
        return self.rules

    def get_all_places_and_transitions(self):
        cur_tab = self.get_current_tab()
        if not cur_tab:
            return []
        items = cur_tab.scene.items()
        places = []
        transitions = []
        for item in items:
            if item.Type == Place.Type:
                places.append(item)
            elif item.Type == Transition.Type:
                transitions.append(item)
        return places, transitions

    def get_item_by_name(self, name, item_type):
        places, transitions = self.get_all_places_and_transitions()
        if item_type == Place.Type:
            for place in places:
                if place.name == name:
                    return place
        elif item_type == Transition.Type:
            for tran in transitions:
                if tran.name == name:
                    return tran
        else:
            return None

    def change_tab(self):
        self.set_mode(1)
        self.has_inited = False

    def init_setting(self):
        if not self.get_current_tab():
            return
        self.set_mode(5)
        self.new_action.setEnabled(False)
        self.open_action.setEnabled(False)
        self.tab_widget.setEnabled(False)
        self.add_place_action.setEnabled(False)
        self.add_transition_action.setEnabled(False)
        self.add_arc_action.setEnabled(False)
        self.set_sim_btn_state(True, False, True, True, False, True)
        self.statusBar().clearMessage()
        self.rules = self.get_rules_lib()

        self.sim_setting_dialog = SimulationSetting(self, 2 * self.anim_duration, self.round_interval, self.ball_color,
                                                    self.flash_color, self.rules, self.condition)
        w = self.sim_setting_dialog.width()
        h = self.sim_setting_dialog.height()
        self.sim_setting_dialog.move(self.pos().x() + self.width() / 2 - w / 2,
                                         self.pos().y() + self.height() / 2 - h / 2)

        # self.sim_setting_dialog.setWindowModality(Qt.NonModal)
        res = self.sim_setting_dialog.exec()
        if res == QDialog.Accepted:
            if not self.has_inited:
                self.save_current_marking()
                self.initial_marking = self.current_marking.copy()
                self.init_marking_set(self.initial_marking)
                self.has_inited = True
            elif self.sim_setting_dialog.keep_marking_flag:
                self.save_current_marking()
                self.initial_marking = self.current_marking.copy()
                self.init_marking_set(self.initial_marking)
            else:
                self.init_marking_set(self.initial_marking)
            self.anim_duration = self.sim_setting_dialog.fire_time / 2
            self.round_interval = self.sim_setting_dialog.round_interval
            self.ball_color = self.sim_setting_dialog.ball_color
            self.flash_color = self.sim_setting_dialog.flash_color
            self.condition = self.sim_setting_dialog.condition
            self.condition_values = self.sim_setting_dialog.condition_values
            self.auto_apply_rule_flag = self.sim_setting_dialog.auto_apply_flag

        elif res == QDialog.Rejected:
            self.add_place_action.setEnabled(True)
            self.add_transition_action.setEnabled(True)
            self.add_arc_action.setEnabled(True)
            self.new_action.setEnabled(True)
            self.open_action.setEnabled(True)
            self.tab_widget.setEnabled(True)
            self.set_sim_btn_state(True, False, False, False, False, False)

    def check_condition(self, vals):
        if len(vals) == 0:
            return None
        if vals[1] == '' or vals[2] == '' or vals[3] == '' or vals[4] == '':
            return None
        if self.condition_values[1] == 'Place tokens':
            target = self.get_item_by_name(self.condition_values[2], Place.Type)
            if eval("target.tokens " + self.condition_values[3] + " " + self.condition_values[4]):
                print("Meet Place condition")
                return True
        elif self.condition_values[1] == 'Transition fire times':
            target = self.get_item_by_name(self.condition_values[2], Transition.Type)
            if eval("target.fired_times " + self.condition_values[3] + " " + self.condition_values[4]):
                print("Meet Transition condition")
                return True
        return False

    def save_current_marking(self):
        if not self.get_current_tab():
            return
        scene = self.get_current_tab().scene
        self.current_marking = {}
        for item in scene.items():
            if item.Type == Place.Type:
                self.current_marking[int(item.place_id)] = item.tokens

    def reset_marking(self, marking_dict):
        if not self.get_current_tab():
            return
        scene = self.get_current_tab().scene
        for item in scene.items():
            if item.Type == Place.Type:
                item.tokens = marking_dict[int(item.place_id)]
        scene.update()

    def init_marking_set(self, init_marking):
        self.reset_marking(init_marking)
        self.current_marking_index = 0
        self.marking_set.clear()
        self.marking_set.append(["none", "none", self.initial_marking.copy()])

    def get_fireable_trans(self):
        fireable_trans = []
        for item in self.get_current_tab().scene.items():
            if item.Type == Transition.Type:
                if item.is_fireable_without_capcity():
                    fireable_trans.append(item)
        return fireable_trans

    def apply_step_forward_state(self):
        self.set_sim_btn_state(True, True, True, True, False, True)

    def sim_step_forward(self, continuous_flag):
        if not self.get_current_tab():
            return
        cur_tab = self.get_current_tab()
        # 单步仿真
        fire_tran_name = ""
        cur_tran = None
        if not continuous_flag:
            self.set_sim_btn_state(False, False, False, False, False, False)
            self.norm_timer = QTimer(self)
            self.norm_timer.setSingleShot(True)
            self.norm_timer.timeout.connect(self.apply_step_forward_state)
            self.norm_timer.start(2 * self.anim_duration)
        # 已经激发过, 直接在标识集中选择已经激发的变迁
        if self.current_marking_index < len(self.marking_set) - 1:
            trans_id = self.marking_set[self.current_marking_index][1]
            cur_tran = cur_tab.scene.get_item_by_itemid(trans_id, Transition.Type)
            cur_tran.fire(self.anim_duration, self.ball_color, self.flash_color)
        else:
            # 没有激发过，随机选择变迁
            firable_trans = self.get_fireable_trans()
            if len(firable_trans) == 0:
                 return False
            rand_index = random.randint(0, len(firable_trans) - 1)
            cur_tran = firable_trans[rand_index]
            cur_tran.fire(self.anim_duration, self.ball_color, self.flash_color)
            # 更新标识集合
            marking = [firable_trans[rand_index].transition_id, "none"]

            self.marking_set[self.current_marking_index][1] = firable_trans[rand_index].transition_id
            self.marking_set.append(marking)
        # 更新当前推进的标识索引
        self.current_marking_index += 1
        msg = "    STEP " + str(self.current_marking_index) + ",  " + str(cur_tran.name) + " fired."
        self.statusBar().showMessage(msg, 0)
        # self.status_label.setText("    STEP " + str(self.current_marking_index))
        return True

    def append_new_marking(self):
        self.save_current_marking()
        self.marking_set[self.current_marking_index].append(self.current_marking.copy())

    def apply_step_backward_state(self):
        if self.current_marking_index == 0:
            self.set_sim_btn_state(True, False, True, True, False, True)
        else:
            self.set_sim_btn_state(True, True, True, True, False, True)

    def sim_step_backward(self, continuous_flag):
        if not self.get_current_tab():
            return
        cur_tab = self.get_current_tab()
        if not continuous_flag:
            self.set_sim_btn_state(False, False, False, False, False, False)
            self.norm_timer = QTimer(self)
            self.norm_timer.setSingleShot(True)
            self.norm_timer.timeout.connect(self.apply_step_backward_state)
            self.norm_timer.start(2 * self.anim_duration)
        to_defire_tran_id = self.marking_set[self.current_marking_index][0]
        if to_defire_tran_id == 'none':
            return
        cur_tran = cur_tab.scene.get_item_by_itemid(to_defire_tran_id, Transition.Type)
        cur_tran.defire(self.anim_duration, self.ball_color, self.flash_color)
        self.current_marking_index -= 1
        cur_tab.scene.update()
        msg = "    STEP " + str(self.current_marking_index) + ",  " + str(cur_tran.name) + " defired."
        self.statusBar().showMessage(msg, 0)

    def sim_forward(self):
        self.set_sim_btn_state(False, False, False, False, True, False)
        self.tab_widget.setEnabled(False)
        self.sim_step_forward(True)
        self.anim_timer = QTimer(self)
        self.anim_timer.start(2 * self.anim_duration + self.round_interval)  # 设置计时间隔并启动
        self.anim_timer.timeout.connect(self.loop)

    def reconfigure(self):
        selected_rule = self.rules[self.condition[0]]
        cur_scene = self.get_current_tab().scene
        rc = Reconfigure(selected_rule)
        lhs_path = QDir.currentPath() + "/examples/" + selected_rule["Lhs"].split('/')[-1]
        rc.apply(cur_scene, lhs_path)

    def loop(self):
        if self.check_condition(self.condition_values) and self.auto_apply_rule_flag:
            if self.anim_timer.isActive():
                self.anim_timer.stop()

            self.reconfigure_sig.emit()
            # 修改重构后状态
            self.condition_values = []
            self.has_inited = False
            self.save_current_marking()
            self.initial_marking = self.current_marking.copy()
            self.current_marking_index = 0
            self.multi_index = 0
            self.marking_set.clear()
            self.marking_set.append(["none", "none", self.initial_marking.copy()])
            self.anim_timer.setInterval(2 * self.anim_duration + self.round_interval)
            self.anim_timer.start()
            # self.set_sim_btn_state(True, False, False, False, False, False)
            # self.set_mode(1)
            # self.new_action.setEnabled(True)
            # self.open_action.setEnabled(True)
            # self.tab_widget.setEnabled(True)
            # self.add_place_action.setEnabled(True)
            # self.add_transition_action.setEnabled(True)
            # self.add_arc_action.setEnabled(True)
            # return
        if not self.sim_step_forward(True):
            self.sim_pause()
            QMessageBox.information(self, "Simulation paused", "No fireable transition, simulation paused.")
            self.stop_action.setEnabled(True)

    def sim_pause(self):
        # 暂停按钮响应处理
        # self.tab_widget.setEnabled(True)
        self.pause_action.setEnabled(False)
        if self.anim_timer.isActive():
            self.anim_timer.stop()
        # if self.still_anim_flag:
        self.norm_timer = QTimer(self)
        self.norm_timer.start(10)

        self.norm_timer.timeout.connect(self.toggle_pause_btn)

    def toggle_pause_btn(self):
        if not self.still_anim_flag:
            self.init_action.setEnabled(True)
            self.stop_action.setEnabled(True)
            self.step_forward_action.setEnabled(True)
            self.step_backward_action.setEnabled(True)
            self.forward_action.setEnabled(True)
            self.norm_timer.stop()

    def sim_stop(self):
        self.new_action.setEnabled(True)
        self.open_action.setEnabled(True)
        self.tab_widget.setEnabled(True)
        self.add_place_action.setEnabled(True)
        self.add_transition_action.setEnabled(True)
        self.add_arc_action.setEnabled(True)
        self.set_sim_btn_state(False, False, False, False, False, False)
        if self.anim_timer.isActive():
            self.anim_timer.stop()
        # self.norm_timer.setSingleShot(True)
        # self.norm_timer.start(self.anim_duration * 2)  # 设置计时间隔并启动
        # self.norm_timer.timeout.connect(self.reset_sim_state)
        self.reset_sim_state()
        self.statusBar().clearMessage()

    def reset_sim_state(self):
        self.set_sim_btn_state(True, False, True, True, False, True)
        self.init_marking_set(self.initial_marking)

    def set_sim_btn_state(self, init_flag, step_back_flag, step_forward_flag, play_flag, pause_flag, stop_flag):
        self.init_action.setEnabled(init_flag)
        self.step_backward_action.setEnabled(step_back_flag)
        self.step_forward_action.setEnabled(step_forward_flag)
        self.forward_action.setEnabled(play_flag)
        self.pause_action.setEnabled(pause_flag)
        self.stop_action.setEnabled(stop_flag)

    def mod_multi_index(self, index):
        self.multi_index = index
