import copy
import sys
import typing

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from forms.main_window import Ui_main_window
from src.serialization.xml_attribute import XmlPlaceAttr
from src.utils.util import Util



class Place(QGraphicsEllipseItem):
    """
    本类定义库所结点, 应具备一下功能：
    新建一个本类时,自动获得一个不重复的id
    本类应具备属性：容量, token数量, 输入弧集合, 输出弧集合
    实现序列化和反序列化功能
    获取前集和后集、检测是否合法、是否容量是否满
    """

    Type = QGraphicsItem.UserType + 1
    DEFAULT_PEN_COLOR = Qt.black
    DEFAULT_BRUSH_COLOR = Qt.white
    PLACE_DIAMETER = 30
    MAX_CAPACITY = 100000

    def __init__(self, place=None, parent=None):
        super(Place, self).__init__()

        self.amend_tokens = None
        self.parent = parent

        if place:
            self.identity_id = int(place.identity_id)
            self.place_id = int(place.id)
        else:
            self.identity_id = Util.get_int_ruid()  # 全局图元Id
            self.place_id = self.parent.id_util.get_place_id(self.identity_id)  # 库所id

        self.name = "p" + str(self.place_id)  # 图元显示的名称, 应具备唯一性
        self.input_arcs = []
        self.output_arcs = []
        self.tokens = 0
        self.capacity = 10000
        self.is_desc_show = False  # 结点描述信息是否显示
        self.description = "a normal place node"  # 结点描述信息
        self.brush_color = QColor(self.DEFAULT_BRUSH_COLOR)  # 结点绘制的画刷
        self.pen_color = QColor(self.DEFAULT_PEN_COLOR)  # 结点绘制的画笔

        self.setRect(0, 0, self.PLACE_DIAMETER, self.PLACE_DIAMETER)

        self.label = QGraphicsSimpleTextItem(self)  # 用于显示结点描述的文本图元
        self.label.setText(self.name)
        self.label.setPos(30, 30)  # 在父Item的坐标系中设置位置
        self.label.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.label.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.label.setFont(QFont("Times New Roman", 10))

        self.setZValue(1000.0)  # 位于图层的上方
        self.setFlag(QGraphicsItem.ItemIsMovable, True)  # 可以移动
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)  # 可以选择
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)

        self.is_desc_show = False

        self.anim_flag1 = False
        self.anim_flag2 = False

        global sec
        sec = 5
        self.timer_thread = timer_thread(3)
        self.timer_thread.timer.connect(self.time_changed)
        # self.timer_thread.timer.connect(self.get_main_win().disable_sim_btn)
        self.timer_thread.end.connect(self.end_timer)
        # self.timer_thread.end.connect(self.get_main_win().enable_sim_btn)

    def init_from_xml(self, place):
        self.place_id = place.id
        self.identity_id = place.identity_id
        self.name = place.name
        self.tokens = int(place.initmark)
        self.capacity = place.capacity
        self.description = place.comment
        self.brush_color = place.brushColor
        self.pen_color = place.penColor
        self.is_desc_show = place.show

    def to_xml(self) -> XmlPlaceAttr:
        place = XmlPlaceAttr()
        place.id = self.place_id
        place.identity_id = self.identity_id
        place.name = self.name
        place.initmark = self.tokens
        place.capacity = self.capacity
        place.x = self.pos().x()
        place.y = self.pos().y()
        place.offsetx = self.label.x()
        place.offsety = self.label.y()
        place.comment = self.description
        place.show = self.is_desc_show
        place.brushColor = self.brush_color
        place.penColor = self.pen_color
        return place

    def get_main_win(self):
        return self.parent.parent().view.main_win

    def add_input_arc(self, arc):
        self.input_arcs.append(arc)
        # self.input_arcs.add(arc)

    def add_output_arc(self, arc):
        self.output_arcs.append(arc)
        # self.output_arcs.add(arc)

    def delete_arc(self, arc_id):
        for arc in self.input_arcs:
            if arc.arc_id == arc_id:
                self.input_arcs.remove(arc)
                return
        for arc in self.output_arcs:
            if arc.arc_id == arc_id:
                self.output_arcs.remove(arc)
                return

    def update_tokens(self, token):
        # if token < 0 or token == 0:
        #     return
        self.tokens += token

    def check_reach_capacity(self, arc) -> bool:
        if self.capacity == 0:
            return False
        elif arc.weight + self.tokens > self.capacity:
            return True
        else:
            return False

    def check_has_relations(self) -> bool:
        if (not self.input_arcs) and (not self.output_arcs):
            return False
        else:
            return True

    # 检查是否存在回路, 必须是纯网关联矩阵才唯一
    def check_has_loop(self) -> bool:
        pre_set = self.get_pre_set()
        post_set = self.get_post_set()
        inter_set = list(set(pre_set) & set(post_set))
        if inter_set:
            return True
        else:
            return False

    def get_pre_set(self):
        pre_set = []
        for arc in self.input_arcs:
            pre_set.append(arc.source_item.identity_id)
        return pre_set

    def get_post_set(self):
        post_set = []
        for arc in self.output_arcs:
            post_set.append(arc.target_item.identity_id)
        return post_set

    @staticmethod
    def get_type() -> int:
        return Place.Type

    def get_bound_rect(self) -> QRectF:
        a = (self.pen().width()) / 2
        return QRectF(0, 0, 30, 30).normalized().adjusted(-a, -a, a, a)

    def shape(self) -> QPainterPath:
        shape = QPainterPath()
        shape.addEllipse(0, 0, self.PLACE_DIAMETER, self.PLACE_DIAMETER)
        return shape

    def itemChange(self, change: QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        """
        本图元若有位置变动则对应的弧连接也要一起变动位置
        """
        if change == QGraphicsItem.ItemPositionChange or change == QGraphicsItem.ItemPositionHasChanged:
            for arc in self.input_arcs:
                arc.update_position()
            for arc in self.output_arcs:
                arc.update_position()
        return value


    def time_changed(self, flag):
        global sec
        sec -= 1
        if flag:
            self.anim_flag1 = not self.anim_flag1
        else:
            self.anim_flag2 = not self.anim_flag2
        self.update()

    def end_timer(self):
        self.update_tokens(self.amend_tokens)
        self.anim_flag1 = False
        self.anim_flag2 = False
        self.update()
        global sec
        sec = 5

    def anim(self, flag, weight):
        self.amend_tokens = weight
        if flag:
            self.timer_thread.flag = 1
        else:
            self.timer_thread.flag = 2
        self.timer_thread.start()



    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem, widget: typing.Optional[QWidget] = ...):

        if not self.check_has_relations():
            pen_color = Qt.darkRed
            brush_color = QColor(255, 240, 240)
        elif self.isSelected():
            for arc in self.input_arcs:
                arc.setSelected(True)
            for arc in self.output_arcs:
                arc.setSelected(True)
            pen_color = Qt.blue
            brush_color = QColor(240, 240, 255)
        else:
            pen_color = self.pen_color
            brush_color = self.brush_color

        painter.setPen(QPen(pen_color, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
        self.label.setBrush(pen_color)
        if self.is_desc_show:
            self.label.setText(self.name + "\n" + self.description)
        else:
            self.label.setText(self.name)
        if self.anim_flag1:
            brush_color = Qt.red
        if self.anim_flag2:
            brush_color = Qt.green
        painter.setBrush(brush_color)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.drawEllipse(0, 0, self.PLACE_DIAMETER, self.PLACE_DIAMETER)

        painter.setBrush(QColor(0, 0, 0))

        if self.tokens == 1:
            painter.drawEllipse(10, 10, 10, 10)
        elif self.tokens == 2:
            painter.drawEllipse(15, 15, 10, 10)
            painter.drawEllipse(5, 5, 10, 10)
        elif self.tokens == 3:
            painter.drawEllipse(15, 15, 10, 10)
            painter.drawEllipse(8, 3, 10, 10)
            painter.drawEllipse(3, 15, 10, 10)
        elif self.tokens == 0:
            pass
        else:
            if self.tokens < 100:
                painter.drawText(7, 20, str(self.tokens))
            else:
                painter.drawText(4, 20, str(self.tokens))

    def collidesWithItem(self, other: QGraphicsItem, mode: Qt.ItemSelectionMode = ...) -> bool:
        path = QPainterPath(self.mapFromItem(other, other.shape()))
        return path.intersects(self.shape())


class timer_thread(QThread):
    timer = pyqtSignal(bool)
    end = pyqtSignal()

    def __init__(self, flag):
        super().__init__()
        self.flag = flag

    global sec
    def run(self) -> None:
        while True:
            self.msleep(100)
            if sec == 0 or sec < 0:
                self.end.emit()
                break
            if self.flag == 1:
                self.timer.emit(True)
            elif self.flag == 2:
                self.timer.emit(False)

