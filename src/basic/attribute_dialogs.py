from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from forms.placeEditDialog import Ui_PlaceDialog
from forms.graphicsEditTab import Ui_GraphicsEditTab
from forms.transitionEditDialog import Ui_TransitionDialog
from forms.arcEditDialog import Ui_ArcDialog


class PlaceDialog(QDialog):
    def __init__(self, item):
        super(PlaceDialog, self).__init__()

        self.ui = Ui_PlaceDialog()
        self.ui.setupUi(self)
        self.ui.tabWidget.addTab(GraphicsEditTab(self), "Graphic")
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.place = item
        self.ui.inputID.setText(str(self.place.place_id))
        self.ui.inputName.setText(self.place.name)
        self.ui.inputTokens.setText(str(self.place.tokens))
        self.ui.inputCapacity.setText(str(self.place.capacity))
        self.ui.chkBoxShow.setChecked(self.place.is_desc_show)
        self.ui.commentEdit.setText(self.place.description)

        self.ui.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.value_check)

        # self.token_validator = QDoubleValidator(0.0, 1000, 2, self)
        # self.ui.inputTokens.setValidator(self.token_validator)

    def value_check(self):
        tokens_str = self.ui.inputTokens.text().strip()
        capacity_str = self.ui.inputCapacity.text().strip()
        tokens = 0 if tokens_str == "" else int(self.ui.inputTokens.text())
        capacity = 0 if capacity_str == "" else int(self.ui.inputCapacity.text())
        if tokens > capacity:
            QMessageBox().critical(self, "Warning", "Tokens > Capacity", buttons=QMessageBox.Ok)
            self.reject()
        else:
            self.accept()


class TransitionDialog(QDialog):
    def __init__(self, item):
        super(TransitionDialog, self).__init__()
        self.ui = Ui_TransitionDialog()
        self.ui.setupUi(self)
        self.ui.tabWidget.addTab(GraphicsEditTab(self), "Graphic")
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.transition = item
        self.ui.editID.setText(str(self.transition.transition_id))
        self.ui.editName.setText(str(self.transition.name))
        self.ui.editComment.setText(self.transition.description)
        self.ui.chkBoxShow.setChecked(self.transition.is_desc_show)


class ArcDialog(QDialog):
    def __init__(self, item):
        super(ArcDialog, self).__init__()
        self.ui = Ui_ArcDialog()
        self.ui.setupUi(self)
        self.ui.tabWidget.addTab(GraphicsEditTab(self), "Graphic")
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.arc = item
        self.ui.inputID.setText(str(item.arc_id))
        self.ui.inputName.setText(item.name)
        self.ui.inputWeight.setText(str(item.weight))

# 颜色组件
class GraphicsEditTab(QWidget):
    def __init__(self, parent=None):
        super(GraphicsEditTab, self).__init__()
        self.setParent(parent)
        self.penPalette = QPalette()
        self.brushPalette = QPalette()
        self.penColor = QColor()
        self.brushColor = QColor()
        self.originPenColor = QColor()
        self.originBrushColor = QColor()
        self.ui = Ui_GraphicsEditTab()
        self.ui.setupUi(self)
        self.initSignals()

    def initSignals(self):
        self.ui.btnPenColorSelect.clicked.connect(self.selectPenColor)
        self.ui.btnBrushColorSelect.clicked.connect(self.selectBrushColor)
        self.ui.btnPenColorReset.clicked.connect(self.resetPenColor)
        self.ui.btnBrushColorReset.clicked.connect(self.resetBrushColor)

    def setPenColor(self, color):
        self.penColor = color
        pen_pal = self.ui.btnPenColorSelect.palette()
        pen_pal.setColor(QPalette.Button, self.penColor)
        self.ui.btnPenColorSelect.setPalette(pen_pal)
        self.ui.btnPenColorSelect.setAutoFillBackground(True)
        self.ui.btnPenColorSelect.setFlat(True)
        self.ui.btnPenColorSelect.update()

    def setBrushColor(self, color):
        self.brushColor = color
        pen_pal = self.ui.btnBrushColorSelect.palette()
        pen_pal.setColor(QPalette.Button, self.brushColor)
        self.ui.btnBrushColorSelect.setPalette(pen_pal)
        self.ui.btnBrushColorSelect.setAutoFillBackground(True)
        self.ui.btnBrushColorSelect.setFlat(True)
        self.ui.btnBrushColorSelect.update()


    def selectPenColor(self):
        dialog = QColorDialog()
        dialog.exec()
        if dialog.selectedColor() != QColor.Invalid:
            self.setPenColor(dialog.selectedColor())
            self.originPenColor = dialog.selectedColor()

    def selectBrushColor(self):
        dialog = QColorDialog()
        dialog.exec()
        if dialog.selectedColor() != QColor.Invalid:
            self.setBrushColor(dialog.selectedColor())
            self.originBrushColor = dialog.selectedColor()

    def resetPenColor(self):
        self.setPenColor(Qt.black)

    def resetBrushColor(self):
        self.setBrushColor(Qt.white)

import sys
if __name__ == '__main__':
    app = QApplication(sys.argv)
    # dialog = PlaceDialog()
    dialog = TransitionDialog()
    # dialog = ArcDialog()
    dialog.show()
    sys.exit(app.exec_())