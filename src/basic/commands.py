from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc import Arc


class AddPlaceCommand(QUndoCommand):
    def __init__(self, item_pos: QPointF, scene):
        super(AddPlaceCommand, self).__init__()
        self.place = Place(parent=scene)       # 新的库所
        self.position = item_pos   # 图元位置
        self.scene = scene         # 要操作的场景

    def undo(self):
        self.scene.id_util.place_id_list[int(self.place.place_id)] = None
        self.scene.removeItem(self.place)
        self.scene.update()

    def redo(self):
        self.scene.addItem(self.place)
        self.place.setPos(self.position)
        self.scene.update()


class AddTransitionCommand(QUndoCommand):
    def __init__(self, item_pos: QPointF, scene):
        super(AddTransitionCommand, self).__init__()
        self.transition = Transition(parent=scene)
        self.position = item_pos
        self.scene = scene

    def undo(self):
        self.scene.removeItem(self.transition)
        self.scene.id_util.trans_id_list[int(self.transition.transition_id)] = None
        self.scene.update()

    def redo(self):
        self.scene.addItem(self.transition)
        self.transition.setPos(self.position)
        self.scene.update()


class RemoveNodeCommand(QUndoCommand):
    def __init__(self, item, scene):
        super(RemoveNodeCommand, self).__init__()
        self.item = item
        self.position = self.item.pos()
        self.scene = scene

    def undo(self):
        self.scene.addItem(self.item)
        self.item.setPos(self.position)
        if self.item.Type == Place.Type:
            self.scene.id_util.place_id_list[int(self.item.place_id)] = self.item.identity_id
        elif self.item.Type == Transition.Type:
            self.scene.id_util.trans_id_list[int(self.item.transition_id)] = self.item.identity_id
        self.scene.update()

    def redo(self):
        self.scene.removeItem(self.item)
        if self.item.Type == Place.Type:
            self.scene.id_util.place_id_list[int(self.item.place_id)] = None
        elif self.item.Type == Transition.Type:
            self.scene.id_util.trans_id_list[int(self.item.transition_id)] = None
        self.scene.update()


class AddArcCommand(QUndoCommand):
    def __init__(self, source_item, target_item, arc_path, scene, weight):
        super(AddArcCommand, self).__init__()
        self.source_item = source_item
        self.target_item = target_item
        self.weight = weight
        self.scene = scene
        self.arc = Arc(parent=scene)
        self.arc.init_arc(self.source_item, self.target_item, arc_path, self.weight)

    def undo(self):
        self.scene.removeItem(self.arc)
        self.remove_connections()
        self.scene.id_util.arc_id_list[int(self.arc.arc_id)] = None
        self.scene.update()

    def redo(self):
        self.add_connections()
        self.arc.update_position()
        self.scene.addItem(self.arc)
        self.scene.update()

    def add_connections(self):
        self.source_item.add_output_arc(self.arc)
        self.target_item.add_input_arc(self.arc)

    def remove_connections(self):
        self.source_item.delete_arc(self.id)
        self.target_item.delete_arc(self.id)


class RemoveArcCommand(QUndoCommand):
    def __init__(self, item, scene):
        super(RemoveArcCommand, self).__init__()
        self.arc = item
        self.scene = scene
        self.source_item = self.arc.source_item
        self.target_item = self.arc.target_item
        self.aid = self.arc.arc_id

    def undo(self):
        self.scene.addItem(self.arc)
        self.add_connections()
        self.scene.id_util.arc_id_list[int(self.arc.arc_id)] = self.arc.identity_id
        self.scene.update()

    def redo(self):
        self.scene.removeItem(self.arc)
        self.scene.id_util.arc_id_list[int(self.arc.arc_id)] = None
        self.remove_connections()
        self.scene.update()

    def add_connections(self):
        self.source_item.add_output_arc(self.arc)
        self.target_item.add_input_arc(self.arc)

    def remove_connections(self):
        self.source_item.delete_arc(self.aid)
        self.target_item.delete_arc(self.aid)


class MoveCommand(QUndoCommand):
    def __init__(self, moved_item, old_position, scene):
        super(MoveCommand, self).__init__()
        self.scene = scene
        self.item = moved_item
        self.oldPos = old_position
        self.newPos = self.item.pos()

    def undo(self):
        self.item.setPos(self.oldPos)
        self.scene.update()

    def redo(self):
        self.item.setPos(self.newPos)
        self.scene.update()


class UndoStack(QUndoStack):
    """
    undo栈， 定义相关槽函数
    """
    def __init__(self, parent=None):
        super(UndoStack, self).__init__()
        self.parent = parent

    def arc_inserted(self, source_item, target_item, arc_path, scene, weight):
        self.push(AddArcCommand(source_item, target_item, arc_path, scene, weight))

    def item_moved(self, moved_item, old_position, scene):
        self.push(MoveCommand(moved_item, old_position, scene))

    def node_removed(self, item, scene):
        self.push(RemoveNodeCommand(item, scene))

    def arc_removed(self, item, scene):
        self.push(RemoveArcCommand(item, scene))


