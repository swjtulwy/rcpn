import math
import sys
import typing

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from forms.main_window import Ui_main_window
from src.serialization.xml_attribute import XmlArcAttr
from src.utils.util import Util
from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc_rect import ArcRect


class Ball(QGraphicsEllipseItem):
    def __init__(self, parent, color):
        super(Ball, self).__init__()
        self.parent = parent
        self.color = color
        self.setRect(0, 0, 10, 10)
        self.setBrush(self.color)
        self.adapter = BallAdapter(self, self)


class BallAdapter(QObject):
    def __init__(self, parent, object_to_animate):
        super(BallAdapter, self).__init__()
        self.object_to_animate = object_to_animate

    def __get_position(self):
        return self.object_to_animate.pos()

    def __set_position(self, pos):
        self.object_to_animate.setPos(pos)

    position = pyqtProperty(QPointF, __get_position, __set_position)


class Arc(QGraphicsPathItem):
    Type = QGraphicsItem.UserType + 3
    DEFAULT_PEN_COLOR = Qt.black
    DEFAULT_BRUSH_COLOR = Qt.white
    OUTLINE_WIDTH = 20
    ARC_SIZE = 10.0
    ARC_ANGLE = math.pi / 3

    def __init__(self, arc=None, parent=None):
        super(Arc, self).__init__()

        self.parent = parent
        self.identity_id = None
        self.arc_id = None

        if arc:
            self.identity_id = arc.identity_id
            self.arc_id = arc.id
        else:
            self.identity_id = Util.get_int_ruid()
            self.arc_id = self.parent.id_util.get_arc_id(self.identity_id)

        self.name = "a" + str(self.arc_id)
        self.source_item = None  # 源结点
        self.target_item = None  # 目的结点

        self.arc_path = None
        self.weight = 1
        self.arc_head = QPolygonF()  # 弧箭头
        self.color = QColor()
        self.rects = []  # 折线关节矩形
        self.label = None

        self.brush_color = QColor(self.DEFAULT_BRUSH_COLOR)
        self.pen_color = QColor(self.DEFAULT_PEN_COLOR)

        self.ball = None
        self.anim_group = None  # 多段动画构成组，因为有可能有折线
        self.anim_reverse_flag = False # 是否反向模拟

    def init_arc(self, source_item, target_item, arc_path, weight):
        self.source_item = source_item
        self.target_item = target_item
        self.setPath(arc_path)
        self.weight = weight
        self.create_arc()

    def init_from_xml(self, source_item, target_item, arc_path: QPainterPath, arc: XmlArcAttr):
        """
        从xml结构中初始化
        """
        self.identity_id = arc.identity_id
        self.arc_id = arc.id
        self.source_item = source_item
        self.target_item = target_item
        self.setPath(arc_path)
        self.weight = arc.weight
        self.brush_color = arc.brushColor
        self.pen_color = arc.penColor
        self.create_arc()
        self.update_position()

    def create_arc(self):
        """
        创建弧的一些公共操作
        """
        self.setZValue(-1000.0)
        self.setFlags(QGraphicsItem.ItemIsSelectable | QGraphicsItem.ItemSendsGeometryChanges)
        self.setSelected(True)
        # 设置关节点
        i = 1
        while i < self.path().elementCount() - 1:
            rec = ArcRect(7.0, parent=self)
            elem = self.path().elementAt(i)
            rec.setPos(elem.x - 7 / 2.0, elem.y - 7 / 2.0)
            self.rects.append(rec)
            i += 1
        self.label = QGraphicsSimpleTextItem(self)
        self.label.setFont(QFont("Times New Roman", 10))

    def to_xml(self):
        arc = XmlArcAttr()
        arc.id = self.arc_id
        arc.identity_id = self.identity_id
        arc.source = self.source_item.identity_id
        arc.target = self.target_item.identity_id

        arc.weight = self.weight
        arc.brushColor = self.brush_color
        arc.penColor = self.pen_color
        for i in range(1, self.path().elementCount() - 1):
            arc.points.append(self.path().elementAt(i))
        return arc

    def show_arc_rects(self, show):
        for rec in self.rects:
            rec.setVisible(show)

    def update_position(self):
        # 源节点的中心位置
        p1 = self.mapFromItem(self.source_item, self.source_item.get_bound_rect().center())
        # 目标节点的中心位置
        p2 = self.mapFromItem(self.target_item, self.target_item.get_bound_rect().center())

        # 绘图路径中首尾节点
        path = self.path()
        path.setElementPositionAt(0, p1.x(), p1.y())
        path.setElementPositionAt(path.elementCount() - 1, p2.x(), p2.y())
        self.setPath(path)

    @staticmethod
    def get_type():
        return Arc.Type

    def shape(self):
        current_path = self.path()
        # 添加箭头
        current_path.addPolygon(self.arc_head)
        painter_path_stroker = QPainterPathStroker()
        painter_path_stroker.setWidth(self.OUTLINE_WIDTH)
        # 为当前绘图路径添加轮廓
        return painter_path_stroker.createStroke(current_path)

    def bounding_rect(self):
        a = self.pen().width() / 2.0 + self.ARC_SIZE / 2.0
        return QRectF(self.path().controlPointRect()).normalized().adjusted(-a, -a, a, a)

    def animation(self, is_out_arc_flag, reverse_flag, anim_duration, ball_color):
        """
        is_out_arc_flag: 是否为输出弧
        reverse_flag: 是否进行反向模拟
        anim_duration: 模拟时长
        """
        self.anim_reverse_flag = reverse_flag
        self.ball = Ball(self.parent.parent(), ball_color)
        self.parent.addItem(self.ball)
        p1 = self.mapFromItem(self.source_item, self.source_item.get_bound_rect().center())
        p2 = self.mapFromItem(self.target_item, self.target_item.get_bound_rect().center())
        
        # 多段折线
        # if len(self.rects) > 0:
        rects = [rect.pos() for rect in self.rects]
        rects.insert(0, p1)
        rects.append(p2)
        if reverse_flag:
            rects = rects[::-1]
        self.anim_group = QSequentialAnimationGroup()
        if is_out_arc_flag:
            animation_object = QPropertyAnimation(self.ball.adapter, b'position')
            animation_object.setDuration(anim_duration)
            animation_object.setStartValue(rects[0])
            animation_object.setEndValue(rects[0])
            animation_object.setLoopCount(1)
            self.anim_group.addAnimation(animation_object)
        for i in range(len(rects) - 1):
            animation_object = QPropertyAnimation(self.ball.adapter, b'position')
            animation_object.setDuration(anim_duration // (len(rects) - 1))
            animation_object.setStartValue(rects[i])
            animation_object.setEndValue(rects[i + 1])
            animation_object.setLoopCount(1)
            self.anim_group.addAnimation(animation_object)
        self.anim_group.start()
        self.anim_group.finished.connect(self.animation_finished)

    def animation_finished(self):
        self.parent.removeItem(self.ball)
        if self.anim_reverse_flag:
            if self.source_item.get_type() == Place.get_type():
                self.source_item.update_tokens(self.weight)
        else:
            if self.target_item.get_type() == Place.get_type():
                self.target_item.update_tokens(self.weight)
        self.parent.update()

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem,
              widget: typing.Optional[QWidget] = ...) -> None:
        # 源节点和目标节点相交则直接退出
        if self.source_item.collidesWithItem(self.target_item, Qt.IntersectsItemShape):
            return
        paint_path = self.path()
        elems = paint_path.elementCount()

        # 最后一段折线
        last_line = QLineF(paint_path.elementAt(elems - 2).x, paint_path.elementAt(elems - 2).y,
                           paint_path.elementAt(elems - 1).x, paint_path.elementAt(elems - 1).y)
        # 判断目标节点类型, head_point 记录最后一条折线与目标结点相交的位置
        if self.target_item.get_type() == Place.get_type() or self.target_item.type() == QGraphicsItem.UserType + 4:
            head_point = self.intersection_point_circle_line(self.target_item, last_line)
        else:
            head_point = self.intersection_point_rect_line(self.target_item, last_line)

        # 关节矩形中心点填入绘图路径
        i = 1
        for rec in self.rects:
            p = self.mapFromItem(rec, rec.boundingRect().center())
            paint_path.setElementPositionAt(i, p.x(), p.y())
            i += 1

        # 构造箭头
        # 求出最后折线的与水平线的角度
        angle = math.acos(last_line.dx() * 1.0 / last_line.length())
        if last_line.dy() >= 0:
            angle = 2 * math.pi - angle
        # p1, p2, p3 为箭头多边形的三个顶点
        p1 = head_point + QPointF(math.sin(angle - self.ARC_ANGLE) * self.ARC_SIZE,
                                  math.cos(angle - self.ARC_ANGLE) * self.ARC_SIZE)

        p3 = head_point + QPointF(math.sin(angle - math.pi + self.ARC_ANGLE) * self.ARC_SIZE,
                                  math.cos(angle - math.pi + self.ARC_ANGLE) * self.ARC_SIZE)

        # p2 位折线上的一个点
        line = QLineF(head_point.x(), head_point.y(), last_line.x1(), last_line.y1())
        p2 = line.pointAt(5.0 / (line.length() + 0.0001))

        # 四个点构成箭头
        self.arc_head.clear()
        self.arc_head.append(head_point)
        self.arc_head.append(p1)
        self.arc_head.append(p2)
        self.arc_head.append(p3)

        # 选中显示关节
        if self.isSelected():
            self.show_arc_rects(True)
            self.color = QColor(0, 0, 150)
        else:
            self.show_arc_rects(False)
            self.color = self.pen_color

        # 画各个折线
        color = self.color
        # if self.anim_flag1:
        #     color = Qt.red
        # if self.anim_flag2:
        #     color = Qt.green
        painter.setPen(QPen(color, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(Qt.NoBrush)
        painter.drawPath(paint_path)

        # 画箭头
        painter.setBrush(color)
        painter.drawPolygon(self.arc_head)

        # 画标签
        self.label.setText(str(self.weight))
        self.label.setPos(paint_path.pointAtPercent(0.5) + QPointF(-10, 0))
        self.label.setBrush(color)

        if self.weight == 1.0:
            self.label.hide()
        else:
            self.label.show()
        self.setPath(paint_path)

    @staticmethod
    def intersection_point_circle_line(circle: QGraphicsItem, line: QLineF):
        """
        计算直线与圆的交点
        """
        R = 15
        x0 = (circle.mapToScene(circle.shape().controlPointRect().center())).x()
        y0 = (circle.mapToScene(circle.shape().controlPointRect().center())).y()
        # 非垂直
        if line.x2() != line.x1():
            # y轴交点
            yaxis_intersection = QPointF()
            line.intersect(QLineF(QPointF(0, 10000), QPointF(0, -10000)), yaxis_intersection)
            # 斜率
            a = (line.y2() - line.y1()) / (line.x2() - line.x1())
            # 截距
            b = yaxis_intersection.y()
            A = 1 + a * a
            B = 2 * (a * b - a * y0 - x0)
            C = x0 * x0 + y0 * y0 + b * b - 2 * b * y0 - R * R

            Q = B * B - 4 * A * C
            if Q < 0:
                Q = 0
            s1 = (-1) * (B + math.sqrt(Q)) / (2 * A)
            s2 = (math.sqrt(Q) - B) / (2 * A)
            ps1 = QPointF(s1, a * s1 + b)
            ps2 = QPointF(s2, a * s2 + b)

            # 直线与圆有两个交点, 返回近点
            if QLineF(line.p1(), ps1).length() <= QLineF(line.p1(), ps2).length():
                return ps1
            else:
                return ps2
        else:
            x = line.x1()
            C = (x - x0) * (x - x0) + y0 * y0 - R * R
            Q = 4 * y0 * y0 - 4 * C
            s1 = y0 - math.sqrt(Q) / 2
            s2 = y0 + math.sqrt(Q) / 2
            ps1 = QPointF(x, s1)
            ps2 = QPointF(x, s2)

            if QLineF(line.p1(), ps1).length() <= QLineF(line.p1(), ps2).length():
                return ps1
            else:
                return ps2

    @staticmethod
    def intersection_point_rect_line(rectangle: QGraphicsRectItem, line: QLineF):
        """
        算直线与矩形的交点
        """
        list = []
        shape = QPainterPath(rectangle.mapToScene(rectangle.shape()))
        line5 = QLineF(shape.elementAt(0).x, shape.elementAt(0).y, shape.elementAt(1).x, shape.elementAt(1).y)
        line6 = QLineF(shape.elementAt(1).x, shape.elementAt(1).y, shape.elementAt(2).x, shape.elementAt(2).y)
        line7 = QLineF(shape.elementAt(2).x, shape.elementAt(2).y, shape.elementAt(3).x, shape.elementAt(3).y)
        line8 = QLineF(shape.elementAt(3).x, shape.elementAt(3).y, shape.elementAt(4).x, shape.elementAt(4).y)
        list.append(line5)
        list.append(line6)
        list.append(line7)
        list.append(line8)
        intersect_point = QPointF()
        point = QPointF()
        for l in list:
            if line.intersect(l, intersect_point) == QLineF.BoundedIntersection:
                point = intersect_point
                # 一旦找到一条就停止
                break
        return point


