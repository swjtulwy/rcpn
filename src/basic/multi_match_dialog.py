from PyQt5 import QtGui
from PyQt5.QtCore import QSize, pyqtSignal, Qt, QModelIndex
from PyQt5.QtGui import QIcon, QPainterPath
from PyQt5.QtWidgets import QWidget, QListWidgetItem, QGraphicsScene, QDialog
from forms.multi_match_dialog import Ui_MultiMatchForm
from src.basic.arc import Arc
from src.basic.transition import Transition
from src.utils.id_util import Id_Util
from src.basic.place import Place


class MultiMatchDialog(QDialog):

    _select_sig = pyqtSignal(int)

    def __init__(self, parent, subgraph_list, arcs):
        super(MultiMatchDialog, self).__init__()
        self.parent = parent
        self.subgraph_list = subgraph_list
        self.arcs = arcs
        self.ui = Ui_MultiMatchForm()
        self.ui.setupUi(self)
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        self.setWindowTitle("Multi Match Dialog")
        self.ui.subgraphList.itemClicked.connect(self.itemClicked)
        self.scene = QGraphicsScene()
        self.scene.setSceneRect(0, 0, 1400, 900)
        self.scene.id_util = Id_Util()
        self.ui.graphicsView.setScene(self.scene)
        self.ui.graphicsView.mapToScene(self.ui.graphicsView.viewport().rect().center())

        for graph in subgraph_list:
            item = QListWidgetItem()
            item.setSizeHint(QSize(60, 60))
            names = []
            for node in graph:
                names.append(node.name)
            item.setText(" ".join(names))
            self.ui.subgraphList.addItem(item)
        self.ui.subgraphList.setCurrentRow(0)
        self.ui.lineEdit.setText(self.ui.subgraphList.currentItem().text())
        self.setWindowFlags(Qt.Dialog | Qt.WindowMinMaxButtonsHint | Qt.WindowCloseButtonHint)
        self._select_sig.connect(self.parent.mod_multi_index)

        self.scale_m = 1
        self.ui.selectBtn.clicked.connect(self.select)
        self.ui.btn_cancel.clicked.connect(self.reject_action)

    def find_item_by_name(self, name, nodes):
        for node in nodes:
            if node.name == name:
                return node

    def find_item_by_identity_id(self, id, nodes):
        for node in nodes:
            if str(node.identity_id) == str(id):
                return node

    def itemClicked(self, list_item):
        self.scene.clear()
        index = self.ui.subgraphList.row(list_item)
        subgraph_nodes = self.subgraph_list[index]
        for node in subgraph_nodes:
            if node.Type == Place.Type:
                item = Place(parent=self.scene)
                item.setPos(node.pos().x(), node.pos().y())
                item.name = node.name
                self.scene.addItem(item)
            elif node.Type == Transition.Type:
                item = Transition(parent=self.scene)
                item.setPos(node.pos().x(), node.pos().y())
                item.name = node.name
                self.scene.addItem(item)

        for arc in self.arcs[index]:
            source_item = self.find_item_by_name(arc.source_item.name, subgraph_nodes)
            target_item = self.find_item_by_name(arc.target_item.name, subgraph_nodes)
            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            new_arc = Arc(parent=self.scene)
            new_arc.init_arc(source_item, target_item, path, 1)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)
            new_arc.update_position()
            self.scene.addItem(new_arc)
            self.scene.update()

        self.ui.lineEdit.setText(list_item.text())

    def select(self):
        self._select_sig.emit(self.ui.subgraphList.currentRow())
        self.accept()

    def reject_action(self):
        self.reject()

    def close(self) -> bool:
        self.reject()

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        if event.modifiers() == Qt.ControlModifier:
            self.scale_fun1(event)
        else:
            QWidget.wheelEvent(self, event)

    def scale_fun1(self, event: QtGui.QWheelEvent):
        if event.modifiers() == Qt.ControlModifier:
            if event.angleDelta().y() > 0 and self.scale_m >= 50:
                return
            elif event.angleDelta().y() < 0 and self.scale_m <= 0.01:
                return
            else:
                scale_factor = self.ui.graphicsView.transform().m11()
                self.scale_m = scale_factor
                wheel_data_value = event.angleDelta().y()
                if wheel_data_value > 0:
                    self.ui.graphicsView.scale(1.1, 1.1)
                else:
                    self.ui.graphicsView.scale(1.0 / 1.1, 1.0 / 1.1)
                self.ui.graphicsView.update()



