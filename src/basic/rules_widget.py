import json

import igraph
from PyQt5.QtCore import QFile, QIODevice, QTextStream, QDir, Qt
from PyQt5.QtGui import QIcon, QPixmap, QPainterPath
from PyQt5.QtWidgets import QWidget, QAbstractItemView, QHeaderView, QTableWidget, QTableWidgetItem, QMessageBox, \
    QFileDialog, QDialog
from forms.rules_widget import Ui_RulesWindow
from src.analysis.dynamic_property import DynamicProperty
from src.basic.arc import Arc
from src.basic.place import Place
from src.basic.rule_dialog import RuleDialog
from src.basic.multi_match_dialog import MultiMatchDialog
from src.basic.transition import Transition
from src.serialization.xml_parser import XmlParser


class RulesWidget(QWidget):
    def __init__(self, parent):
        super(RulesWidget, self).__init__()
        self.rule_dialog = None
        self.multi_match_dialog = None
        self.parent = parent
        self.cur_tab = self.parent.get_current_tab()

        self.ui = Ui_RulesWindow()
        self.ui.setupUi(self)
        self.ui.ruleList.setColumnCount(5)
        # self.ui.ruleList.setHorizontalHeaderLabels(["ID", "Name", "LHS-path",
        #                                             "RHS-path", "Rule-path", "Input_Lhs", "Output_Lhs",
        #                                             "Input_Rhs", "Output_Rhs"])
        self.ui.ruleList.setHorizontalHeaderLabels(["ID", "Name", "LHS-path",
                                                    "RHS-path", "Rule-path"])
        self.setWindowIcon(QIcon("./resources/application-icon-red.svg"))

        self.ui.label_cur_dir.setText(QDir.currentPath())

        self.ui.ruleList.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.ui.ruleList.setSelectionMode(QAbstractItemView.SingleSelection)
        self.ui.ruleList.horizontalHeader().setStretchLastSection(True)
        self.ui.ruleList.horizontalHeader().setSectionsClickable(False)
        self.ui.ruleList.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.ui.ruleList.horizontalHeader().setSectionResizeMode(0, QHeaderView.Fixed)
        self.ui.ruleList.setColumnWidth(0, 140)
        # self.ui.ruleList.horizontalHeader().setSectionResizeMode(1, QHeaderView.Fixed)
        # self.ui.ruleList.setColumnWidth(1, 200)
        self.ui.ruleList.horizontalHeader().setStyleSheet("QHeaderView::section{background:skyblue;}")
        self.ui.ruleList.verticalHeader().setVisible(False)
        self.ui.ruleList.setShowGrid(True)
        self.ui.ruleList.setEditTriggers(QTableWidget.NoEditTriggers)

        self.ui.btnNewRule.clicked.connect(self.new_rule)
        self.ui.btnImport.clicked.connect(self.import_rule)
        self.ui.btnDetail.clicked.connect(self.edit_rule)
        self.ui.btnRemove.clicked.connect(self.remove_rule)
        self.ui.btnCheck.clicked.connect(self.check)
        self.ui.btnApply.clicked.connect(self.apply)

        self.multi_index = 0

        self.rule_names_set = set()
        self.init_rules()

    def mod_multi_index(self, index):
        self.multi_index = index

    # 读取规则填充到列表
    def init_rules(self):
        lib_dir = QDir(QDir.currentPath() + "/rules")
        file_name_filters = ["*.json"]
        file_infos = lib_dir.entryInfoList(file_name_filters, QDir.Files | QDir.Readable, QDir.Name)
        file_paths = [info.absoluteFilePath() for info in file_infos]

        for json_path in file_paths:
            if json_path == "":
                continue
            with open(json_path, 'r') as json_file:
                json_str = json.load(json_file)
            json_str = json.dumps(json_str)
            try:
                rule_object = json.loads(json_str)
            except ValueError:
                continue

            self.add_item(rule_object["Id"],
                          rule_object["Name"],
                          rule_object["Lhs"],
                          rule_object["Rhs"],
                          rule_object["Path"]
                          )
            self.rule_names_set.add(rule_object["Name"])
        self.ui.ruleList.selectRow(0)

    def new_rule(self):
        self.rule_dialog = RuleDialog(parent=self)
        ret = self.rule_dialog.exec_()
        if ret == QDialog.Rejected:
            return

    def import_rule(self):
        directory, _ = QFileDialog.getOpenFileName(self, "Select Json File", QDir.currentPath() + "/rules",
                                                   "Json Files (*.json)")
        if directory == "":
            return
        with open(directory, 'r') as json_file:
            json_str = json.load(json_file)
        json_str = json.dumps(json_str)
        try:
            rule_object = json.loads(json_str)
        except ValueError:
            QMessageBox.critical(self, 'Error', 'Json format error!')
            return
        rule_list = self.ui.ruleList
        rows = rule_list.rowCount()
        for r in range(0, rows):
            rule_id = rule_list.item(r, 0).text()
            if rule_id == rule_object["Id"]:
                QMessageBox.warning(self, 'Warning', 'Rule already exists!')
                return
        # self.parent.rule_data.append(rule_object)
        self.add_item(rule_object["Id"],
                      rule_object["Name"],
                      rule_object["Lhs"],
                      rule_object["Rhs"],
                      rule_object["Path"]
                      )

    def mod_item(self, id, name, lhs_path, rhs_path, path):
        lhs_path = "/".join(lhs_path.split('/')[-2:])
        rhs_path = "/".join(rhs_path.split('/')[-2:])
        path = "/".join(path.split('/')[-2:])
        cur_row = self.ui.ruleList.currentRow()
        self.ui.ruleList.setItem(cur_row, 0, QTableWidgetItem(id))
        self.ui.ruleList.setItem(cur_row, 1, QTableWidgetItem(name))
        self.ui.ruleList.setItem(cur_row, 2, QTableWidgetItem(lhs_path))
        self.ui.ruleList.setItem(cur_row, 3, QTableWidgetItem(rhs_path))
        self.ui.ruleList.setItem(cur_row, 4, QTableWidgetItem(path))
        for i in range(5):
            self.ui.ruleList.item(cur_row, i).setTextAlignment(Qt.AlignCenter)

    def add_item(self, id, name, lhs_path, rhs_path, path):
        lhs_path = "/".join(lhs_path.split('/')[-2:])
        rhs_path = "/".join(rhs_path.split('/')[-2:])
        path = "/".join(path.split('/')[-2:])
        rows = self.ui.ruleList.rowCount()
        self.ui.ruleList.setRowCount(rows + 1)
        self.ui.ruleList.setItem(rows, 0, QTableWidgetItem(id))
        self.ui.ruleList.setItem(rows, 1, QTableWidgetItem(name))
        self.ui.ruleList.setItem(rows, 2, QTableWidgetItem(lhs_path))
        self.ui.ruleList.setItem(rows, 3, QTableWidgetItem(rhs_path))
        self.ui.ruleList.setItem(rows, 4, QTableWidgetItem(path))
        self.ui.ruleList.selectRow(rows)
        for i in range(5):
            self.ui.ruleList.item(rows, i).setTextAlignment(Qt.AlignCenter)

    def edit_rule(self):
        current_row = self.ui.ruleList.currentRow()
        if current_row < 0:
            QMessageBox.warning(self, 'Warning', 'Please select a rule!')
            return
        rule_name = self.ui.ruleList.item(current_row, 1).text()
        rule_lhs_path = QDir.currentPath() + "/" + self.ui.ruleList.item(current_row, 2).text()
        rule_rhs_path = QDir.currentPath() + "/" + self.ui.ruleList.item(current_row, 3).text()
        rule_rule_path = QDir.currentPath() + "/" + self.ui.ruleList.item(current_row, 4).text()
        rule_infos = [rule_name, rule_lhs_path, rule_rhs_path, rule_rule_path]
        self.rule_dialog = RuleDialog(parent=self, edit_flag=True, rule_infos=rule_infos)
        ret = self.rule_dialog.exec_()
        if ret == QDialog.Rejected:
            return

    def remove_rule(self):
        current_row = self.ui.ruleList.currentRow()
        if current_row < 0:
            QMessageBox.warning(self, 'Warning', 'Please select a rule!')
            return
        mbox = QMessageBox.question(self, 'Confirm Delete', 'Are you sure to delete?')
        if mbox == QMessageBox.Yes:
            rule_path = QDir.currentPath() + "/rules/" + self.ui.ruleList.item(current_row, 4).text().split('/')[-1]
            if not rule_path or not QFile.exists(QFile(rule_path)):
                return
            if QFile.remove(rule_path):
                self.ui.ruleList.removeRow(current_row)
        else:
            return

    def get_net_from_file(self, url):
        file = QFile(url)
        if not file.open(QIODevice.ReadOnly | QIODevice.Text):
            # QMessageBox.critical(self, "Loading File Error", file.errorString())
            return
        file.seek(0)
        text_stream = QTextStream(file)
        xml_content = text_stream.readAll()
        file.close()
        parser = XmlParser()
        if not parser.parseXML(xml_content):
            msg_box = QMessageBox(QMessageBox.Warning, 'Warning', 'Document was not opened!')
            msg_box.exec_()
            return
        net = parser.getNetFromXML()
        pages = net.pages
        return pages

    def check(self):
        current_row = self.ui.ruleList.currentRow()
        if current_row < 0:
            QMessageBox.warning(self, 'Warning', 'Please select a rule!')
            return

        rule_path = QDir.currentPath() + "/rules/" + self.ui.ruleList.item(current_row, 4).text().split('/')[-1]

        with open(rule_path, 'r') as json_file:
            rule_json_str = json.load(json_file)
        rule_json_str = json.dumps(rule_json_str)
        try:
            rule_object = json.loads(rule_json_str)
        except ValueError:
            return
        lhs_input_type = rule_object["Input_Lhs"][0]
        lhs_output_type = rule_object["Output_Lhs"][0]
        rhs_input_type = rule_object["Input_Rhs"][0]
        rhs_output_type = rule_object["Output_Rhs"][0]

        if lhs_input_type != lhs_output_type:
            QMessageBox.critical(self, "Error", "The input and output types of LHS do not match!")
            return False
        if rhs_input_type != rhs_output_type:
            QMessageBox.critical(self, "Error", "The input and output types of RHS do not match!")
            return False
        if lhs_input_type != rhs_input_type:
            QMessageBox.critical(self, "Error", "The sockets of LHS and RHS do not match!")
            return False

        # check osm omg cmg
        lhs_path = QDir.currentPath() + "/examples/" + self.ui.ruleList.item(current_row, 2).text().split('/')[-1]
        rhs_path = QDir.currentPath() + "/examples/" + self.ui.ruleList.item(current_row, 3).text().split('/')[-1]
        lhs_page = self.get_net_from_file(lhs_path)
        rhs_page = self.get_net_from_file(rhs_path)

        places, trans, arcs = self.load_sub_net(lhs_page)
        lhs_input = self.get_item_by_name(places, trans, rule_object["Input_Lhs"])
        lhs_output = self.get_item_by_name(places, trans, rule_object["Output_Lhs"])
        d_lhs = DynamicProperty(places, trans, arcs)

        if not d_lhs.is_osm() and not d_lhs.is_omg() and not d_lhs.is_ppn(lhs_input, lhs_output):
            QMessageBox.critical(self, "Error", "LHS is not PPN!")
            return False

        places, trans, arcs = self.load_sub_net(rhs_page)
        rhs_input = self.get_item_by_name(places, trans, rule_object["Input_Rhs"])
        rhs_output = self.get_item_by_name(places, trans, rule_object["Output_Rhs"])
        d_rhs = DynamicProperty(places, trans, arcs)

        if not d_rhs.is_osm() and not d_rhs.is_omg() and not d_rhs.is_ppn(rhs_input, rhs_output):
            QMessageBox.critical(self, "Error", "RHS is not PPN!")
            return False

        # self.load_sub_net(rhs_page)

        msg = QMessageBox(self)
        msg.setWindowTitle("Result")
        msg.setText("    Check Pass!    ")
        msg.setWindowIcon(QIcon("./resources/application-icon-red.svg"))
        pxmap = QPixmap()
        pxmap.load("./resources/check_pass.png")
        msg.setIconPixmap(pxmap)
        msg.exec_()
        return True

    def get_item_by_name(self, places, transitions, name):
        for place in places:
            if str(name) == str(place.name):
                return place
        for tran in transitions:
            if str(name) == str(tran.name):
                return tran

    def get_item_by_id(self, places, transitions, id):
        for place in places:
            if str(id) == str(place.identity_id):
                return place
        for tran in transitions:
            if str(id) == str(tran.identity_id):
                return tran

    def load_sub_net(self, xml_page):
        places = []
        transitions = []
        arcs = []
        for page in xml_page:
            for place in page.placeNodes:
                p = Place(place=place)
                p.init_from_xml(place)
                places.append(p)
            for transition in page.transitionNodes:
                t = Transition(transition=transition)
                t.init_from_xml(transition)
                transitions.append(t)
            for xml_arc in page.arcs:
                source_item = self.get_item_by_id(places, transitions, xml_arc.source)
                target_item = self.get_item_by_id(places, transitions, xml_arc.target)

                path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
                for point in xml_arc.points:
                    path.lineTo(point)
                path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
                arc = Arc(arc=xml_arc)
                arc.init_from_xml(source_item, target_item, path, xml_arc)
                source_item.add_output_arc(arc)
                target_item.add_input_arc(arc)
                arcs.append(arc)

        return places, transitions, arcs

    def match_net(self, l_url):
        """
        子图匹配
        """
        if not self.cur_tab:
            QMessageBox.warning(self, 'Warning', 'Please create at least a tab!')
            return None
        # current tab nodes/main graph g
        big_graph = igraph.Graph(directed=True)
        nodes = self.cur_tab.scene.items()
        vertex = []
        g_type = []
        for n in nodes:
            if n.Type == Place.Type:
                g_type.append(0)
                vertex.append(str(n.identity_id))
            if n.Type == Transition.Type:
                g_type.append(1)
                vertex.append(str(n.identity_id))
        big_graph.add_vertices(vertex)
        # print(vertex)
        edges = []
        for n in nodes:
            if n.Type == Arc.Type:
                edges.append((str(n.source_item.identity_id), str(n.target_item.identity_id)))
        big_graph.add_edges(edges)
        # left-hand side/sub-graph s
        sub_graph = igraph.Graph(directed=True)
        lhs_graph_vertex = []
        lhs_graph_edges = []
        pages = self.get_net_from_file(l_url)
        if pages is None:
            QMessageBox.critical(self, 'Error', 'Path error, subnet not find!')
            return None
        lhs_type = []
        for page in pages:
            for p in page.placeNodes:
                lhs_type.append(0)
                lhs_graph_vertex.append(p.identity_id)
            for t in page.transitionNodes:
                lhs_type.append(1)
                lhs_graph_vertex.append(t.identity_id)
            for a in page.arcs:
                lhs_graph_edges.append((a.source, a.target))
        # print('l_Vertexs', lhs_graph_vertex)
        sub_graph.add_vertices(lhs_graph_vertex)
        sub_graph.add_edges(lhs_graph_edges)
        # matching whether s is in g
        is_match = big_graph.subisomorphic_vf2(sub_graph, color1=g_type, color2=lhs_type)
        # get subgraph
        ret_map = big_graph.get_subisomorphisms_vf2(sub_graph, color1=g_type, color2=lhs_type)
        all_subgraph_nodes_id = []
        all_subgraph_nodes = []
        map_back = ret_map
        new_map = []
        dup_indices = []
        i = 0
        for m in map_back:
            m = sorted(m)
            if m not in new_map:
                new_map.append(m)
            else:
                dup_indices.append(i)
            i = i + 1
        ret_map = [ret_map[i] for i in range(len(ret_map)) if (i not in dup_indices)]
        for m in ret_map:
            subgraph_nodes_id = []
            subgraph_nodes = []
            for i in m:
                subgraph_nodes_id.append(vertex[i])
                subgraph_nodes.append(self.cur_tab.scene.get_item_by_id(vertex[i]))
            all_subgraph_nodes_id.append(subgraph_nodes_id)
            all_subgraph_nodes.append(subgraph_nodes)
        # print("原网中匹配的id列表", all_subgraph_nodes_id)
        # print(is_match)
        # print(all_subgraph_nodes_id)
        return is_match, all_subgraph_nodes_id, all_subgraph_nodes, lhs_graph_vertex

    def get_inner_arcs_by_nodes(self, subgraph_nodes_ids):
        arcs = []
        ret_arcs = []
        ret_arc_pairs = []
        for item in self.cur_tab.scene.items():
            if item.Type == Arc.Type:
                arcs.append(item)
        for subgraph_nodes_id in subgraph_nodes_ids:
            temp = []
            temp2 = []
            for arc in arcs:
                if str(arc.source_item.identity_id) in subgraph_nodes_id and str(
                        arc.target_item.identity_id) in subgraph_nodes_id:
                    temp.append(arc)
                    temp2.append((arc.source_item.name, arc.target_item.name))
            ret_arcs.append(temp)
            ret_arc_pairs.append(temp2)
        return ret_arcs, ret_arc_pairs

    def apply(self):
        """
        手动应用规则
        """
        self.cur_tab = self.parent.get_current_tab()
        if not self.cur_tab:
            return
        current_row = self.ui.ruleList.currentRow()
        if current_row is None:
            QMessageBox.warning(self, 'Warning', 'Please select a rule!')
            return
        lhs_path_str = self.ui.ruleList.item(current_row, 2).text()
        lhs_path_str = QDir.currentPath() + "/examples/" + lhs_path_str.split('/')[-1]

        # 是否匹配, 匹配的子网id, 匹配的子网结点, 左手侧id列表
        is_match, subgraph_nodes_ids, subgraph_nodes, lhs_id_list = self.match_net(lhs_path_str)
        if len(subgraph_nodes_ids) < 1 or not is_match:
            QMessageBox.about(self, 'Not matching', 'Not matching')
            return
        # 获取匹配的子网内部弧
        arcs, arc_pairs = self.get_inner_arcs_by_nodes(subgraph_nodes_ids)
        # 多重匹配的情况
        if len(subgraph_nodes_ids) > 1:
            self.multi_match_dialog = MultiMatchDialog(parent=self, subgraph_list=subgraph_nodes.copy(),
                                                       arcs=arcs.copy())
            res = self.multi_match_dialog.exec()
            if res == QDialog.Rejected:
                return

        # multi_index 由用户选择了, 默认是0
        subgraph_nodes_id_selected = subgraph_nodes_ids[self.multi_index]
        subgraph_nodes_selected = subgraph_nodes[self.multi_index]

        # 获取左右手侧的item
        lhs_pages = self.get_net_from_file(QDir.currentPath() + "/examples/" +
                                           self.ui.ruleList.item(current_row, 2).text().split('/')[-1])
        rhs_pages = self.get_net_from_file(QDir.currentPath() + "/examples/" +
                                           self.ui.ruleList.item(current_row, 3).text().split('/')[-1])

        rule_path = self.ui.ruleList.item(current_row, 4).text()
        # 获取左右手侧的接口结点名字
        with open(rule_path, 'r') as json_file:
            rule_json_str = json.load(json_file)
        rule_json_str = json.dumps(rule_json_str)
        try:
            rule_object = json.loads(rule_json_str)
        except ValueError:
            return
        lhs_input_names = rule_object["Input_Lhs"].split(",")
        lhs_output_names = rule_object["Output_Lhs"].split(",")
        rhs_input_names = rule_object["Input_Rhs"].split(",")
        rhs_output_names = rule_object["Output_Rhs"].split(",")

        # 左手侧中的输入输出接口identity_id, 用于寻找索引, 后续作用不大
        lhs_input_ids = self.get_id_by_name_in_page(lhs_pages, lhs_input_names)
        lhs_output_ids = self.get_id_by_name_in_page(lhs_pages, lhs_output_names)

        # 这个索引对于原网匹配子网和左手侧的位置 是一致的
        input_map_index = lhs_id_list.index(lhs_input_ids[0])  # 左手侧中输入接口的位置索引
        output_map_index = lhs_id_list.index(lhs_output_ids[0])  # 左手侧中输出接口的位置索引

        net_input_ids = [subgraph_nodes_id_selected[input_map_index]]  # 原网中匹配子网输入接口的identity_id
        net_output_ids = [subgraph_nodes_id_selected[output_map_index]]  # 原网中匹配子网输出接口的identity_id

        inpout_pre_set = subgraph_nodes_selected[input_map_index].get_pre_set()
        input_post_set = subgraph_nodes_selected[input_map_index].get_post_set()
        output_pre_set = subgraph_nodes_selected[output_map_index].get_pre_set()
        output_post_set = subgraph_nodes_selected[output_map_index].get_post_set()

        for pre_id in inpout_pre_set[:]:
            if pre_id in subgraph_nodes_id_selected:
                inpout_pre_set.remove(pre_id)
        for post_id in input_post_set[:]:
            if post_id in subgraph_nodes_id_selected:
                input_post_set.remove(post_id)
        for pre_id in output_pre_set[:]:
            if pre_id in subgraph_nodes_id_selected:
                output_pre_set.remove(pre_id)
        for post_id in output_post_set[:]:
            if post_id in subgraph_nodes_id_selected:
                output_post_set.remove(post_id)

        net_input_nodes = []  # 原网的输入结点
        net_output_node = []  # 原网的输出结点

        subgraph_nodes_selected_ultimate = []  # 最终要在原网中删除的结点 (item形式)
        for node in subgraph_nodes_selected:
            if str(node.identity_id) in net_input_ids:
                net_input_nodes.append(node)
            elif str(node.identity_id) in net_output_ids:
                net_output_node.append(node)
            else:
                subgraph_nodes_selected_ultimate.append(node)

        pre_post_arcs_weight = {}
        for pre_node_id in inpout_pre_set:
            pre_post_arcs_weight[str(pre_node_id) + "+" + str(net_input_ids[0])] \
                = self.cur_tab.scene.find_arc(pre_node_id, net_input_ids[0]).weight
        for post_node_id in input_post_set:
            pre_post_arcs_weight[str(net_input_ids[0]) + "+" + str(post_node_id)] \
                = self.cur_tab.scene.find_arc(net_input_ids[0], post_node_id).weight
        for pre_node_id in output_pre_set:
            pre_post_arcs_weight[str(pre_node_id) + "+" + str(net_output_ids[0])] \
                = self.cur_tab.scene.find_arc(pre_node_id, net_output_ids[0]).weight
        for post_node_id in output_post_set:
            pre_post_arcs_weight[str(net_output_ids[0]) + "+" + str(post_node_id)] \
                = self.cur_tab.scene.find_arc(net_output_ids[0], post_node_id).weight
        # 删除子网, 参数为Item类型列表
        self.cur_tab.scene.del_sub_graph(subgraph_nodes_selected)

        rhs_input_ids = self.get_id_by_name_in_page(rhs_pages, rhs_input_names)
        rhs_output_ids = self.get_id_by_name_in_page(rhs_pages, rhs_output_names)

        # 添加右手侧
        self.cur_tab.scene.add_rhs_graph(rhs_pages, inpout_pre_set, input_post_set, output_pre_set, output_post_set
                                         , rhs_input_ids, rhs_output_ids, net_input_ids, net_output_ids,
                                         pre_post_arcs_weight)

        self.parent.has_inited = False
        self.parent.set_sim_btn_state(True, False, False, False, False, False)
        self.hide()

    def get_id_by_name_in_page(self, pages, names):
        places = []
        transitions = []
        ids = []
        for page in pages:
            places = page.placeNodes
            transitions = page.transitionNodes
        for name in names:
            if name.startswith("p"):
                for place in places:
                    if place.name == name:
                        ids.append(place.identity_id)
            elif name.startswith("t"):
                for trans in transitions:
                    if trans.name == name:
                        ids.append(trans.identity_id)
        return ids
