from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from src.basic.my_graphicsscene import MyGraphicsScene
from src.basic.my_graphicsview import MyGraphicsView
from src.serialization.xml_attribute import *
from src.basic.place import Place
from src.basic.transition import Transition
from src.basic.arc import Arc


class TabWidget(QWidget):
    def __init__(self, file_name, parent=None):
        super(TabWidget, self).__init__(parent)

        self.net_id = None
        self.name = None
        self.file_name = file_name  # 本tab页对应的文件名，完整路径

        # self.setWindowTitle(self.name)
        self.view = MyGraphicsView(self)
        self.view.setAlignment(Qt.AlignCenter)
        self.view.setCacheMode(QGraphicsView.CacheBackground)
        self.view.setViewportUpdateMode(QGraphicsView.FullViewportUpdate)
        self.view.setRenderHint(QPainter.SmoothPixmapTransform)

        self.scene = MyGraphicsScene(self)
        # self.scene.setSceneRect(0, 0, self.view.width(), self.view.height())
        self.scene.setSceneRect(0, 0, 2000, 2000)
        self.view.setScene(self.scene)
        self.view.centerOn(-1000000, -1000000)
        self.view.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.view.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)

        self.rules = []

        self.layout = QHBoxLayout(self)
        self.layout.addWidget(self.view)
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.setMouseTracking(True)

    def init_from_xml(self, xml_net):
        self.net_id = xml_net.id
        self.name = xml_net.name
        self.scene.init_from_xml(xml_net.pages)

        self.scene.id_util.push_ids(self.scene.items())
        self.view.centerOn(self.scene.itemsBoundingRect().center())

    def to_xml(self):
        net = XmlPetriNetAttr()
        net.id = self.net_id
        net.name = self.name
        page = XmlPageAttr()
        page.id = "page0"
        page.name = self.name
        for item in self.scene.items():
            if item.Type == Place.Type:
                page.placeNodes.append(item.to_xml())
                continue
            if item.Type == Transition.Type:
                page.transitionNodes.append(item.to_xml())
                continue
            if item.Type == Arc.Type:
                page.arcs.append(item.to_xml())
        net.pages.append(page)
        return net

    def is_saved(self):
        return self.scene.undo_stack.isClean()

