import sys
import time
import typing

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

from src.serialization.xml_attribute import XmlTransitionAttr
from src.utils.util import Util
from src.basic.place import Place


class TranSig(QObject):
    in_token_sig = pyqtSignal(bool)
    out_token_sig = pyqtSignal(bool)


global flash_counts


class FlashThread(QThread):
    flash = pyqtSignal()
    end = pyqtSignal()
    global flash_counts

    def run(self) -> None:
        while True:
            self.msleep(100)
            if flash_counts == 0 or flash_counts < 0:
                self.end.emit()
                break
            self.flash.emit()


class Transition(QGraphicsRectItem):
    Type = QGraphicsItem.UserType + 2
    DEFAULT_PEN_COLOR = Qt.black
    DEFAULT_BRUSH_COLOR = Qt.white
    TRANSITION_WIDTH = 30
    TRANSITION_HEIGHT = 30

    def __init__(self, transition=None, parent=None):
        super(Transition, self).__init__()

        self.parent = parent
        self.identity_id = None
        self.transition_id = None

        if transition:
            # 从 xml 构造
            self.identity_id = int(transition.identity_id)
            self.transition_id = int(transition.id)
        else:
            # 从无到有的新构造
            self.identity_id = Util.get_int_ruid()
            self.transition_id = self.parent.id_util.get_transition_id(self.identity_id)  # 变迁id

        self.name = "t" + str(self.transition_id) # 图元显示的名称, 应具备唯一性
        self.input_arcs = []
        self.output_arcs = []
        self.rotation = 0
        self.function = None

        self.is_desc_show = False  # 结点描述信息是否显示
        self.description = ""  # 结点描述信息
        self.brush_color = QColor(self.DEFAULT_BRUSH_COLOR)  # 结点绘制的画刷
        self.pen_color = QColor(self.DEFAULT_PEN_COLOR)  # 结点绘制的画笔

        self.setRect(0, 0, self.TRANSITION_WIDTH, self.TRANSITION_HEIGHT)

        self.label = QGraphicsSimpleTextItem(self)  # 用于显示结点描述的文本图元
        self.label.setText(self.name)
        self.label.setPos(30, 30)  # 在父Item的坐标系中设置位置
        self.label.setFlag(QGraphicsItem.ItemIsSelectable, True)
        self.label.setFlag(QGraphicsItem.ItemIsMovable, True)
        self.label.setFont(QFont("Times New Roman", 10))

        self.setZValue(1000.0)  # 位于图层的上方
        self.setFlag(QGraphicsItem.ItemIsMovable, True)  # 可以移动
        self.setFlag(QGraphicsItem.ItemIsSelectable, True)  # 可以选择
        self.setFlag(QGraphicsItem.ItemSendsGeometryChanges, True)

        self.is_desc_show = False
        self.fired_times = 0

        self.sigs = TranSig()
        self.tran_anim_timer = QTimer(self.sigs)
        self.anim_duration = 500
        self.flash_color = QColor(255, 166, 49)
        global flash_counts  # 全局变量，本模块中的类都能共享，表示激发变迁的闪烁次数
        flash_counts = 2 * self.anim_duration / 100
        self.timer_thread = FlashThread()  # 闪烁线程
        self.timer_thread.flash.connect(self.on_flash)  # 为闪烁信号绑定事件
        self.timer_thread.end.connect(self.end_flash)  # 线程结束绑定事件

    def init_from_xml(self, transition):
        self.transition_id = transition.id
        self.identity_id = transition.identity_id
        self.name = transition.name
        self.function = transition.function
        self.description = transition.comment
        self.brush_color = transition.brushColor
        self.pen_color = transition.penColor
        self.is_desc_show = transition.show

    def to_xml(self):
        transition = XmlTransitionAttr()
        transition.id = self.transition_id
        transition.identity_id = self.identity_id
        transition.name = self.name
        transition.function = self.function
        transition.rotation = self.rotation
        transition.x = self.pos().x()
        transition.y = self.pos().y()
        transition.offsetx = self.label.x()
        transition.offsety = self.label.y()
        transition.comment = self.description
        transition.show = self.is_desc_show
        transition.brushColor = self.brush_color
        transition.penColor = self.pen_color
        return transition

    def is_firable(self):
        # 只要前继弧对应的库所中有任意一个不满足的就不能激发
        for arc in self.input_arcs:
            w = arc.weight
            source_item = arc.source_item
            if source_item.get_type() == Place.get_type():
                tokens = source_item.tokens
                if tokens < w:
                    return False
        # 只要后继弧对应的库所中任意一个容量不足就不能激发
        for arc in self.output_arcs:
            target_item = arc.target_item
            if target_item.get_type() == Place.get_type():
                if target_item.check_reach_capacity(arc):
                    return False
        return True

    def is_fireable_without_capcity(self):
        # 只要前继弧对应的库所中有任意一个不满足的就不能激发
        for arc in self.input_arcs:
            w = arc.weight
            source_item = arc.source_item
            if source_item.get_type() == Place.get_type():
                tokens = source_item.tokens
                if tokens < w:
                    return False
        return True

    def fire(self, anim_duration, ball_color, flash_color):
        self.fired_times += 1
        self.anim_duration = anim_duration
        self.flash_color = flash_color
        self.get_main_win().still_anim_flag = True
        for arc in self.input_arcs:
            arc.source_item.update_tokens(-arc.weight)
            arc.animation(False, False, anim_duration, ball_color)
        self.timer_thread.start()
        for arc in self.output_arcs:
            arc.animation(True, False, anim_duration, ball_color)

    def defire(self, anim_duration, ball_color, flash_color):
        self.fired_times -= 1
        self.anim_duration = anim_duration
        self.flash_color = flash_color
        for arc in self.output_arcs:
            arc.target_item.update_tokens(-arc.weight)
            arc.animation(False, True, anim_duration, ball_color)
        self.timer_thread.start()
        for arc in self.input_arcs:
            arc.animation(True, True, anim_duration, ball_color)

    def on_flash(self):
        global flash_counts
        flash_counts -= 1  # 递减闪烁次数，不是时间
        if self.DEFAULT_BRUSH_COLOR == QColor(self.flash_color):
            self.DEFAULT_BRUSH_COLOR = Qt.white
        elif self.DEFAULT_BRUSH_COLOR == Qt.white:
            self.DEFAULT_BRUSH_COLOR = QColor(self.flash_color)
        self.update()

    def end_flash(self):
        self.DEFAULT_BRUSH_COLOR = Qt.white
        self.update()
        global flash_counts
        flash_counts = 2 * self.anim_duration / 100
        self.get_main_win().still_anim_flag = False
        self.get_main_win().append_new_marking()

    def add_input_arc(self, arc):
        self.input_arcs.append(arc)
        # self.input_arcs.add(arc)

    def add_output_arc(self, arc):
        self.output_arcs.append(arc)
        # self.output_arcs.add(arc)

    def delete_arc(self, arc_id):
        for arc in self.input_arcs:
            if arc.arc_id == arc_id:
                self.input_arcs.remove(arc)
                return
        for arc in self.output_arcs:
            if arc.arc_id == arc_id:
                self.output_arcs.remove(arc)
                return

    def check_has_relations(self) -> bool:
        if (not self.input_arcs) and (not self.output_arcs):
            return False
        else:
            return True

    def check_has_loop(self) -> bool:
        pre_set = self.get_pre_set()
        post_set = self.get_post_set()
        inter_set = list(set(pre_set) & set(post_set))
        if inter_set:
            return True
        else:
            return False

    def get_pre_set(self):
        pre_set = []
        for arc in self.input_arcs:
            pre_set.append(arc.source_item.identity_id)
        return pre_set

    def get_post_set(self):
        post_set = []
        for arc in self.output_arcs:
            post_set.append(arc.target_item.identity_id)
        return post_set

    @staticmethod
    def get_type() -> int:
        return Transition.Type

    def get_bound_rect(self):
        return QRectF(0, 0, self.TRANSITION_WIDTH, self.TRANSITION_HEIGHT).normalized()

    def shape(self):
        shape = QPainterPath()
        shape.addRect(0, 0, self.TRANSITION_WIDTH, self.TRANSITION_HEIGHT)
        return shape

    def itemChange(self, change: QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        if change == QGraphicsItem.ItemPositionChange or change == QGraphicsItem.ItemPositionHasChanged \
                or change == QGraphicsItem.ItemTransformChange or change == QGraphicsItem.ItemTransformHasChanged:
            for arc in self.input_arcs:
                arc.update_position()
            for arc in self.output_arcs:
                arc.update_position()
            self.update()
        return value

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem, widget=None):
        if not self.check_has_relations():
            pen_color = Qt.darkRed
        else:
            pen_color = self.DEFAULT_PEN_COLOR
        if self.isSelected():
            pen_color = Qt.blue
            for arc in self.input_arcs:
                arc.setSelected(True)
            for arc in self.output_arcs:
                arc.setSelected(True)

        self.label.setBrush(pen_color)
        if self.is_desc_show:
            self.label.setText(self.name + "\n" + self.description)
        else:
            self.label.setText(self.name)

        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(self.DEFAULT_BRUSH_COLOR)
        painter.setPen(pen_color)
        painter.setFont(QFont("Times", 15))
        painter.setRenderHint(QPainter.Antialiasing)
        painter.drawRect(0, 0, self.TRANSITION_WIDTH, self.TRANSITION_HEIGHT)

    def collidesWithItem(self, other: QGraphicsItem, mode: Qt.ItemSelectionMode = ...) -> bool:
        path = QPainterPath(self.mapFromItem(other, other.shape()))
        return path.intersects(self.shape())

    def get_main_win(self):
        return self.parent.parent().view.main_win
