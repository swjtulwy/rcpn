from src.basic.arc_rect import ArcRect
from src.basic.commands import *

from src.basic.attribute_dialogs import *
from src.utils.util import Util
from src.utils.id_util import Id_Util

from src.basic.place import Place
from src.basic.transition import Transition

import igraph


class MyGraphicsScene(QGraphicsScene):
    """
    场景类，本类负责管理所有的图元，同时负责处理鼠标等事件
    Undo栈放在本类中
    图元擦插入删除的槽函数也在这里实现
    """
    place_id_list = []
    place_count = 0

    def __init__(self, parent=None):
        super(MyGraphicsScene, self).__init__(parent)
        self.id_util = Id_Util()
        # self.place_index = 0
        # self.transition_index = 0
        # self.arc_index = 0
        self.current_item = None
        self.selection_rect = None
        self.origin_point = None
        self.old_pos = None
        self.path_item = None
        self.points = []
        self.current_pos = None
        self.transform = QTransform()
        # self.setSceneRect(0, 0, 2000, 2000)
        self.setItemIndexMethod(QGraphicsScene.NoIndex)  # 动态处理多应该禁止索引

        self.undo_stack = UndoStack(self)

    def enable_selection(self, selectable):
        """
        所有图元设置可选择/不可选择
        """
        for item in self.items():
            item.setFlag(QGraphicsItem.ItemIsSelectable, selectable)

    def init_from_xml(self, pages):
        """
        将xml中的结点信息初始化插入场景中
        """
        for page in pages:
            self.add_xml_places(page.placeNodes)
            self.add_xml_transitions(page.transitionNodes)
            self.add_xml_arcs(page.arcs)

    def add_xml_places(self, places):
        for place in places:
            item = Place(place=place, parent=self)
            item.init_from_xml(place)
            self.addItem(item)
            item.setPos(place.x, place.y)

    def add_xml_transitions(self, transitions):
        for transition in transitions:
            item = Transition(transition=transition, parent=self)
            item.init_from_xml(transition)
            self.addItem(item)
            item.setPos(transition.x, transition.y)
            item.setRotation(transition.rotation)

    def add_xml_arcs(self, arcs):
        for xml_arc in arcs:
            source_item = self.get_item_by_id(xml_arc.source)
            target_item = self.get_item_by_id(xml_arc.target)

            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))

            for point in xml_arc.points:
                path.lineTo(point)
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))

            arc = Arc(arc=xml_arc, parent=self)
            arc.init_from_xml(source_item, target_item, path, xml_arc)
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)

    def get_item_by_id(self, id):
        for item in self.items():
            if item.Type == Place.Type or item.Type == Transition.Type:
                if str(item.identity_id) == str(id):
                    return item

    def get_item_by_itemid(self, itemid, item_type):
        for item in self.items():
            if item.Type == Place.Type and item_type == Place.Type:
                if str(item.place_id) == itemid:
                    return item
            if item.Type == Transition.Type and item_type == Transition.Type:
                if str(item.transition_id) == itemid:
                    return item

    def get_cur_mode(self):
        return self.parent().view.main_win.mode

    def mousePressEvent(self, event: QGraphicsSceneMouseEvent):
        if event.button() != Qt.LeftButton:
            return
        self.current_pos = QPointF(event.scenePos().x() - 15, event.scenePos().y() - 15)
        self.current_item = self.itemAt(event.scenePos(), self.transform)
        current_mode = self.get_cur_mode()
        if current_mode == Util.SELECT_MODE:
            # 选择框
            if not self.current_item or self.current_item == Arc.Type:
                self.origin_point = event.scenePos()
                if self.selection_rect:
                    self.removeItem(self.selection_rect)
                    self.selection_rect = None
                self.selection_rect = QGraphicsRectItem()
                self.selection_rect.setZValue(4000.0)
                self.selection_rect.setPen(QPen(Qt.darkBlue, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
                self.selection_rect.setBrush(QColor(255, 0, 0, 80))
                self.addItem(self.selection_rect)
            else:
                self.old_pos = self.current_item.pos()
        elif current_mode == Util.PLACE_MODE:
            self.add_place(self.current_pos)
        elif current_mode == Util.TRANS_MODE:
            self.add_transition(self.current_pos)
        elif current_mode == Util.ARC_MODE:
            if not self.current_item:
                return
            if self.current_item.Type == Place.Type or self.current_item.Type == Transition.Type:
                if self.path_item is None:
                    self.path_item = QGraphicsPathItem()
                    self.path_item.setZValue(-1000.0)
                    self.path_item.setPen(QPen(Qt.black, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
                    self.points.append(event.scenePos())
                    self.addItem(self.path_item)
        QGraphicsScene.mousePressEvent(self, event)
        self.update(self.itemsBoundingRect())

    def mouseMoveEvent(self, event: QGraphicsSceneMouseEvent):
        current_mode = self.get_cur_mode()
        # 画弧模式且存在绘图路径(画弧模式下点击即确定存在了)
        if current_mode == Util.ARC_MODE and self.path_item:
            newPath = QPainterPath()
            newPath.moveTo(self.points[0])
            for point in self.points:
                newPath.lineTo(point)
            newPath.lineTo(event.scenePos())
            self.path_item.setPath(newPath)
        if self.selection_rect:
            rec = QRectF(self.origin_point, event.scenePos())
            self.selection_rect.setRect(rec.normalized())
            path = QPainterPath()
            path.addRect(rec)
            self.setSelectionArea(path, QTransform())
        # 正常模式则直接传递事件
        if current_mode == Util.SELECT_MODE or current_mode == Util.INIT_MODE:
            QGraphicsScene.mouseMoveEvent(self, event)
        self.update(self.itemsBoundingRect())

    def mouseReleaseEvent(self, event: QGraphicsSceneMouseEvent):
        current_mode = self.get_cur_mode()
        # 存在选择框
        if self.selection_rect:
            self.removeItem(self.selection_rect)
            self.selection_rect = None
        if self.current_item and self.old_pos:
            if not self.itemAt(event.scenePos(), QTransform()):
                return
            self.move_item(self.itemAt(event.scenePos(), QTransform()), self.old_pos)
            self.old_pos = QPointF(0, 0)

        item = self.itemAt(event.scenePos(), QTransform())
        if current_mode == Util.ARC_MODE and self.path_item and item:
            if item.type() == 2:
                self.points.append(event.scenePos())
                return
            else:
                path = self.path_item.path()
                self.removeItem(self.path_item)
                self.points.clear()
                self.path_item = None
                source_item = self.itemAt(path.pointAtPercent(0), QTransform())
                target_item = self.itemAt(event.scenePos(), QTransform())
                if source_item and target_item:
                    if not source_item.collidesWithItem(target_item, Qt.IntersectsItemShape):
                        if source_item.type() != target_item.type():
                            self.add_arc(source_item, target_item, path, self, 1)

        QGraphicsScene.mouseReleaseEvent(self, event)
        self.update(self.itemsBoundingRect())

    def remove_selected_items(self):
        if not self.selectedItems():
            return
        for item in self.selectedItems():
            if item.Type == QGraphicsSimpleTextItem.Type or item.Type == ArcRect.Type:
                item.setSelected(False)
            if item.Type == Arc.Type:
                self.remove_arc(item)
            if item.Type == Place.Type or item.Type == Transition.Type:
                self.remove_node(item)

    def add_place(self, current_pos):
        self.undo_stack.push(AddPlaceCommand(current_pos, self))

    def add_transition(self, current_pos):
        self.undo_stack.push(AddTransitionCommand(current_pos, self))

    def add_arc(self, source_item, target_item, arc_path, scene, weight):
        self.undo_stack.push(AddArcCommand(source_item, target_item, arc_path, scene, weight))

    def remove_node(self, item):
        self.undo_stack.push(RemoveNodeCommand(item, self))

    def remove_arc(self, item):
        if item.scene() is None:
            return
        self.undo_stack.push(RemoveArcCommand(item, self))

    def clean_undo_stack(self):
        self.undo_stack.setClean()

    def move_item(self, moved_item, old_pos):
        self.undo_stack.push(MoveCommand(moved_item, old_pos, self))

    def mouseDoubleClickEvent(self, event: QGraphicsSceneMouseEvent):
        self.current_item = self.itemAt(event.scenePos(), self.transform)
        mode = self.get_cur_mode()
        cursor_pos = QCursor.pos()
        if not self.current_item:
            return
        if mode == Util.SELECT_MODE:
            self.item_double_clicked(self.current_item, cursor_pos)

    def item_double_clicked(self, current_item, cursor_pos):
        if current_item.Type == Place.Type:
            self.place_double_clicked(current_item, cursor_pos)
        elif current_item.Type == Transition.Type:
            self.transition_double_clicked(current_item, cursor_pos)
        elif current_item.Type == Arc.Type:
            self.arc_double_clicked(current_item, cursor_pos)
        else:
            return

    def transition_double_clicked(self, item, cursor_pos):
        dialog = TransitionDialog(item)
        dialog.move(cursor_pos.x(), cursor_pos.y())
        dialog.exec()
        if dialog.result() == QDialog.Rejected:
            return
        else:
            item.name = dialog.ui.editName.text()
            # item.startTime = float(dialog.ui.editStartTime.text())
            # item.avgFireRate = float(dialog.ui.editAFR.text())
            item.function = dialog.ui.editFunc.text()
            item.is_desc_show = dialog.ui.chkBoxShow.isChecked()
            item.description = dialog.ui.editComment.toPlainText()

    def arc_double_clicked(self, item, cursor_pos):
        dialog = ArcDialog(item)
        dialog.move(cursor_pos.x(), cursor_pos.y())
        dialog.exec()

        if dialog.result() == QDialog.Rejected:
            return
        else:
            item.weight = int(dialog.ui.inputWeight.text())

    def place_double_clicked(self, item, cursor_pos):
        dialog = PlaceDialog(item)
        dialog.move(cursor_pos.x(), cursor_pos.y())
        dialog.exec()

        if dialog.result() == QDialog.Rejected:
            return
        else:
            item.tokens = int(dialog.ui.inputTokens.text())
            item.name = dialog.ui.inputName.text()
            item.capacity = int(dialog.ui.inputCapacity.text())
            item.is_desc_show = dialog.ui.chkBoxShow.isChecked()
            item.description = dialog.ui.commentEdit.toPlainText()
            item.update()

    def auto_layout(self):
        graph = igraph.Graph(directed=True)
        items = self.items()
        vertexs = []
        non_arc_nodes = []
        for item in items:
            if item.Type == Place.Type or item.Type == Transition.Type:
                non_arc_nodes.append(item)
                vertexs.append(item.name)
        graph.add_vertices(vertexs)
        edges = []
        for item in items:
            if item.Type == Arc.Type:
                edges.append((item.source_item.name, item.target_item.name))
        graph.add_edges(edges)

        center_pos_x = 0
        center_pos_y = 0
        for item in non_arc_nodes:
            center_pos_x += item.x()
            center_pos_y += item.y()

        center_pos_x /= len(non_arc_nodes)
        center_pos_y /= len(non_arc_nodes)

        layout = graph.layout_sugiyama(vgap=0.8, hgap=0.9)
        layout.rotate(-90)

        for l in layout:
            l[0] = l[0] * 100 + center_pos_x / 2 + 50
            l[1] = l[1] * 100 + center_pos_y / 2 + 200
        for i in range(0, len(non_arc_nodes)):
            non_arc_nodes[i].setPos(layout[i][0], layout[i][1])
        self.update()

    def find_item(self, node):
        for item in self.items():
            if item.Type == Place.Type or item.Type == Transition.Type or item.Type == Arc.Type:
                if node.identity_id == item.identity_id:
                    return item

    def del_sub_graph(self, to_be_del_nodes):
        for node in to_be_del_nodes:
            item = self.find_item(node)

            for input_arc in item.input_arcs[:]:
                # print("input_arc of " + item.name, input_arc.source_item.name, input_arc.target_item.name)
                self.remove_arc(input_arc)
            for output_arc in item.output_arcs[:]:
                # print("output_arc of " + item.name, output_arc.source_item.name, output_arc.target_item.name)
                self.remove_arc(output_arc)
            self.remove_node(item)

    def find_arc(self, source_id, target_id):
        for item in self.items():
            if item.Type == Arc.Type:
                if str(item.source_item.identity_id) == str(source_id) and \
                        str(item.target_item.identity_id) == str(target_id):
                    return item
        return None

    def add_rhs_graph(self, rhs_pages, input_pre_set, input_post_set, output_pre_set, output_post_set, rhs_input_ids,
                      rhs_output_ids, net_input_ids, net_output_ids, pre_post_arcs_weight):
        places = []
        transitions = []
        arcs = []
        node_map = {}

        for page in rhs_pages:
            places = page.placeNodes
            transitions = page.transitionNodes
            arcs = page.arcs

        for place in places:
            new_place = Place(parent=self)
            new_place.description = place.comment
            new_place.capacity = place.capacity
            new_place.tokens = int(place.initmark)
            new_place.is_desc_show = place.show
            new_place.setPos(place.x, place.y)
            node_map[place.identity_id] = new_place.identity_id
            self.addItem(new_place)

        for transition in transitions:
            # if transition.identity_id in rhs_input_ids or transition.identity_id in rhs_output_ids:
            #     continue
            new_transition = Transition(parent=self)
            new_transition.setPos(transition.x, transition.y)
            new_transition.is_desc_show = transition.show
            new_transition.description = transition.comment
            new_transition.function = transition.function
            node_map[transition.identity_id] = new_transition.identity_id
            self.addItem(new_transition)

        for xml_arc in arcs:
            source_item = self.get_item_by_id(node_map[xml_arc.source])
            target_item = self.get_item_by_id(node_map[xml_arc.target])

            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            for point in xml_arc.points:
                path.lineTo(point)
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            arc = Arc(parent=self)
            arc.source_item = source_item
            arc.target_item = target_item
            arc.setPath(path)
            arc.weight = xml_arc.weight
            arc.brush_color = xml_arc.brushColor
            arc.pen_color = xml_arc.penColor
            arc.create_arc()
            arc.update_position()
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)

        output_post_set = list(set(output_post_set))
        input_pre_set = list(set(input_pre_set))
        # 连接输入结点的前集
        for pre_node in input_pre_set:
            source_item = self.get_item_by_id(pre_node)
            target_item = self.get_item_by_id(node_map[rhs_input_ids[0]])
            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            arc = Arc(parent=self)
            arc.source_item = source_item
            arc.target_item = target_item
            arc.setPath(path)
            arc.weight = pre_post_arcs_weight[str(pre_node) + "+" + str(net_input_ids[0])]
            arc.brush_color = Arc.DEFAULT_BRUSH_COLOR
            arc.pen_color = Arc.DEFAULT_PEN_COLOR
            arc.create_arc()
            arc.update_position()
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)

        # 连接输入结点的后集
        for post_node in input_post_set:
            source_item = self.get_item_by_id(node_map[rhs_input_ids[0]])
            target_item = self.get_item_by_id(post_node)
            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            arc = Arc(parent=self)
            arc.source_item = source_item
            arc.target_item = target_item
            arc.setPath(path)
            arc.weight = pre_post_arcs_weight[str(net_input_ids[0]) + "+" + str(post_node)]
            arc.brush_color = Arc.DEFAULT_BRUSH_COLOR
            arc.pen_color = Arc.DEFAULT_PEN_COLOR
            arc.create_arc()
            arc.update_position()
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)

        # 连接输出结点的前集
        for pre_node in output_pre_set:
            source_item = self.get_item_by_id(pre_node)
            target_item = self.get_item_by_id(node_map[rhs_output_ids[0]])
            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            arc = Arc(parent=self)
            arc.source_item = source_item
            arc.target_item = target_item
            arc.setPath(path)
            arc.weight = pre_post_arcs_weight[str(pre_node) + "+" + str(net_output_ids[0])]
            arc.brush_color = Arc.DEFAULT_BRUSH_COLOR
            arc.pen_color = Arc.DEFAULT_PEN_COLOR
            arc.create_arc()
            arc.update_position()
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)

        # 连接输出结点的后集
        for post_node in output_post_set:
            source_item = self.get_item_by_id(node_map[rhs_output_ids[0]])
            target_item = self.get_item_by_id(post_node)
            path = QPainterPath(source_item.mapToScene(source_item.get_bound_rect().center()))
            path.lineTo(target_item.mapToScene(target_item.get_bound_rect().center()))
            arc = Arc(parent=self)
            arc.source_item = source_item
            arc.target_item = target_item
            arc.setPath(path)
            arc.weight = pre_post_arcs_weight[str(net_output_ids[0]) + "+" + str(post_node)]
            arc.brush_color = Arc.DEFAULT_BRUSH_COLOR
            arc.pen_color = Arc.DEFAULT_PEN_COLOR
            arc.create_arc()
            arc.update_position()
            self.addItem(arc)
            source_item.add_output_arc(arc)
            target_item.add_input_arc(arc)
