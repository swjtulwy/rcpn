import sys

from PyQt5 import QtGui
from PyQt5.QtCore import Qt, QPointF
from PyQt5.QtGui import QPainter
from PyQt5.QtWidgets import QGraphicsView, QApplication, QMainWindow

from forms.main_window import Ui_main_window
from src.basic.place import Place
from src.basic.transition import Transition
from src.utils.util import Util


class MyGraphicsView(QGraphicsView):
    def __init__(self, parent=None):
        super(MyGraphicsView, self).__init__(parent)
        # self.setDragMode(QGraphicsView.RubberBandDrag)
        self.setRenderHint(QPainter.Antialiasing)
        self.setRenderHint(QPainter.TextAntialiasing)

        self.main_win = self.parent().parent().parent()

        self.setMouseTracking(True)
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        self.setResizeAnchor(QGraphicsView.AnchorUnderMouse)
        self.scale_m = 1

    def wheelEvent(self, event: QtGui.QWheelEvent) -> None:
        # self.scale_fun1(event)
        # self.scale_fun2(event)
        if event.modifiers() == Qt.ControlModifier:
            self.scale_fun1(event)
        else:
            QGraphicsView.wheelEvent(self, event)

    def scale_fun1(self, event: QtGui.QWheelEvent):
        if event.modifiers() == Qt.ControlModifier:
            if event.angleDelta().y() > 0 and self.scale_m >= 50:
                return
            elif event.angleDelta().y() < 0 and self.scale_m <= 0.01:
                return
            else:
                scale_factor = self.transform().m11()
                self.scale_m = scale_factor
                wheel_data_value = event.angleDelta().y()
                if wheel_data_value > 0:
                    self.scale(1.1, 1.1)
                else:
                    self.scale(1.0 / 1.1, 1.0 / 1.1)
                self.update()

    def scale_fun2(self, event: QtGui.QWheelEvent):
        # 获取当前鼠标相对于view的位置;
        cursorPoint = event.pos()
        # 获取当前鼠标相对于scene的位置;
        scenePos = self.mapToScene(QPointF(cursorPoint.x(), cursorPoint.y()))

        # 获取view的宽高;
        viewWidth = self.viewport().width()
        viewHeight = self.viewport().height()

        # 获取当前鼠标位置相当于view大小的横纵比例;
        hScale = cursorPoint.x() / viewWidth
        vScale = cursorPoint.y() / viewHeight

        # self.scale_view(math.pow(2.0, -event.angleDelta().y() / 240.0))
        scaleFactor = self.transform().m11()
        wheelDataValue = event.angleDelta().y()
        if wheelDataValue > 0:
            self.scale(1.1, 1.1)
        else:
            self.scale(1.0 / 1.1, 1.0 / 1.1)
        # 将scene坐标转换为放大缩小后的坐标;
        viewPoint = self.transform().map(scenePos)
        # 通过滚动条控制view放大缩小后的展示scene的位置;
        self.horizontalScrollBar().setValue(int(viewPoint.x() - viewWidth * hScale))
        self.verticalScrollBar().setValue(int(viewPoint.y() - viewHeight * vScale))

    def mousePressEvent(self, event: QtGui.QMouseEvent) -> None:
        mode = self.get_mode()
        if mode == Util.INIT_MODE or mode == Util.SELECT_MODE:
            if event.button() == Qt.RightButton:
                self.setDragMode(QGraphicsView.ScrollHandDrag)
            elif event.button() == Qt.LeftButton:
                self.setDragMode(QGraphicsView.RubberBandDrag)
        else:
            self.setDragMode(QGraphicsView.NoDrag)
        QGraphicsView.mousePressEvent(self, event)

    def mouseMoveEvent(self, event: QtGui.QMouseEvent) -> None:
        str1 = "(" + str(event.pos().x()) + "," + str(event.pos().y()) + ")"
        self.main_win.status_label.setText(str1)
        QGraphicsView.mouseMoveEvent(self, event)

    def get_mode(self):
        return self.main_win.mode
