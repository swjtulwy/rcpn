import typing

from PyQt5.QtCore import QRectF, Qt
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtWidgets import QGraphicsRectItem, QGraphicsItem, QStyleOptionGraphicsItem


class ArcRect(QGraphicsRectItem) :
    def __init__(self, shape_length, parent):
        super(ArcRect, self).__init__()
        self.shape_length = shape_length
        self.parent = parent
        self.setParentItem(parent)
        self.setRect(0, 0, shape_length, shape_length)
        self.setZValue(4000.0)
        self.setFlags(QGraphicsItem.ItemIsMovable | QGraphicsItem.ItemIsSelectable)
        self.setSelected(True)

    def itemChange(self, change: QGraphicsItem.GraphicsItemChange, value: typing.Any) -> typing.Any:
        if change == QGraphicsItem.ItemSelectedHasChanged and self.isSelected():
            self.parentItem().setSelected(True)
        return value

    def paint(self, painter: QPainter, option: QStyleOptionGraphicsItem, widget=None):
        painter.setPen(QPen(Qt.black, 1, Qt.SolidLine, Qt.RoundCap, Qt.MiterJoin))
        painter.setBrush(Qt.white)
        painter.drawRect(QRectF(0, 0, self.shape_length, self.shape_length))