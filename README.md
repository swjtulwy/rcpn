# RCPN
![img.png](images/project_introduction.png)
## 项目介绍
本项目名称为 RCPN (Reconfigurable Petri Net), 是一款用于建模和仿真变结构Petri网的工具。
该工具支持位置/变迁系统(P/T Net)的建模与仿真。提供以下基本功能：
- Petri网可视化编辑、序列化与反序列化
- Petri网静态/动态属性分析
- Petri网变结构规则编辑与存取
- Petri网仿真运行
## 项目配置
本小节介绍使用本项目的源代码进行开发的方法
### 安装环境
- 开发工具: Pycharm 2022.1.3
- 开发语言: Python 3.8
- 开发框架: PyQt5
- 第三方库: Numpy, igraph, pyqt5-tools
#### 配置方法
可以参考：https://blog.csdn.net/qq_32892383/article/details/108867482
1. 安装好 Pycharm 和 Python, 设置好环境变量
2. 在当前环境中执行 `pip3 install PyQt5`, 安装开发核心开发库, 该库是 Qt 库的 Python版本
3. 在当前环境中执行 `pip3 install pyQt5-tools`, 安装开发支持库, 该库包含一些用于辅助 Qt设计界面的工具，包括Qt Designer等
4. 在Pycharm中将Qt designer 和 PyUIC 配置为外部工具
5. 在当前环境中执行 `pip3 install numpy igraph` 安装依赖的第三方库 

配置完毕后, 就可以导入本项目代码进行开发了
#### 项目结构
![img.png](images/project_structure.png)

src: 源代码目录\
forms: 存放窗体设计文件及其代码\
schemas: 存放校验xml格式的文件\
resources: 存放软件中的图标，图片等资源\
rules: 存放变结构规则位置\
examples: 存放建模案例位置\
images: 存放README文件用到的图片\
main.py 为程序入口

## 软件使用说明
软甲界面简洁明了, 主要由四个部分组成: 菜单栏, 工具栏, Petri网可视化窗口, 状态栏
![img.png](images/usage.png)

工具栏上有常用的功能按钮，以icon的形式展示，鼠标停放在上面会显示详细的功能用途,其主要分为以下几个模块，如下图所示\
![img.png](images/toolbox.png)

一个基本的流程是：
1. 新建一个Petri网
2. 进行编辑并使得Petri网结构完善
3. Petri网结构合法后，可以随时对其进行属性分析
4. 点击规则编辑按钮可以查看和编辑新的规则
5. 在仿真运行前，必须要设置仿真相关参数
6. 设置完了仿真参数后, 仿真按钮可用, 点击运行即可, 仿真运行时编辑功能不可用
7. 仿真结束请按Stop, 此时其他功能恢复