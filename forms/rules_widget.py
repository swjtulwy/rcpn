# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rules_widget.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RulesWindow(object):
    def setupUi(self, RulesWindow):
        RulesWindow.setObjectName("RulesWindow")
        RulesWindow.setWindowModality(QtCore.Qt.ApplicationModal)
        RulesWindow.resize(1203, 677)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei")
        RulesWindow.setFont(font)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout(RulesWindow)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(-1, 10, -1, 10)
        self.horizontalLayout.setSpacing(20)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.btnNewRule = QtWidgets.QToolButton(RulesWindow)
        self.btnNewRule.setMinimumSize(QtCore.QSize(100, 30))
        self.btnNewRule.setBaseSize(QtCore.QSize(100, 0))
        self.btnNewRule.setObjectName("btnNewRule")
        self.horizontalLayout.addWidget(self.btnNewRule)
        self.btnImport = QtWidgets.QToolButton(RulesWindow)
        self.btnImport.setMinimumSize(QtCore.QSize(100, 30))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei")
        self.btnImport.setFont(font)
        self.btnImport.setObjectName("btnImport")
        self.horizontalLayout.addWidget(self.btnImport)
        self.btnDetail = QtWidgets.QToolButton(RulesWindow)
        self.btnDetail.setMinimumSize(QtCore.QSize(100, 30))
        self.btnDetail.setBaseSize(QtCore.QSize(100, 0))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei")
        self.btnDetail.setFont(font)
        self.btnDetail.setObjectName("btnDetail")
        self.horizontalLayout.addWidget(self.btnDetail)
        self.btnRemove = QtWidgets.QToolButton(RulesWindow)
        self.btnRemove.setMinimumSize(QtCore.QSize(100, 30))
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei")
        self.btnRemove.setFont(font)
        self.btnRemove.setObjectName("btnRemove")
        self.horizontalLayout.addWidget(self.btnRemove)
        self.btnCheck = QtWidgets.QToolButton(RulesWindow)
        self.btnCheck.setMinimumSize(QtCore.QSize(100, 30))
        self.btnCheck.setObjectName("btnCheck")
        self.horizontalLayout.addWidget(self.btnCheck)
        self.label = QtWidgets.QLabel(RulesWindow)
        self.label.setMinimumSize(QtCore.QSize(100, 0))
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.label_cur_dir = QtWidgets.QLabel(RulesWindow)
        self.label_cur_dir.setText("")
        self.label_cur_dir.setObjectName("label_cur_dir")
        self.horizontalLayout.addWidget(self.label_cur_dir)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.ruleList = QtWidgets.QTableWidget(RulesWindow)
        self.ruleList.setColumnCount(9)
        self.ruleList.setObjectName("ruleList")
        self.ruleList.setRowCount(0)
        self.verticalLayout_3.addWidget(self.ruleList)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setContentsMargins(-1, 10, 30, 10)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.btnApply = QtWidgets.QToolButton(RulesWindow)
        self.btnApply.setMinimumSize(QtCore.QSize(100, 30))
        self.btnApply.setObjectName("btnApply")
        self.verticalLayout_2.addWidget(self.btnApply, 0, QtCore.Qt.AlignRight)
        self.verticalLayout_3.addLayout(self.verticalLayout_2)

        self.retranslateUi(RulesWindow)
        QtCore.QMetaObject.connectSlotsByName(RulesWindow)

    def retranslateUi(self, RulesWindow):
        _translate = QtCore.QCoreApplication.translate
        RulesWindow.setWindowTitle(_translate("RulesWindow", "Reconfigure Rule Lib"))
        self.btnNewRule.setText(_translate("RulesWindow", "New Rule"))
        self.btnImport.setText(_translate("RulesWindow", "Import"))
        self.btnDetail.setText(_translate("RulesWindow", "Edit"))
        self.btnRemove.setText(_translate("RulesWindow", "Remove"))
        self.btnCheck.setText(_translate("RulesWindow", "Check"))
        self.label.setText(_translate("RulesWindow", "Current Dir:"))
        self.btnApply.setText(_translate("RulesWindow", "Apply"))
