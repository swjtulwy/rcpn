# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'transitionEditDialog.ui'
#
# Created by: PyQt5 UI code generator 5.15.4
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_TransitionDialog(object):
    def setupUi(self, TransitionDialog):
        TransitionDialog.setObjectName("TransitionDialog")
        TransitionDialog.resize(448, 490)
        self.verticalLayout = QtWidgets.QVBoxLayout(TransitionDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tabWidget = QtWidgets.QTabWidget(TransitionDialog)
        font = QtGui.QFont()
        font.setFamily("Microsoft YaHei")
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.formLayoutWidget = QtWidgets.QWidget(self.tab)
        self.formLayoutWidget.setGeometry(QtCore.QRect(20, 20, 281, 191))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtWidgets.QFormLayout(self.formLayoutWidget)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.formLayoutWidget)
        self.label.setObjectName("label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label)
        self.editID = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.editID.setObjectName("editID")
        self.editID.setReadOnly(True)
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.editID)
        self.label_2 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_2.setObjectName("label_2")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.editName = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.editName.setObjectName("editName")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.editName)
        self.label_3 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_3.setObjectName("label_3")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.editStartTime = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.editStartTime.setObjectName("editStartTime")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.editStartTime)
        self.label_4 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_4.setObjectName("label_4")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.editAFR = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.editAFR.setObjectName("editAFR")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.editAFR)
        self.label_5 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_5.setObjectName("label_5")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.label_5)
        self.editFunc = QtWidgets.QLineEdit(self.formLayoutWidget)
        self.editFunc.setObjectName("editFunc")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.editFunc)
        self.label_6 = QtWidgets.QLabel(self.formLayoutWidget)
        self.label_6.setObjectName("label_6")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.label_6)
        self.chkBoxShow = QtWidgets.QCheckBox(self.formLayoutWidget)
        self.chkBoxShow.setObjectName("chkBoxShow")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.chkBoxShow)
        self.editComment = QtWidgets.QTextEdit(self.tab)
        self.editComment.setGeometry(QtCore.QRect(20, 220, 381, 171))
        self.editComment.setObjectName("editComment")
        self.tabWidget.addTab(self.tab, "")
        self.verticalLayout.addWidget(self.tabWidget)
        self.buttonBox = QtWidgets.QDialogButtonBox(TransitionDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(TransitionDialog)
        self.tabWidget.setCurrentIndex(0)
        self.buttonBox.accepted.connect(TransitionDialog.accept)
        self.buttonBox.rejected.connect(TransitionDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(TransitionDialog)

    def retranslateUi(self, TransitionDialog):
        _translate = QtCore.QCoreApplication.translate
        TransitionDialog.setWindowTitle(_translate("TransitionDialog", "Transition Edit Dialog"))
        self.label.setText(_translate("TransitionDialog", "ID:"))
        self.label_2.setText(_translate("TransitionDialog", "Name:"))
        self.label_3.setText(_translate("TransitionDialog", "StartTime:"))
        self.label_4.setText(_translate("TransitionDialog", "Avg Fire Rate:"))
        self.label_5.setText(_translate("TransitionDialog", "Function:"))
        self.label_6.setText(_translate("TransitionDialog", "Comment:"))
        self.chkBoxShow.setText(_translate("TransitionDialog", "show"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("TransitionDialog", "General"))
